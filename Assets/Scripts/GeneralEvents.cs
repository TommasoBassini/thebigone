﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class GeneralEvents : MonoBehaviour
{
    [Header("Eventi a tempo")]
    public TimedEvent[] timedEvents;

    public IEnumerator TimedFeedback(int n, float t)
    {
        yield return new WaitForSeconds(t);
        timedEvents[n].timedEvent.Invoke();
    }

    public void TimedEvent(int n)
    {
        StartCoroutine(TimedFeedback(n, Random.Range(timedEvents[n].timeToWaitMin, timedEvents[n].timeToWaitMax)));
    }
}