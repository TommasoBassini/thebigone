﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public enum AbrsorbType
{
    nessuno,
    silice,
    titanio,
    fluidoCriogenico,
    fluidoSolvente,
    cristalliAzoto
}

public class AbsorbMaterial : MonoBehaviour
{
    public AbrsorbType material;
    public bool isBroken;

    public UnityEvent absorbEvent;
    public UnityEvent expelEvent;

    public bool oneTimeAbsorb;
    public bool oneTimeExpel;
    public UnityEvent oneTimeAbsorbEvent;
    public UnityEvent oneTimeExpelEvent;

    private GameObject currentPanel;

    void Start()
    {
        CheckMaterial(material);
    }

    public void CheckMaterial(AbrsorbType _type)
    {
        if (!isBroken)
        {
            if (currentPanel != null)
            {
                currentPanel.SetActive(false);
            }

            switch (_type)
            {
                case AbrsorbType.nessuno:
                    {
                        currentPanel = transform.Find("cnvData/pnlData/Pannelli/Vuoto").gameObject;
                        break;
                    }
                case AbrsorbType.silice:
                    {
                        currentPanel = transform.Find("cnvData/pnlData/Pannelli/Silice").gameObject;
                        break;
                    }
                case AbrsorbType.titanio:
                    {
                        currentPanel = transform.Find("cnvData/pnlData/Pannelli/Titanio").gameObject;
                        break;
                    }
                case AbrsorbType.fluidoCriogenico:
                    {
                        currentPanel = transform.Find("cnvData/pnlData/Pannelli/Fluido Criogenico").gameObject;
                        break;
                    }
                case AbrsorbType.fluidoSolvente:
                    {
                        currentPanel = transform.Find("cnvData/pnlData/Pannelli/Fluido Solvente").gameObject;
                        break;
                    }
                case AbrsorbType.cristalliAzoto:
                    {
                        currentPanel = transform.Find("cnvData/pnlData/Pannelli/Cristalli di Azoto").gameObject;
                        break;
                    }
            }
            currentPanel.SetActive(true);
        }
        else
        {
            transform.Find("cnvData/pnlData/Rotto").gameObject.SetActive(true);
            this.gameObject.tag = "Untagged";
        }
    }

    public void RepairContainer()
    {
        transform.Find("cnvData/pnlData/Rotto").gameObject.SetActive(false);
        CheckMaterial(material);
    }
}
