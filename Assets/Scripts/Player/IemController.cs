﻿using UnityEngine;
using System.Collections;

public class IemController : MonoBehaviour {

	#region IEM_CONTROLLER_PARAMETERS
	[Header ("Variables")]

	[Tooltip ("Determines the seconds amount of the enemy stun - from 1 to 10")]
	[Range (1, 10)] public int secOfStun = 2;

	[Tooltip ("Determines the speed of the player projectile - from 0.01f to 2f")]
	[Range (0.01f, 2f)] public float speed = 1f;
	#endregion


	#region IEM_CONTROLLER_MONOBEHAVIOUR_METHODS
	public void FixedUpdate() {
		
		this.transform.position += transform.forward * this.speed;

    }

    void OnCollisionEnter() {

		this.Invoke ("IEMDestroy", 0.03f);

    }
	#endregion


	#region IEM_CONTROLLER_METHODS
	public void IEMDestroy () {

		Destroy (this.gameObject);

	}
	#endregion

}