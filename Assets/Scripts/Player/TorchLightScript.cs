﻿using UnityEngine;
using System.Collections;

public class TorchLightScript : MonoBehaviour {

	#region TORCHLIGHT_PARAMETERS
	[Header ("Riferimenti")]
	public Light torchLight;
	public BatteryScript batteryScript;

	[Header ("Flag Booleani")]
	public bool torchLightHasBeenTriggeredOn;
	public bool torchLightHasBeenTriggeredOff;

    private bool tutorialTorch = false;
    private PlayerStatus playerStatus;
    private MenuControl menuControl;
	#endregion


	#region TORCHLIGHT_PROPERTIES
	public float TorchLightIntensityAmount {

		set {

			this.torchLight.intensity = value;
			this.torchLight.bounceIntensity = value;

		}

		get {

			return this.torchLight.intensity;

		}

	}
	#endregion


	#region TORCHLIGHT_MONOBEHAVIOUR_METHODS
	public void Start ()
    {
        playerStatus = GetComponent<PlayerStatus>();
        menuControl = FindObjectOfType<MenuControl>();

        //Solita pulizia dei booleani
        this.torchLightHasBeenTriggeredOn = false;
		this.torchLightHasBeenTriggeredOff = false;
		this.torchLight.enabled = false;

	}

	public void Update () {
		
		if ((Input.GetKeyDown (KeyCode.Joystick1Button2) || Input.GetKeyDown(KeyCode.T)) && batteryScript.batteryEnergyAmount > 0 && playerStatus.hasPod && !menuControl.isMenu)
        {
            //Attivazione-disattivazione torcia; qualora venisse abilitata, si memorizza un booleano di trigger

            if (this.torchLight.enabled = !this.torchLight.enabled)
            {
                this.torchLightHasBeenTriggeredOn = true;
                //insetite qua torcia accesa
            }
            else
            {
                this.torchLightHasBeenTriggeredOff = true;
                //insetite qua torcia accesa
            }

            if (!tutorialTorch)
            {
                GameObject pnlTorch = FindObjectOfType<MenuControl>().transform.Find("pnlMainArea/pnlOtherInputs/pnlTorch").gameObject;
                if (pnlTorch.activeInHierarchy)
                {
                    pnlTorch.SetActive(false);
                    tutorialTorch = true;
                }
            }
			
		}

        if (batteryScript.batteryEnergyAmount <= 0)
        {
            this.torchLight.enabled = false;
            this.torchLightHasBeenTriggeredOff = true;
        }

		if (this.torchLightHasBeenTriggeredOn) {
			
			this.batteryScript.StartBatteryEnergyDecadence ();
			this.torchLightHasBeenTriggeredOn = false;
			
		}
		
		if (this.torchLightHasBeenTriggeredOff) {
			
			this.batteryScript.StopBatteryEnergyDecadence ();
			this.torchLightHasBeenTriggeredOff = false;
			
		}
		
	}
	#endregion

}