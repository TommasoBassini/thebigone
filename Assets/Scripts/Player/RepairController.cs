﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
public class RepairController : MonoBehaviour
{
    public AbrsorbType repairMaterial;
    public UnityEvent repairEvent;
    public bool isShow = false;

    public void RepairDoor(GameObject _door)
    {
        DoorBaseScript door = _door.GetComponent<DoorBaseScript>(); 
        door.isBroken = false;
        door.UpdateButtons();
        door.UpdateLight();
        Destroy(this.gameObject);
    }

    public void RepairTerminal(GameObject _terminal)
    {
        _terminal.tag = "Terminal";
        _terminal.transform.FindChild("Main").gameObject.SetActive(true);
        Destroy(this.gameObject);
    }

    public void RepairContainer(GameObject _container)
    {
        _container.tag = "Absorb";
        _container.GetComponent<AbsorbMaterial>().isBroken = false;
        _container.GetComponent<AbsorbMaterial>().RepairContainer();
        Destroy(this.gameObject);
    }

    public void GeneralRepair()
    {
        Destroy(this.gameObject);
    }
    
    public void ShowRepairUI()
    {
        if (!isShow)
        {
            StopAllCoroutines();
            isShow = true;
            StartCoroutine("ShowUICO");
        }
    }

    public void CloseRepairUI()
    {
        if (isShow)
        {
            StopAllCoroutines();
            isShow = false;
            StartCoroutine("CloseUICO");
        }
    }

    private IEnumerator ShowUICO()
    {
        
        GameObject menu = FindObjectOfType<MenuControl>().gameObject;
        GameObject repairPanel = menu.transform.Find("pnlMainArea/pnlCentralMessage/pnlRepair").gameObject;
        repairPanel.SetActive(true);
        RectTransform line1 = repairPanel.transform.Find("UIGraphic/panel1").GetComponent<RectTransform>();
        RectTransform line2 = repairPanel.transform.Find("UIGraphic/panel2").GetComponent<RectTransform>();

        float elapsedTime = 0.0f;
        float startPosY = 0;

        while (elapsedTime < 0.3f)
        {
            float prova = Mathf.Lerp(startPosY, 70.0f, (elapsedTime / 0.3f));
            line1.localPosition = new Vector3(line1.localPosition.x, prova,0);
            line2.localPosition = new Vector3(line1.localPosition.x, prova * -1, 0);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        line1.localPosition = new Vector3(line1.localPosition.x, 70, 0);
        line2.localPosition = new Vector3(line1.localPosition.x, 70 * -1, 0);
        Text text = repairPanel.transform.Find("txtRepair").GetComponent<Text>();
        text.gameObject.SetActive(true);

        switch (repairMaterial)
        {
            case AbrsorbType.silice:
                {
                    text.text = "Necessita di silice";
                    break;
                }
            case AbrsorbType.titanio:
                {
                    text.text = "Necessita di titanio";
                    break;
                }
            case AbrsorbType.fluidoCriogenico:
                {
                    text.text = "Necessita di fluido criogenico";
                    break;
                }
            case AbrsorbType.fluidoSolvente:
                {
                    text.text = "Necessita di fluido solvente";
                    break;
                }
            case AbrsorbType.cristalliAzoto:
                {
                    text.text = "Necessita di cristalli di azoto";
                    break;
                }
        }
    }

    private IEnumerator CloseUICO()
    {
        GameObject menu = FindObjectOfType<MenuControl>().gameObject;
        GameObject repairPanel = menu.transform.Find("pnlMainArea/pnlCentralMessage/pnlRepair").gameObject;

        RectTransform line1 = repairPanel.transform.Find("UIGraphic/panel1").GetComponent<RectTransform>();
        RectTransform line2 = repairPanel.transform.Find("UIGraphic/panel2").GetComponent<RectTransform>();
        Text text = repairPanel.transform.Find("txtRepair").GetComponent<Text>();
        text.gameObject.SetActive(false);
        float elapsedTime = 0.0f;
        float startPosY = line1.localPosition.y;

        while (elapsedTime < 0.3f)
        {
            float prova = Mathf.Lerp(startPosY, 0.0f, (elapsedTime / 0.3f));
            line1.localPosition = new Vector3(line1.localPosition.x, prova, 0);
            line2.localPosition = new Vector3(line1.localPosition.x, prova * -1, 0);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        line1.localPosition = new Vector3(line1.localPosition.x, 0, 0);
        line2.localPosition = new Vector3(line1.localPosition.x, 0 , 0);
        repairPanel.SetActive(false);
    }
}
