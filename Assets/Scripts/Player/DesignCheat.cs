﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DesignCheat : MonoBehaviour
{
    private bool isCheat = false;

    

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            GetComponent<OxygenScript>().FullOxygen();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            GetComponent<BatteryScript>().FullBattery();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameObject player = FindObjectOfType<PlayerStatus>().gameObject;
            player.transform.position = new Vector3(4f, 0f, 16f);
            SceneManager.LoadScene("Matteo (Area1)");
            SceneManager.UnloadScene(SceneManager.GetActiveScene());
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GameObject player = FindObjectOfType<PlayerStatus>().gameObject;
            player.transform.position = new Vector3(8, 0, 24);
            SceneManager.LoadScene("Gonzalo (Area 3)");
            SceneManager.UnloadScene(SceneManager.GetActiveScene());
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GameObject player = FindObjectOfType<PlayerStatus>().gameObject;
            player.transform.position = new Vector3(-2.7f, 1.9f, -72f);
            SceneManager.LoadScene("Antonio (Area 4)");
            SceneManager.UnloadScene(SceneManager.GetActiveScene());
        }
    }
}
