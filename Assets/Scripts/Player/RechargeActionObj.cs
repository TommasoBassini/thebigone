﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class RechargeActionObj : ActionObj
{
    private bool isRecharging = false;
    private GameObject pod;
    private GameObject podParent;
    [Header("Variabili pubbliche temporanee")]
    [Tooltip("velocità di ricarica ogni secondo")]
    public float rechargeSpeed;
    public Image batteryLevel;
    public Text batteryText;
    public Image uiBatteryBar;
    public GameObject noPodPanel;
    public GameObject podPanel;
    public UnityEvent generalFeedbackEvent;

    public UnityEvent podInsertEvent;
    public UnityEvent podTakeEvent;

    public bool oneTimeInsert; // settare a true nell'inspector se c'e'
    public bool oneTimeTake;
    public UnityEvent oneTimeInsertEvent;
    public UnityEvent oneTimeTakeEvent;

    public override void DoStuff()
    {
        generalFeedbackEvent.Invoke();

        PlayerStatus ps = FindObjectOfType<PlayerStatus>();
        if (!ps.isCourotineWork)
        {
            if (!isRecharging)
            {
                if (ps.hasPod)
                {
                    //mettere stop suono ricarica 
                    Invoke("PodAppear", 0.5f);
                    FindObjectOfType<ObjectInteract>().torcia.SetActive(false);
                    ps.PutPod();
                }
            }
            else
            {
                if (!ps.hasPod)
                {
                    //mettere start suono ricarica 
                    this.transform.GetChild(0).gameObject.SetActive(false);
                    isRecharging = false;
                    ps.TakePod();
                    FindObjectOfType<ObjectInteract>().torcia.SetActive(true);
                    noPodPanel.SetActive(true);
                    podPanel.SetActive(false);
                    actionType = ActionType.putRecharge;
                    podTakeEvent.Invoke();

                    if (oneTimeTake)
                    {
                        oneTimeTakeEvent.Invoke();
                        oneTimeTake = false;
                    }
                }
            }
        }
    }

    private void PodAppear()
    {
        PlayerStatus ps = FindObjectOfType<PlayerStatus>();

        this.transform.GetChild(0).gameObject.SetActive(true);
        noPodPanel.SetActive(false);
        podPanel.SetActive(true);
        isRecharging = true;
        actionType = ActionType.takeRecharge;

        podInsertEvent.Invoke();
        if (oneTimeInsert)
        {
            oneTimeInsertEvent.Invoke();
            oneTimeInsert = false;
        }

        StartCoroutine(RechargeBattery());
    }

    IEnumerator RechargeBattery()
    {
        BatteryScript bs = FindObjectOfType<BatteryScript>();
        float rechargeSpeedValue = (float)rechargeSpeed / 100;
        while(bs.batteryEnergyAmount < 1.0f && isRecharging)
        {
            bs.batteryEnergyAmount += rechargeSpeedValue * Time.deltaTime;
            batteryLevel.fillAmount = bs.batteryEnergyAmount;
            batteryText.text = "Batteria: " + (bs.batteryEnergyAmount * 100).ToString("0")+"%";
            uiBatteryBar.fillAmount = bs.BatteryEnergyAmount;
            yield return null;
        }
    }
}
