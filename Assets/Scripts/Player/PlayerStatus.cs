﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class PlayerStatus : MonoBehaviour
{
    public float timeNoOxygen = 300.0f;
    public bool hasPod = false;

    public int medicLvl;
    public int engineerLvl;
    public int guardLvl;

    public int maxLife;
    public int life;

    public AbrsorbType storageMaterial;

    public float timeForRestore;
    public float speedOfRestore;

    public Image damageEffectImage;
    public bool isDeath = false;

    public Image deathImage;
    public Text deathText1;
    public Text deathText2;

    public GameObject rightArm;
    public AudioSource aSEffect;

    private bool m_isCoroutineWork = false;

    public bool isCourotineWork
    {
        get { return m_isCoroutineWork; }
    }

    void Start()
    {
        life = maxLife;
        DontDestroyOnLoad(this.gameObject);
    }

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Joystick1Button7) || Input.GetKeyDown(KeyCode.Return)) && isDeath)
        {
            Scene current = SceneManager.GetActiveScene();
            SceneManager.LoadScene(current.name);
        }
    }

    public void NoOxygen()
    {
        //fare logica poco ossigeno
        StartCoroutine("NoOxygenCO");
    }

    public IEnumerator NoOxygenCO()
    {
        yield return new WaitForSeconds(5.0f);
        //mettere suono no ossigeno
        aSEffect.Play();
        float elapsedTime = 0.0f;
        BlurOptimized blur = GetComponentInChildren<BlurOptimized>();
        while (elapsedTime < timeNoOxygen)
        {
            if (!blur.enabled)
            {
                blur.enabled = true;
            }
            blur.blurSize = Mathf.Lerp(0.0f, 4.0f, (elapsedTime) / timeNoOxygen);

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(DeathCO());
    }

    public void ReceiveDamege(int damage)
    {
        StopCoroutine("RestoreLife");
        life -= damage;
        //mettere suono danno
        if (life <= 0)
        {
            StartCoroutine(DeathCO());
            //Mettere qui logica per morte
        }
        else
        {
            StartCoroutine("RestoreLife");
        }

        if (life <= (50))
        {
            StartCoroutine(DamageEffect(life));
        }
    }

    private IEnumerator DamageEffect(int life)
    {
        float elapsedTime = 0.0f;
        float startAlpha = damageEffectImage.color.a;
        float step = 200 / 50;

        float finalAlpha = (200 - (step * life)) / 255;
        while (elapsedTime < 0.3f)
        {
            damageEffectImage.color = new Color(damageEffectImage.color.r, damageEffectImage.color.g, damageEffectImage.color.b, Mathf.Lerp(startAlpha, finalAlpha, (elapsedTime / 0.3f)));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator RestoreLife()
    {
        yield return new WaitForSeconds(timeForRestore);
        float elapsedTime = 0.0f;
        float startLife = life;
        while (elapsedTime < speedOfRestore)
        {
            life = Mathf.RoundToInt(Mathf.Lerp(startLife, maxLife, (elapsedTime / speedOfRestore)));
            if (life < 50)
                StartCoroutine(DamageEffect(life));

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    public void TakeHelmet()
    {
        BlurOptimized blur = GetComponentInChildren<BlurOptimized>();
        aSEffect.Stop();
        StopCoroutine("NoOxygenCO");
        if (blur.enabled)
        {
            blur.blurSize = 0.0f;
            blur.enabled = false;
        }
    }

    public void TakePod()
    {
        hasPod = true;
        m_isCoroutineWork = true;
        StartCoroutine(TakePodCO());
    }

    IEnumerator TakePodCO()
    {
        float elapsedTime = 0.0f;
        float startRot = rightArm.transform.localEulerAngles.y;
        while (elapsedTime < 1f)
        {
            Vector3 newRot = new Vector3(rightArm.transform.localEulerAngles.x, Mathf.Lerp(startRot, 45f, (elapsedTime / 1f)), rightArm.transform.localEulerAngles.z);
            rightArm.transform.localEulerAngles = newRot;
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        m_isCoroutineWork = false;

    }

    public void PutPod()
    {
        hasPod = false;
        m_isCoroutineWork = true;
        StartCoroutine(PutPodCO());
    }

    IEnumerator PutPodCO()
    {
        float elapsedTime = 0.0f;
        float startRot = rightArm.transform.localEulerAngles.y;
        while (elapsedTime < 1f)
        {
            Vector3 newRot = new Vector3(rightArm.transform.localEulerAngles.x, Mathf.Lerp(startRot, 125f, (elapsedTime / 1f)), rightArm.transform.localEulerAngles.z);
            rightArm.transform.localEulerAngles = newRot;
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        m_isCoroutineWork = false;
    }

    IEnumerator DeathCO()
    {
        GetComponent<FirstPersonController>().enabled = false;
        isDeath = true;
        aSEffect.Stop();
        //inserire audio morte
        float elapsedTime = 0.0f;

        while (elapsedTime < 3f)
        {
            Vector3 newRot = new Vector3(Mathf.Lerp(0f, 90f, (elapsedTime / 1.5f)), this.transform.eulerAngles.y, this.transform.eulerAngles.z);
            this.transform.eulerAngles = newRot;
            deathImage.color = new Color(deathImage.color.r, deathImage.color.g, deathImage.color.b, Mathf.RoundToInt(Mathf.Lerp(0, 1, (elapsedTime / 3f))));
            deathText1.color = new Color(deathText1.color.r, deathText1.color.g, deathText1.color.b, Mathf.RoundToInt(Mathf.Lerp(0, 1, (elapsedTime / 3f))));
            deathText2.color = new Color(deathText2.color.r, deathText2.color.g, deathText2.color.b, Mathf.RoundToInt(Mathf.Lerp(0, 1, (elapsedTime / 3f))));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
