﻿using UnityEngine;
using System.Collections;

public abstract class FiniteStateMachine <TAI_Strategy> : MonoBehaviour {

	#region FINITE_STATE_MACHINE_PARAMETERS
	private int state;
	#endregion


	#region FINITE_STATE_MACHINE_MONOBEHAVIOUR_METHODS
	protected abstract void Awake ();


	protected abstract void Start ();
	#endregion


	#region FINITE_STATE_MACHINE_METHODS
	protected void FiniteStateMachineMethod (EnemyReference <TAI_Strategy> enemyReference) {

		this.state = 1;

		if (enemyReference.enemyStrategyState == StrategyState.NoStrategyChanging)
			return;

		do {

			if (enemyReference.enemyStrategyState == (StrategyState) ((int) StrategyState.NoStrategyChanging + this.state)) {

				enemyReference.enemyImplementedStrategy = enemyReference.enemyStrategyList [this.state - 1];
				return;

			}

		} while (++(this.state) <= enemyReference.enemyStrategyList.Count);

		Debug.LogError (this.ToString () + " does not understand wich strategy it should use!");

	}
	#endregion

}