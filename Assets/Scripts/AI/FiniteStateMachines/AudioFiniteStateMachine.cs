﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AudioFiniteStateMachine : FeedbackFiniteStateMachine <IAI_ImplementedAudioStrategy> {

	#region AUDIO_FINITE_STATE_MACHINE_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference = new EnemyFeedBackReference <IAI_ImplementedAudioStrategy> ();
		this.enemyFeedBackReference.enemyStrategyList = new List <IAI_ImplementedAudioStrategy> ();

	}
	#endregion


	#region AUDIO_FINITE_STATE_MACHINE_METHODS
	protected override void ExecuteFeedBack () {

		base.ExecuteFeedBack ();

		this.enemyFeedBackReference.enemyImplementedStrategy.ExecuteImplementedStrategy (this.enemyFeedBackReference.previousEnemyStrategyState);

	}
	#endregion

}