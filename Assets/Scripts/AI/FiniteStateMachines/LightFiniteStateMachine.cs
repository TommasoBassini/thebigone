﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class LightFiniteStateMachine : FeedbackFiniteStateMachine <IAI_ImplementedLightStrategy> {

	#region LIGHT_FINITE_STATE_MACHINE_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference = new EnemyFeedBackReference <IAI_ImplementedLightStrategy> ();
		this.enemyFeedBackReference.enemyStrategyList = new List <IAI_ImplementedLightStrategy> ();

	}
	#endregion


	#region AUDIO_FINITE_STATE_MACHINE_METHODS
	protected override void ExecuteFeedBack () {

		base.ExecuteFeedBack ();

		this.enemyFeedBackReference.enemyImplementedStrategy.ExecuteImplementedStrategy (this.enemyFeedBackReference.previousEnemyStrategyState);

	}
	#endregion

}