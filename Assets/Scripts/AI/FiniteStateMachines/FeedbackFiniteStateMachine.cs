﻿using UnityEngine;
using System.Collections;

public abstract class FeedbackFiniteStateMachine <TAI_Strategy> : FiniteStateMachine <TAI_Strategy>, IAI_FiniteStateMachineStrategy {

	#region FEEDBACK_FINITE_STATE_MACHINE_PARAMETERS
	protected EnemyFeedBackReference <TAI_Strategy> enemyFeedBackReference;

	private IAI_ObserverFiniteStateMachine observerFiniteStateMachine;
	#endregion


	#region FEEDBACK_FINITE_STATE_MACHINE_INTERFACES_PROPERTIES
	public StrategyState currentStrategy {

		get {

			return this.enemyFeedBackReference.enemyStrategyState;

		}

	}
	#endregion


	#region FEEDBACK_FINITE_STATE_MACHINE_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		this.observerFiniteStateMachine = this.GetComponentsInParent <IAI_ObserverFiniteStateMachine> (true) [0];
		this.observerFiniteStateMachine.FeedBacks += this.ExecuteFeedBack;

	}


	protected override void Start () {

		this.enemyFeedBackReference.previousEnemyStrategyState = StrategyState.NoStrategyChanging;
		this.enemyFeedBackReference.enemyStrategyState = StrategyState.Guarding;

		this.FiniteStateMachineMethod (this.enemyFeedBackReference);

	}
	#endregion


	#region FEEDBACK_FINITE_STATE_MACHINE_METHODS
	protected virtual void ExecuteFeedBack () {

		if (this.observerFiniteStateMachine.currentStrategy != StrategyState.NoStrategyChanging) {
			
			this.enemyFeedBackReference.previousEnemyStrategyState = this.enemyFeedBackReference.enemyStrategyState;
			this.enemyFeedBackReference.enemyStrategyState = this.observerFiniteStateMachine.currentStrategy;
			this.FiniteStateMachineMethod (this.enemyFeedBackReference);
			
		}

	}
	#endregion

}