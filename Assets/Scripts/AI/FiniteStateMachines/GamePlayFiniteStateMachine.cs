﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class GamePlayFiniteStateMachine : FiniteStateMachine <IAI_ImplementedGamePlayStrategy>, IAI_ObserverFiniteStateMachine {

	#region GAMEPLAY_FINITE_STATE_MACHINE_PARAMETERS
	protected EnemyReference <IAI_ImplementedGamePlayStrategy> enemyReference;

	private event EnemyFeedBackDelegate ExecuteFeedBacks;
	#endregion


	#region GAMEPLAY_FINITE_STATE_MACHINE_INTERFACE_PROPERTIES
	public event EnemyFeedBackDelegate FeedBacks {

		add {

			this.ExecuteFeedBacks += value;

		}


		remove {

			this.ExecuteFeedBacks -= value;

		}

	}


	public StrategyState currentStrategy {
		
		get {
			
			return this.enemyReference.enemyStrategyState;
			
		}
		
	}
	#endregion


	#region GAMEPLAY_FINITE_STATE_MACHINE_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		this.enemyReference = new EnemyReference <IAI_ImplementedGamePlayStrategy> ();
		this.enemyReference.enemyStrategyList = new List <IAI_ImplementedGamePlayStrategy> ();

	}


	protected override void Start () {

		this.enemyReference.enemyStrategyState = StrategyState.Guarding;

	}


	private void Update () {
		
		if (this.ExecuteFeedBacks != null)
			this.ExecuteFeedBacks ();

		this.FiniteStateMachineMethod (this.enemyReference);
		this.enemyReference.enemyStrategyState = this.enemyReference.enemyImplementedStrategy.ExecuteImplementedStrategy ();

	}
	#endregion

}