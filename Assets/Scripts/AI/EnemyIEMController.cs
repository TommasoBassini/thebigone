﻿using UnityEngine;
using System.Collections;

public class EnemyIEMController : MonoBehaviour {

	#region ENEMY_IEM_CONTROLLER_PARAMETERS
	[Header ("Variables")]

	[Tooltip ("Determines the damage amount of the enemy projectile - from 1 to 10")]
	[Range (1, 10)] public int enemyProjectileDamage = 1;

	[Tooltip ("Determines the speed of the enemy projectile - from 0.01f to 2f")]
	[Range (0.01f, 2f)] public float enemyProjectileSpeed = 1f;


	[Header ("Scripts")]

	[Tooltip ("DO NOT TOUCH!")]
	public PlayerStatus player;
	#endregion


	#region ENEMY_IEM_CONTROLLER_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.player = GameObject.FindGameObjectWithTag ("Player").GetComponent <PlayerStatus> ();

	}


	public void FixedUpdate () {

		this.transform.position += this.transform.forward * this.enemyProjectileSpeed;

	}


	public void OnCollisionEnter (Collision collision) {

		if (collision.collider.gameObject == this.player.gameObject)
			this.player.ReceiveDamege (this.enemyProjectileDamage);
		
		Destroy (this.gameObject);

	}
	#endregion

}