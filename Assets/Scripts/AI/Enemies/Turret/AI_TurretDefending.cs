﻿using UnityEngine;
using System.Collections;

public class AI_TurretDefending : MonoBehaviour, IAI_ImplementedGamePlayStrategy, IAI_Guarding {

	#region TURRET_DEFENDING_PARAMETERS
	[Tooltip ("DO NOT TOUCH!")]
	public AI_TurretComponent turretComponents;

	private event EnemyFeedBackDelegate TriggerGuardingFeedBacks;
	#endregion


	#region TURRET_DEFENDING_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate FeedBacks {

		add {

			this.TriggerGuardingFeedBacks += value;

		}


		remove {

			this.TriggerGuardingFeedBacks -= value;

		}

	}
	#endregion


	#region TURRET_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.turretComponents = this.GetComponent <AI_TurretComponent> ();

	}
	#endregion


	#region IMPLEMENTED_STRATEGY_METHOD
	public StrategyState ExecuteImplementedStrategy () {

		Debug.Log ("Turret is in <<Defending>>");

		if (this.turretComponents.playerInSight) {

			this.transform.LookAt (
				new Vector3 (this.turretComponents.player.transform.position.x,
					this.transform.position.y,
					this.turretComponents.player.transform.position.z));

			this.turretComponents.attackRay.SetPosition (0, this.transform.position);
			this.turretComponents.attackRay.SetPosition (1, this.turretComponents.player.transform.position);

		}


		
		if (!this.turretComponents.playerHasBeenDetected) {
			
			Debug.Log ("Turret switches from <<Defending>> to <<Guarding>>");

			if (this.TriggerGuardingFeedBacks != null)
				this.TriggerGuardingFeedBacks ();

			return StrategyState.Guarding;
			
		} else {
			
			Debug.Log ("Turret does not change strategy");
			return StrategyState.NoStrategyChanging;
			
		}

	}
	#endregion

}