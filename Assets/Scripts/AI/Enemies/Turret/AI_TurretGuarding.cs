﻿using UnityEngine;
using System.Collections;

public class AI_TurretGuarding : MonoBehaviour, IAI_ImplementedGamePlayStrategy, IAI_Defending {

	#region TURRET_GUARDING_PARAMETERS
	[Tooltip ("DO NOT TOUCH!")]
	public AI_TurretComponent turretComponents;

	private event EnemyFeedBackDelegate TriggerDefendingFeedBacks;
	#endregion


	#region SENTINEL_GUARDING_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate FeedBacks {

		add {

			this.TriggerDefendingFeedBacks += value;

		}


		remove {

			this.TriggerDefendingFeedBacks -= value;

		}

	}
	#endregion


	#region TURRET_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.turretComponents = this.GetComponent <AI_TurretComponent> ();

	}
	#endregion


	#region IMPLEMENTED_STRATEGY_METHOD
	public StrategyState ExecuteImplementedStrategy () {

		Debug.Log ("Turret is in <<Guarding>>");

		this.transform.LookAt (
			new Vector3 (this.turretComponents.turretScanner.transform.position.x,
				this.transform.position.y,
				this.turretComponents.turretScanner.transform.position.z));

		this.turretComponents.attackRay.SetPosition (0, this.transform.position);
		this.turretComponents.attackRay.SetPosition (1, this.turretComponents.turretScanner.transform.position);

		
		if (this.turretComponents.playerInSight) {
			
			Debug.Log ("Turret switches from <<Guarding>> to <<Defending>>");

			if (this.TriggerDefendingFeedBacks != null)
				this.TriggerDefendingFeedBacks ();

			return StrategyState.Defending;
			
		} else {
			
			Debug.Log ("Turret does not change strategy");
			return StrategyState.NoStrategyChanging;
			
		}

	}
	#endregion

}