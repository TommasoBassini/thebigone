﻿using UnityEngine;
using System.Collections;

public class AI_TurretComponent : MonoBehaviour {

	#region TURRET_PARAMETERS
	[Header ("Boolean Flags")]

	[Tooltip ("Determines if the enemy can or cannot attack the player")]
	public bool enemyMayAttack;												//Not to be clean on Start
	[Tooltip ("DO NOT TOUCH!")]
	public bool enemyIsActive;
	[Tooltip ("DO NOT TOUCH!")]
	public bool playerHasBeenDetected;
	[Tooltip ("DO NOT TOUCH!")]
	public bool playerInSight;							// Whether or not the player is currently sighted
	[Tooltip ("DO NOT TOUCH!")]
	public bool turretIsShoothing;


	[Header ("Variables")]

	[Tooltip ("DO NOT TOUCH! Ask programmers for utilization")]
	public int destPoint;

	[Tooltip ("Scanning time used to recognize the player - from 0.1f to 10f")]
	[Range (0.1f, 10f)] public float scanningTime = 2f;
	[Tooltip ("Waiting time used to return in guarding state - from 0.1f to 10f")]
	[Range (0.1f, 10f)] public float slipOutTime = 5f;
	[Tooltip ("Determines the Attack time of the enemy (from 0.1f to 10f)")]
	[Range (0.1f, 10f)] public float attackTime = 1f;
	[Tooltip ("Turret Scanner speed - from 0.1f to 10f")]
	[Range (0.1f, 10f)] public float turretScannerSpeed = 5f;


	[Header ("Classes")]

	[Tooltip ("DO NOT TOUCH!")]
	public LineRenderer attackRay;


	[Header ("Scripts")]

	[Tooltip ("DO NOT TOUCH!")]
	public AI_TurretArea turretArea;
	[Tooltip ("DO NOT TOUCH!")]
	public AI_Turret turretAI;
	[Tooltip ("DO NOT TOUCH!")]
	public AI_TurretScanner turretScanner;


	[Header ("GameObjects")]

	[Tooltip ("DO NOT TOUCH!")]
	public GameObject player;							// Reference to the player


	[Header ("Patrolling Points")]

	[Tooltip ("First, set the number; second, manually assign any GameObject desidered to be a patrolling point (Transforms will be automatically taken)")]
	public Transform[] points;
	#endregion


	#region TURRET_MONOBEHAVIOUR_METHODS
	public void Awake() {

		this.turretArea = this.GetComponentInParent <AI_TurretArea> ();
		this.turretAI = this.GetComponentInChildren <AI_Turret> (true);
		this.turretScanner = this.turretArea.GetComponentInChildren <AI_TurretScanner> (true);
		this.attackRay = this.GetComponentInChildren <LineRenderer> (true);
		this.player = GameObject.FindGameObjectWithTag ("Player");

	}


	public void Start () {

		if (this.turretAI.isActiveAndEnabled)
			this.enemyIsActive = true;
		else
			this.enemyIsActive = false;
		
		this.playerHasBeenDetected = false;
		this.playerInSight = false;
		this.turretIsShoothing = false;

		this.destPoint = 0;

	}


	/*private void Update () {

		if (Input.GetKeyDown (KeyCode.J))
			this.SwitchEnemyActivation (true);

		if (Input.GetKeyDown (KeyCode.K))
			this.SwitchEnemyActivation (false);

	}*/
	#endregion


	#region TURRET_METHODS
	public void SwitchEnemyActivation (bool newCondition) {

		this.enemyIsActive = newCondition;

		this.turretArea.SwitchEnemyAreaActivation (newCondition);
		this.turretAI.SwitchAITurretActivation (newCondition);
		this.turretScanner.gameObject.SetActive (newCondition);
		this.attackRay.gameObject.SetActive (newCondition);

	}


	public void SwitchEnemyAttack (bool newCondition) {

		this.enemyMayAttack = newCondition;

	}
	#endregion

}