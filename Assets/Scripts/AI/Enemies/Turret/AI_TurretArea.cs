﻿using UnityEngine;
using System.Collections;

public class AI_TurretArea : MonoBehaviour, IAI_Area {

	#region TURRET_AREA_SUBCLASSES
	public class Delegates {

		public EnemyStateDelegate <AI_TurretArea> TurretAttackPostDelay = delegate (AI_TurretArea turretAreaReference) {

			turretAreaReference.turretComponents.turretIsShoothing = true;
			turretAreaReference.attackCoroutine = turretAreaReference.KillPreviousCoroutine (turretAreaReference.attackCoroutine);

			if (turretAreaReference.DeActivateFeedBacks != null)
				turretAreaReference.DeActivateFeedBacks ();

		};

		public EnemyStateDelegate <AI_TurretArea> TurretSlipOutPostDelay = delegate (AI_TurretArea turretAreaReference) {

			turretAreaReference.turretComponents.playerHasBeenDetected = false;
			turretAreaReference.turretComponents.turretScanner.EnableTurretScanner (true);	//Might be moved elsewhere
			turretAreaReference.slipOutCoroutine = turretAreaReference.KillPreviousCoroutine (turretAreaReference.slipOutCoroutine);

			if (turretAreaReference.DeActivateFeedBacks != null)
				turretAreaReference.DeActivateFeedBacks ();

		};

		public CO_EnemyCoroutineDelegate <AI_TurretArea> CO_TurretCoroutine;

	}
	#endregion


	#region TURRET_AREA_PARAMETERS
	[Header ("Structs")]

	[Tooltip ("DO NOT TOUCH!")]
	public Vector3 direction;
	[Tooltip ("DO NOT TOUCH!")]
	public RaycastHit hit;


	[Header ("Classes")]

	[Tooltip ("DO NOT TOUCH!")]
	public Coroutine attackCoroutine;
	[Tooltip ("DO NOT TOUCH!")]
	public Coroutine slipOutCoroutine;
	[Tooltip ("DO NOT TOUCH!")]
	public Delegates delegates;


	[Header ("Scripts")]

	[Tooltip ("DO NOT TOUCH!")]
	public AI_TurretComponent turretComponents;


	private event EnemyFeedBackDelegate ActivateFeedBacks;
	private event EnemyFeedBackDelegate DeActivateFeedBacks;
	#endregion


	#region TURRET_AREA_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate StartFeedBacks {

		add {

			this.ActivateFeedBacks += value;

		}


		remove {

			this.ActivateFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate EndFeedBacks {

		add {

			this.DeActivateFeedBacks += value;

		}


		remove {

			this.DeActivateFeedBacks -= value;

		}

	}
	#endregion


	#region TURRET_AREA_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.delegates = new Delegates ();
		this.turretComponents = this.GetComponentInChildren <AI_TurretComponent> ();

	}


	public void Start () {
		
		this.delegates.CO_TurretCoroutine = this.CO_TurretDelayedTime;

		this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);
		this.slipOutCoroutine = this.KillPreviousCoroutine (this.slipOutCoroutine);

	}


	public void OnTriggerEnter (Collider other) {

		if (other.gameObject == this.turretComponents.player) {

			if (!this.turretComponents.enemyIsActive && this.turretComponents.playerHasBeenDetected)
				this.turretComponents.playerInSight = true;

		}

	}


	public void OnTriggerStay (Collider other) {

		// If the player has entered the trigger box...
		if (other.gameObject == this.turretComponents.player) {

			if (this.turretComponents.enemyIsActive) {

				if (this.turretComponents.playerHasBeenDetected) {

					// By default the player is not in sight.
					this.turretComponents.playerInSight = false;

					// Compute a vector from the enemy to the player...
					this.direction = other.transform.position - this.turretComponents.transform.position;

					// ... and if a raycast towards the player hits something...
					if (Physics.Raycast (this.turretComponents.transform.position, this.direction.normalized, out this.hit)) {

						// ... and if the raycast hits the player...
						if (this.hit.collider.gameObject == this.turretComponents.player) {

							// ... the player is in sight...
							this.turretComponents.playerInSight = true;

							if (this.turretComponents.enemyMayAttack && !this.turretComponents.turretIsShoothing) {

								this.SwitchCoroutine (out this.slipOutCoroutine, out this.attackCoroutine, ref this.turretComponents.turretIsShoothing, this.slipOutCoroutine,
									this.attackCoroutine, this.delegates.CO_TurretCoroutine, ref this.turretComponents.scanningTime, this.delegates.TurretAttackPostDelay);

							}

						} else {

							this.SwitchCoroutine (out this.attackCoroutine, out this.slipOutCoroutine, ref this.turretComponents.turretIsShoothing, this.attackCoroutine,
								this.slipOutCoroutine, this.delegates.CO_TurretCoroutine, ref this.turretComponents.slipOutTime, this.delegates.TurretSlipOutPostDelay);

							if (this.ActivateFeedBacks != null)
								this.ActivateFeedBacks ();

						}

					} else {

						this.SwitchCoroutine (out this.attackCoroutine, out this.slipOutCoroutine, ref this.turretComponents.turretIsShoothing, this.attackCoroutine,
							this.slipOutCoroutine, this.delegates.CO_TurretCoroutine, ref this.turretComponents.slipOutTime, this.delegates.TurretSlipOutPostDelay);

						if (this.ActivateFeedBacks != null)
							this.ActivateFeedBacks ();
					
					}

				}

			}

		}

	}


	public void OnTriggerExit (Collider other) {

		// If the player leaves the trigger zone...
		if (other.gameObject == this.turretComponents.player) {

			// ... the player is not in sight.
			this.turretComponents.playerInSight = false;

			if (this.turretComponents.enemyIsActive && this.turretComponents.playerHasBeenDetected) {
				
				this.SwitchCoroutine (out this.attackCoroutine, out this.slipOutCoroutine, ref this.turretComponents.turretIsShoothing, this.attackCoroutine,
					this.slipOutCoroutine, this.delegates.CO_TurretCoroutine, ref this.turretComponents.slipOutTime, this.delegates.TurretSlipOutPostDelay);

				if (this.ActivateFeedBacks != null)
					this.ActivateFeedBacks ();

			}
			
		}

	}
	#endregion


	#region TURRET_AREA_METHODS
	public void SwitchCoroutine (out Coroutine stoppedCoroutine, out Coroutine initializedCoroutine, ref bool turretIsShoothing, Coroutine toStopCoroutine,
		Coroutine toStartCoroutine, CO_EnemyCoroutineDelegate <AI_TurretArea> CO_DelegatedMethod, ref float waitingTime, EnemyStateDelegate <AI_TurretArea> DelegatedMethod) {

		stoppedCoroutine = this.KillPreviousCoroutine (toStopCoroutine);

		if (turretIsShoothing)
			turretIsShoothing = false;

		if (toStartCoroutine == null)
			initializedCoroutine = this.StartCoroutine_Auto (CO_DelegatedMethod (waitingTime, DelegatedMethod));
		else
			initializedCoroutine = toStartCoroutine;

	}


	public Coroutine KillPreviousCoroutine (Coroutine coroutine) {

		if (coroutine != null)
			this.StopCoroutine (coroutine);

		return null;

	}


	public void SwitchEnemyAreaActivation (bool newCondition) {

		if (newCondition) {

			if (this.turretComponents.playerHasBeenDetected) {

				if (this.turretComponents.playerInSight)
					this.attackCoroutine = this.StartCoroutine_Auto (this.delegates.CO_TurretCoroutine (this.turretComponents.scanningTime, this.delegates.TurretAttackPostDelay));
				
				else {
					
					this.slipOutCoroutine = this.StartCoroutine_Auto (this.delegates.CO_TurretCoroutine (this.turretComponents.slipOutTime, this.delegates.TurretSlipOutPostDelay));

					if (this.ActivateFeedBacks != null)
						this.ActivateFeedBacks ();

				}

			}

		} else {

			this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);
			this.slipOutCoroutine = this.KillPreviousCoroutine (this.slipOutCoroutine);

			this.turretComponents.turretIsShoothing = false;

		}

	}
	#endregion


	#region TURRET_AREA_COROUTINE_METHODS
	public IEnumerator CO_TurretDelayedTime (float waitingTime, EnemyStateDelegate <AI_TurretArea> DelegatedMethod) {

		yield return new WaitForSeconds (waitingTime);
		DelegatedMethod (this);

	}
	#endregion

}