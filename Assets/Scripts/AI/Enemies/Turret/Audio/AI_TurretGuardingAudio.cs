﻿using UnityEngine;
using System.Collections;

public class AI_TurretGuardingAudio : AI_StateFeedBack <IAI_Guarding>, IAI_ImplementedAudioStrategy {

	#region FEEDBACK_AUDIO_METHODS
	protected override void InitializeFeedBacks (IAI_Guarding[] entrances) {

		foreach (IAI_Guarding entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_AUDIO_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Guarding Audio");
			this.CleanTriggers ();

		}

	}
	#endregion

}