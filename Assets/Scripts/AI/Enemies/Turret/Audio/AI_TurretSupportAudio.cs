﻿using UnityEngine;
using System.Collections;

public class AI_TurretSupportAudio : AI_StateFeedBack <IAI_Area> {

	#region FEEDBACK_AUDIO_PARAMETERS
	private bool audioHasNotBeenPlayed;

	private IAI_FiniteStateMachineStrategy finiteStateMachineStrategy;
	private IAI_SpecialTrigger specialDefendingTrigger;
	#endregion


	#region FEEDBACK_AUDIO_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.finiteStateMachineStrategy = this.GetComponent <IAI_FiniteStateMachineStrategy> ();
		this.specialDefendingTrigger = this.GetComponent <IAI_SpecialTrigger> ();

	}


	protected override void Start () {

		base.Start ();

		this.audioHasNotBeenPlayed = true;

	}
	#endregion


	#region FEEDBACK_AUDIO_METHODS
	protected override void InitializeFeedBacks (IAI_Area[] entrances) {

		foreach (IAI_Area entrance in entrances) {
			
			entrance.StartFeedBacks += this.TriggerFeedBack;
			entrance.EndFeedBacks += this.CleanTriggers;

		}

	}


	protected override void TriggerFeedBack () {

		base.TriggerFeedBack ();

		if (this.audioHasNotBeenPlayed) {
			
			Debug.Log ("Test Slip Out sound activation");
			this.audioHasNotBeenPlayed = false;

		}

	}


	protected override void CleanTriggers () {

		if (this.feedBackIsTriggered) {
			
			Debug.Log ("Test Slip Out sound deactivation");
			this.audioHasNotBeenPlayed = true;

			if (this.finiteStateMachineStrategy.currentStrategy == StrategyState.Defending)
				this.specialDefendingTrigger.setSpecialTriggering = true;

		}
		
		base.CleanTriggers ();

	}
	#endregion

}