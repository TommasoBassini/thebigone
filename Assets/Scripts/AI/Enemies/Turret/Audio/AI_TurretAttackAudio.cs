﻿using UnityEngine;
using System.Collections;

public class AI_TurretAttackAudio : AI_FeedBack {

	#region TURRET_ATTACK_AUDIO_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		IAI_Attack i_Attack = this.GetComponentInParent <AI_TurretComponent> ().GetComponentInChildren <IAI_Attack> (true);

		i_Attack.StartingFeedBacks += this.ExecutePreAttackingFeedBack;
		i_Attack.EndingFeedBacks += this.ExecutePostAttackingFeedBack;
		i_Attack.AttackFeedBacks += this.ExecuteShotFeedBack;

	}
	#endregion


	#region FEEDBACK_AUDIO_METHODS
	private void ExecutePreAttackingFeedBack () {

		Debug.Log ("Pre Attack Test Sound");
		this.TriggerFeedBack ();

	}


	private void ExecutePostAttackingFeedBack () {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Post Attack Test Sound");
			this.CleanTriggers ();

		}

	}


	private void ExecuteShotFeedBack () {

		Debug.Log ("Shot Test Sound");

	}
	#endregion

}