﻿using UnityEngine;
using System.Collections;

public class AI_TurretAudio : AudioFiniteStateMachine {

	#region TURRET_AUDIO_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_TurretGuardingAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_TurretDefendingAudio> ());

	}
	#endregion

}