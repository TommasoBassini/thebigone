﻿using UnityEngine;
using System.Collections;

public class AI_TurretAttack : MonoBehaviour, IAI_Attack {

	#region TURRET_ATTACK_DELEGATES
	public EnemyTriggerDelegate <AI_TurretAttack> DelegatedMethod = delegate (AI_TurretAttack turretAttackReference, Collider playerCollider) {

		float angle;
		Vector3 distance;
		GameObject projectile;

		Debug.LogWarning ("Shooting!");

		distance = playerCollider.transform.position - turretAttackReference.transform.position;
		angle = Vector3.Angle (distance, turretAttackReference.transform.forward);

		projectile = Instantiate <GameObject> (turretAttackReference.enemyProjectile);

		projectile.transform.position = turretAttackReference.transform.position;
		projectile.transform.LookAt (playerCollider.transform);

		projectile.transform.position += projectile.transform.forward;

		if (turretAttackReference.TriggerAttackFeedBacks != null)
			turretAttackReference.TriggerAttackFeedBacks ();

	};
	#endregion

	#region TURRET_ATTACK_PARAMETERS
	[Header ("Classes")]

	[Tooltip ("DO NOT TOUCH!")]
	public Coroutine attackCoroutine;
	[Tooltip ("DO NOT TOUCH!")]
	public Collider playerCollider;


	[Header ("Scripts")]

	[Tooltip ("DO NOT TOUCH!")]
	public AI_TurretComponent turretComponents;


	[Header ("Prefabs")]

	[Tooltip ("DO NOT MODIFY!")]
	public GameObject enemyProjectile;


	private event EnemyFeedBackDelegate TriggerPreAttackingFeedBacks;
	private event EnemyFeedBackDelegate TriggerPostAttackingFeedBacks;
	private event EnemyFeedBackDelegate TriggerAttackFeedBacks;
	#endregion


	#region SENTINEL_ATTACK_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate StartingFeedBacks {

		add {

			this.TriggerPreAttackingFeedBacks += value;

		}


		remove {

			this.TriggerPreAttackingFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate EndingFeedBacks {

		add {

			this.TriggerPostAttackingFeedBacks += value;

		}


		remove {

			this.TriggerPostAttackingFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate AttackFeedBacks {

		add {

			this.TriggerAttackFeedBacks += value;

		}


		remove {

			this.TriggerAttackFeedBacks -= value;

		}

	}
	#endregion


	#region TURRET_ATTACK_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.playerCollider = GameObject.FindGameObjectWithTag ("Player").GetComponent <Collider> ();
		this.turretComponents = this.GetComponentInParent <AI_TurretComponent> ();

	}


	public void Start () {

		this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);

	}


	public void Update () {

		if (this.turretComponents.turretIsShoothing) {

			if (this.attackCoroutine == null) {
				
				this.attackCoroutine = this.StartCoroutine_Auto (this.CO_Attack (this.turretComponents.attackTime, this.DelegatedMethod));

				if (this.TriggerPreAttackingFeedBacks != null)
					this.TriggerPreAttackingFeedBacks ();

			}

		} else {

			this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);

			if (this.TriggerPostAttackingFeedBacks != null)
				this.TriggerPostAttackingFeedBacks ();

		}

	}
	#endregion


	#region TURRET_ATTACK_METHODS
	public Coroutine KillPreviousCoroutine (Coroutine coroutine) {

		if (coroutine != null)
			this.StopCoroutine (coroutine);

		return null;

	}
	#endregion


	#region TURRET_ATTACK_COROUTINES
	public IEnumerator CO_Attack (float attackTime, EnemyTriggerDelegate <AI_TurretAttack> DelegatedMethod) {

		while (true) {

			yield return new WaitForSeconds (attackTime);
			DelegatedMethod (this, this.playerCollider);

		}

	}
	#endregion

}