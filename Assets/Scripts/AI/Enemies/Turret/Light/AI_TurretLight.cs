﻿using UnityEngine;
using System.Collections;

public class AI_TurretLight : LightFiniteStateMachine, IAI_FiniteStateMachineFeedBacks {

	#region TURRET_LIGHT_PARAMETERS
	private event EnemyFeedBackDelegate ExecuteFeedBacks;
	#endregion


	#region TURRET_LIGHT_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate FeedBacks {

		add {

			this.ExecuteFeedBacks += value;

		}


		remove {

			this.ExecuteFeedBacks -= value;

		}

	}
	#endregion


	#region TURRET_LIGHT_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_TurretGuardingLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_TurretDefendingLight> ());

	}
	#endregion


	#region AUDIO_FINITE_STATE_MACHINE_METHODS
	protected override void ExecuteFeedBack () {

		base.ExecuteFeedBack ();

		if (this.ExecuteFeedBacks != null)
			this.ExecuteFeedBacks ();

	}
	#endregion

}