﻿using UnityEngine;
using System.Collections;

public class AI_TurretSupportLight : AI_StateFeedBack <IAI_Area> {

	#region FEEDBACK_LIGHT_PARAMETERS
	private bool specialFeedBackIsTriggered;

	private IAI_FiniteStateMachineStrategy finiteStateMachineStrategy;
	private IAI_SpecialTrigger specialDefendingTrigger;
	#endregion


	#region FEEDBACK_LIGHT_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.finiteStateMachineStrategy = this.GetComponentInChildren <IAI_FiniteStateMachineStrategy> (true);
		this.specialDefendingTrigger = this.GetComponentInChildren <IAI_SpecialTrigger> (true);
		this.GetComponentInChildren <IAI_FiniteStateMachineFeedBacks> (true).FeedBacks += this.FeedBack;

	}


	protected override void Start () {

		base.Start ();

		this.specialFeedBackIsTriggered = false;

	}
	#endregion


	#region FEEDBACK_LIGHT_METHODS
	protected override void TriggerFeedBack () {

		base.TriggerFeedBack ();

		this.specialFeedBackIsTriggered = true;

	}


	protected override void InitializeFeedBacks (IAI_Area[] entrances) {

		foreach (IAI_Area entrance in entrances) {

			entrance.StartFeedBacks += this.TriggerFeedBack;
			entrance.EndFeedBacks += this.CleanTriggers;

		}

	}


	private void FeedBack () {

		if (this.feedBackIsTriggered)
			Debug.Log ("Test Continuos Light FeedBack");
		
		else if (this.finiteStateMachineStrategy.currentStrategy == StrategyState.Defending && this.specialFeedBackIsTriggered) {

			this.specialDefendingTrigger.setSpecialTriggering = true;
			this.specialFeedBackIsTriggered = false;

		}

	}
	#endregion

}