﻿using UnityEngine;
using System.Collections;

public class AI_TurretGuardingLight : AI_StateFeedBack <IAI_Guarding>, IAI_ImplementedLightStrategy {

	#region FEEDBACK_LIGHT_METHODS
	protected override void InitializeFeedBacks (IAI_Guarding[] entrances) {

		foreach (IAI_Guarding entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_LIGHT_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {
			
			Debug.Log ("Test Guarding Light");
			this.CleanTriggers ();

		}

	}
	#endregion

}