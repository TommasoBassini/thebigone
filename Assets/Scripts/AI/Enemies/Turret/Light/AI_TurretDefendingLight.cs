﻿using UnityEngine;
using System.Collections;

public class AI_TurretDefendingLight : AI_StateFeedBack <IAI_Defending>, IAI_ImplementedLightStrategy, IAI_SpecialTrigger {

	#region FEEDBACK_LIGHT_INTERFACES_PROPERTIES
	public bool setSpecialTriggering {

		set {

			if (value)
				this.feedBackIsTriggered = value;

		}

	}
	#endregion


	#region FEEDBACK_LIGHT_METHODS
	protected override void InitializeFeedBacks (IAI_Defending[] entrances) {

		foreach (IAI_Defending entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_LIGHT_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Defending Light");
			this.CleanTriggers ();

		}

	}
	#endregion

}