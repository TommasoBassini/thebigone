﻿using UnityEngine;
using System.Collections;

public class AI_Turret : GamePlayFiniteStateMachine {

	#region TURRET_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_TurretGuarding> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_TurretDefending> ());

	}
	#endregion


	#region TURRET_METHODS
	public void SwitchAITurretActivation (bool newCondition) {

		this.enabled = newCondition;

	}
	#endregion

}