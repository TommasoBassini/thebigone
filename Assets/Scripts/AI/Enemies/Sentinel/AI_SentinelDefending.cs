﻿using UnityEngine;
using System.Collections;

public class AI_SentinelDefending : MonoBehaviour, IAI_ImplementedGamePlayStrategy, IAI_Scanning {

	#region SENTINEL_DEFENDING_PARAMETERS
	[Tooltip ("DO NOT TOUCH!")]
	public AI_SentinelComponent sentinelComponents;

	private event EnemyFeedBackDelegate TriggerScanningFeedBacks;
	#endregion


	#region SENTINEL_DEFENDING_INTERFACES_PROPERTIES
	event EnemyFeedBackDelegate IAI_Scanning.FeedBacks {

		add {

			this.TriggerScanningFeedBacks += value;

		}


		remove {

			this.TriggerScanningFeedBacks -= value;

		}

	}
	#endregion


	#region SENTINEL_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.sentinelComponents = this.GetComponent <AI_SentinelComponent> ();

	}
	#endregion


	#region SENTINEL_METHODS
	public void ResetHearingCollidersRadius () {

		this.sentinelComponents.sentinelHasEnlargedItsHearingColliders = false;

		this.sentinelComponents.runCol.radius *= 0.5f;
		this.sentinelComponents.walkCol.radius *= 0.5f;
		this.sentinelComponents.crouchCol.radius *= 0.5f;

	}

	public void ChasePlayer () {

		this.sentinelComponents.agent.destination = this.sentinelComponents.player.transform.position;

	}
	#endregion


	#region IMPLEMENTED_STRATEGY_METHOD
	public StrategyState ExecuteImplementedStrategy () {

		if (!this.sentinelComponents.enemyHasBeenStunned) {

			Debug.Log ("Sentinel is in <<Defending>>");

			this.sentinelComponents.agent.speed = this.sentinelComponents.runningSpeed;

			if (this.sentinelComponents.sentinelHasEnlargedItsHearingColliders)
				this.ResetHearingCollidersRadius ();

			this.ChasePlayer ();


			if (!this.sentinelComponents.playerInSight) {

				Debug.Log ("Sentinel switches from <<Defending>> to <<Scanning>>");

				if (this.TriggerScanningFeedBacks != null)
					this.TriggerScanningFeedBacks ();

				return StrategyState.Scanning;

			} else {

				Debug.Log ("Sentinel does not change strategy");
				return StrategyState.NoStrategyChanging;

			}

		} else {
			
			Debug.Log ("Sentinel has been stunned");
			return StrategyState.NoStrategyChanging;

		}

	}
	#endregion

}