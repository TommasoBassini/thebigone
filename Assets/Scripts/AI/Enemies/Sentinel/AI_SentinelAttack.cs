﻿using UnityEngine;
using System.Collections;

public class AI_SentinelAttack : MonoBehaviour, IAI_Attack {

	#region SENTINEL_ATTACK_DELEGATES
	public EnemyTriggerDelegate <AI_SentinelAttack> DelegatedMethod = delegate (AI_SentinelAttack sentinelAttackReference, Collider playerCollider) {

		float angle;
		Vector3 distance;
		GameObject projectile;

		Debug.LogWarning ("Shooting!");

		distance = playerCollider.transform.position - sentinelAttackReference.transform.position;
		angle = Vector3.Angle (distance, sentinelAttackReference.transform.forward);

		projectile = Instantiate <GameObject> (sentinelAttackReference.enemyProjectile);

		projectile.transform.position = sentinelAttackReference.transform.position;
		projectile.transform.LookAt (playerCollider.transform);

		projectile.transform.position += projectile.transform.forward;

		if (sentinelAttackReference.TriggerAttackFeedBacks != null)
			sentinelAttackReference.TriggerAttackFeedBacks ();

	};
	#endregion


	#region SENTINEL_ATTACK_PARAMETERS
	[Header ("Variables")]

	[Tooltip ("DO NOT TOUCH!")]
	public float angle;


	[Header ("Structs")]

	[Tooltip ("DO NOT TOUCH!")]
	public Vector3 distance;


	[Header ("Classes")]

	[Tooltip ("DO NOT TOUCH!")]
	public Coroutine attackCoroutine;
	[Tooltip ("DO NOT TOUCH!")]
	public Collider playerCollider;
	[Tooltip ("DO NOT TOUCH!")]
	public LineRenderer attackRay;


	[Header ("Scripts")]

	[Tooltip ("DO NOT TOUCH!")]
	public AI_SentinelComponent sentinelComponents;


	[Header ("Prefabs")]

	[Tooltip ("DO NOT MODIFY!")]
	public GameObject enemyProjectile;


	private event EnemyFeedBackDelegate TriggerPreAttackingFeedBacks;
	private event EnemyFeedBackDelegate TriggerPostAttackingFeedBacks;
	private event EnemyFeedBackDelegate TriggerAttackFeedBacks;
	#endregion


	#region SENTINEL_ATTACK_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate StartingFeedBacks {

		add {

			this.TriggerPreAttackingFeedBacks += value;

		}


		remove {

			this.TriggerPreAttackingFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate EndingFeedBacks {

		add {

			this.TriggerPostAttackingFeedBacks += value;

		}


		remove {

			this.TriggerPostAttackingFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate AttackFeedBacks {

		add {

			this.TriggerAttackFeedBacks += value;

		}


		remove {

			this.TriggerAttackFeedBacks -= value;

		}

	}
	#endregion


	#region SENTINEL_ATTACK_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.playerCollider = GameObject.FindGameObjectWithTag ("Player").GetComponent <Collider> ();
		this.attackRay = this.GetComponent <LineRenderer> ();
		this.sentinelComponents = this.GetComponentInParent <AI_SentinelComponent> ();

	}


	public void Start () {

		this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);

	}


	public void Update () {

		this.attackRay.SetPosition (0, this.transform.position);
		this.attackRay.SetPosition (1, this.transform.position + (this.sentinelComponents.viewCol.radius * this.transform.forward));

		if (this.sentinelComponents.inputCheckingCoroutine != null) {

			this.distance = this.sentinelComponents.player.transform.position - this.sentinelComponents.transform.position;
			this.angle = Vector3.Angle (this.distance, this.sentinelComponents.transform.forward);

			if (this.angle < this.sentinelComponents.fieldOfViewAngle * 0.5f) {

				if (this.distance.sqrMagnitude < Mathf.Pow (this.sentinelComponents.attackDistance, 2f)) {

					this.sentinelComponents.StopAgent ();

					if (this.sentinelComponents.agent.velocity == Vector3.zero) {

						if (this.angle > this.sentinelComponents.angularDegreeSpeed) {

							this.distance.y = this.sentinelComponents.transform.position.y;

							if (Vector3.Dot (Vector3.Cross (this.sentinelComponents.transform.forward, this.distance), Vector3.up) > 0f)
								this.sentinelComponents.transform.eulerAngles += Quaternion.AngleAxis (this.sentinelComponents.angularDegreeSpeed * Time.deltaTime, Vector3.up).eulerAngles;
							else
								this.sentinelComponents.transform.eulerAngles += Quaternion.AngleAxis (this.sentinelComponents.angularDegreeSpeed * Time.deltaTime, Vector3.down).eulerAngles;

						} else {

							this.sentinelComponents.transform.LookAt (new Vector3 (this.sentinelComponents.player.transform.position.x,
								this.sentinelComponents.transform.position.y, this.sentinelComponents.player.transform.position.z));

						}

					}

				} else if (this.sentinelComponents.agentHasBeenStopped)
					this.sentinelComponents.ResumeAgent ();

			}

		}

		if (this.sentinelComponents.enemyMayAttack && this.sentinelComponents.playerInSight) {

			if (this.attackCoroutine == null) {
				
				this.attackCoroutine = this.StartCoroutine_Auto (this.CO_Attack (this.sentinelComponents.attackTime, this.DelegatedMethod));

				if (this.TriggerPreAttackingFeedBacks != null)
					this.TriggerPreAttackingFeedBacks ();

			}
			
		} else {

			this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);

			if (this.TriggerPostAttackingFeedBacks != null)
				this.TriggerPostAttackingFeedBacks ();

		}

	}
	#endregion


	#region SENTINEL_ATTACK_METHODS
	public Coroutine KillPreviousCoroutine (Coroutine coroutine) {

		if (coroutine != null)
			this.StopCoroutine (coroutine);

		return null;

	}
	#endregion


	#region SENTINEL_ATTACK_COROUTINES
	public IEnumerator CO_Attack (float attackTime, EnemyTriggerDelegate <AI_SentinelAttack> DelegatedMethod) {

		while (true) {

			yield return new WaitForSeconds (attackTime);
			DelegatedMethod (this, this.playerCollider);

		}

	}
	#endregion

}