﻿using UnityEngine;
using System.Collections;

public class AI_SentinelScanningAudio : AI_StateFeedBack <IAI_Scanning>, IAI_ImplementedAudioStrategy {

	#region FEEDBACK_AUDIO_METHODS
	protected override void InitializeFeedBacks (IAI_Scanning[] entrances) {

		foreach (IAI_Scanning entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_AUDIO_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Scanning Audio");
			this.CleanTriggers ();

		}

	}
	#endregion

}