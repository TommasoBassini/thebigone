﻿using UnityEngine;
using System.Collections;

public class AI_SentinelAudio : AudioFiniteStateMachine {

	#region SENTINEL_AUDIO_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelGuardingAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelDefendingAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelFallingIntoLineAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelInspectingAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelScanningAudio> ());

	}
	#endregion

}