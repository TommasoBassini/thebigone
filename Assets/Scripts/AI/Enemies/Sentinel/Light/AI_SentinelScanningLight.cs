﻿using UnityEngine;
using System.Collections;

public class AI_SentinelScanningLight : AI_StateFeedBack <IAI_Scanning>, IAI_ImplementedLightStrategy {

	#region FEEDBACK_LIGHT_METHODS
	protected override void InitializeFeedBacks (IAI_Scanning[] entrances) {

		foreach (IAI_Scanning entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_LIGHT_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Scanning Light");
			this.CleanTriggers ();

		}

	}
	#endregion

}