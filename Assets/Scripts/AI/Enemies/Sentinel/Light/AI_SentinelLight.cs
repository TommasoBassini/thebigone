﻿using UnityEngine;
using System.Collections;

public class AI_SentinelLight : LightFiniteStateMachine {

	#region SENTINEL_LIGHT_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelGuardingLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelDefendingLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelFallingIntoLineLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelInspectingLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelScanningLight> ());

	}
	#endregion

}