﻿using UnityEngine;
using System.Collections;

public class AI_SentinelInspectingLight : AI_StateFeedBack <IAI_Inspecting>, IAI_ImplementedLightStrategy {

	#region FEEDBACK_LIGHT_METHODS
	protected override void InitializeFeedBacks (IAI_Inspecting[] entrances) {

		foreach (IAI_Inspecting entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_LIGHT_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			if (previousStrategy == StrategyState.Scanning)
				Debug.Log ("Test Inspecting Light");

			this.CleanTriggers ();

		}

	}
	#endregion

}