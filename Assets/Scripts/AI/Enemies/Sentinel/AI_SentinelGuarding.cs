﻿using UnityEngine;
using System.Collections;

public class AI_SentinelGuarding : MonoBehaviour, IAI_ImplementedGamePlayStrategy, IAI_Defending, IAI_Inspecting {

	#region SENTINEL_GUARDING_PARAMETERS
	[Tooltip ("DO NOT TOUCH!")]
	public AI_SentinelComponent sentinelComponents;

	private event EnemyFeedBackDelegate TriggerDefendingFeedBacks;
	private event EnemyFeedBackDelegate TriggerInspectingFeedBacks;
	#endregion


	#region SENTINEL_GUARDING_INTERFACES_PROPERTIES
	event EnemyFeedBackDelegate IAI_Defending.FeedBacks {

		add {

			this.TriggerDefendingFeedBacks += value;

		}


		remove {

			this.TriggerDefendingFeedBacks -= value;

		}

	}


	event EnemyFeedBackDelegate IAI_Inspecting.FeedBacks {

		add {

			this.TriggerInspectingFeedBacks += value;

		}


		remove {

			this.TriggerInspectingFeedBacks -= value;

		}

	}
	#endregion


	#region SENTINEL_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.sentinelComponents = this.GetComponent <AI_SentinelComponent> ();

	}
	#endregion


	#region SENTINEL_METHODS
	public void GoToNextPoint () {

		// Returns if no points have been set up
		if (this.sentinelComponents.points.Length == 0) {

			Debug.LogWarning ("WARNING! " + this.ToString() + " Has the transform's array length equal to zero!");
			return;

		}

		// Set the agent to go to the currently selected destination.
		this.sentinelComponents.agent.destination = this.sentinelComponents.points [this.sentinelComponents.destPoint].position;

		// Choose the next point in the array as the destination,
		// cycling to the start if necessary.
		this.sentinelComponents.destPoint = (this.sentinelComponents.destPoint + 1) % this.sentinelComponents.points.Length;

	}
	#endregion


	#region IMPLEMENTED_STRATEGY_METHOD
	public StrategyState ExecuteImplementedStrategy () {

		if (!this.sentinelComponents.enemyHasBeenStunned) {

			Debug.Log ("Sentinel is in <<Guarding>>");


			if (this.sentinelComponents.agent.remainingDistance < 0.5f) {
				
				if (this.sentinelComponents.enemyIsPatrolling)
					this.GoToNextPoint ();
				else if (this.sentinelComponents.enemyIsChangingDestination) {

					this.GoToNextPoint ();
					this.sentinelComponents.SwitchEnemyDestination (false);

				}

			}
		

			if (this.sentinelComponents.playerInSight) {

				Debug.Log ("Sentinel switches from <<Guarding>> to <<Defending>>");

				if (this.TriggerDefendingFeedBacks != null)
					this.TriggerDefendingFeedBacks ();

				return StrategyState.Defending;

			} else if (this.sentinelComponents.playerHasBeenHeard) {

				Debug.Log ("Sentinel switches from <<Guarding>> to <<Inspecting>>");

				if (this.TriggerInspectingFeedBacks != null)
					this.TriggerInspectingFeedBacks ();

				return StrategyState.Inspecting;

			} else {

				Debug.Log ("Sentinel does not change strategy");
				return StrategyState.NoStrategyChanging;

			}

		} else {
			
			Debug.Log ("Sentinel has been stunned");
			return StrategyState.NoStrategyChanging;

		}

	}
	#endregion

}