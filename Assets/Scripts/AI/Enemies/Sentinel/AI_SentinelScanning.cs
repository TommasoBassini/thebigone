﻿using UnityEngine;
using System.Collections;

public class AI_SentinelScanning : MonoBehaviour, IAI_ImplementedGamePlayStrategy, IAI_Defending, IAI_Inspecting, IAI_FallingIntoLine {

	#region SENTINEL_SCANNING_PARAMETERS
	public Coroutine scanningCoroutine;
	[Tooltip ("DO NOT TOUCH!")]
	public AI_SentinelComponent sentinelComponents;

	private event EnemyFeedBackDelegate TriggerDefendingFeedBacks;
	private event EnemyFeedBackDelegate TriggerInspectingFeedBacks;
	private event EnemyFeedBackDelegate TriggerFallingIntoLineFeedBacks;
	#endregion


	#region SENTINEL_SCANNING_INTERFACES_PROPERTIES
	event EnemyFeedBackDelegate IAI_Defending.FeedBacks {

		add {

			this.TriggerDefendingFeedBacks += value;

		}


		remove {

			this.TriggerDefendingFeedBacks -= value;

		}

	}


	event EnemyFeedBackDelegate IAI_Inspecting.FeedBacks {

		add {

			this.TriggerInspectingFeedBacks += value;

		}


		remove {

			this.TriggerInspectingFeedBacks -= value;

		}

	}


	event EnemyFeedBackDelegate IAI_FallingIntoLine.FeedBacks {

		add {

			this.TriggerFallingIntoLineFeedBacks += value;

		}


		remove {

			this.TriggerFallingIntoLineFeedBacks -= value;

		}

	}
	#endregion


	#region SENTINEL_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.sentinelComponents = this.GetComponent <AI_SentinelComponent> ();

	}
	#endregion


	#region SENTINEL_METHODS
	public bool EnlargeHearingCollidersRadius () {

        if (!this.sentinelComponents.sentinelHasEnlargedItsHearingColliders)
        {
		this.sentinelComponents.sentinelHasEnlargedItsHearingColliders = true;

		this.sentinelComponents.runCol.radius *= 2f;
		this.sentinelComponents.walkCol.radius *= 2f;
		this.sentinelComponents.crouchCol.radius *= 2f;
        }

		return true;

	}


	public void StopScanning () {

		if (this.scanningCoroutine != null)
			this.StopCoroutine (this.scanningCoroutine);

		this.sentinelComponents.sentinelEndsScanning = false;
		this.sentinelComponents.sentinelIsScanning = false;

	}
	#endregion


	#region SENTINEL_COROUTINES
	public IEnumerator CO_ScanningCoroutine () {

		yield return new WaitForSeconds (this.sentinelComponents.scanningTime);
		this.sentinelComponents.sentinelEndsScanning = true;

	}
	#endregion


	#region IMPLEMENTED_STRATEGY_METHOD
	public StrategyState ExecuteImplementedStrategy () {

		if (!this.sentinelComponents.enemyHasBeenStunned) {

			Debug.Log ("Sentinel is in <<Scanning>>");


			if (!this.sentinelComponents.sentinelHasEnlargedItsHearingColliders)
				this.sentinelComponents.agent.destination = this.sentinelComponents.player.transform.position;

			if (!this.sentinelComponents.sentinelIsScanning) {

				this.sentinelComponents.sentinelIsScanning = this.EnlargeHearingCollidersRadius ();
				this.scanningCoroutine = this.StartCoroutine_Auto (this.CO_ScanningCoroutine ());

			}


			if (this.sentinelComponents.playerInSight) {

				Debug.Log ("Sentinel switches from <<Scanning>> to <<Defending>>");
				this.StopScanning ();

				if (this.TriggerDefendingFeedBacks != null)
					this.TriggerDefendingFeedBacks ();

				return StrategyState.Defending;

			} else if (this.sentinelComponents.playerHasBeenHeard) {
			
				Debug.Log ("Sentinel switches from <<Scanning>> to <<Inspecting>>");
				this.StopScanning ();

				if (this.TriggerInspectingFeedBacks != null)
					this.TriggerInspectingFeedBacks ();

				return StrategyState.Inspecting;

			} else if (this.sentinelComponents.sentinelEndsScanning) {

				Debug.Log ("Sentinel switches from <<Scanning>> to <<Falling Into Line>>");
				this.StopScanning ();

				if (this.TriggerFallingIntoLineFeedBacks != null)
					this.TriggerFallingIntoLineFeedBacks ();

				return StrategyState.FallingIntoLine;

			} else {

				Debug.Log ("Sentinel does not change strategy");
				return StrategyState.NoStrategyChanging;

			}

		} else {
			
			Debug.Log ("Sentinel has been stunned");
			return StrategyState.NoStrategyChanging;

		}

	}
	#endregion

}