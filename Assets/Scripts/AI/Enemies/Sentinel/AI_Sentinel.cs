﻿using UnityEngine;
using System.Collections;

public class AI_Sentinel : GamePlayFiniteStateMachine {

	#region SENTINEL_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelGuarding> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelDefending> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelFallingIntoLine> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelInspecting> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_SentinelScanning> ());

	}
	#endregion

}