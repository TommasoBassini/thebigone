﻿using UnityEngine;
using System.Collections;

public class AI_DroneFallingIntoLine : MonoBehaviour, IAI_ImplementedGamePlayStrategy, IAI_Defending {

	#region DRONE_FALLING_INTO_LINE_PARAMETERS
	[Tooltip ("DO NOT TOUCH!")]
    public AI_DroneComponent droneComponents;

	private event EnemyFeedBackDelegate TriggerDefendingFeedBacks;
	#endregion


	#region SENTINEL_FALLING_INTO_LINE_INTERFACES_PROPERTIES
	event EnemyFeedBackDelegate IAI_Defending.FeedBacks {

		add {

			this.TriggerDefendingFeedBacks += value;

		}


		remove {

			this.TriggerDefendingFeedBacks -= value;

		}

	}
	#endregion


	#region DRONE_MONOBEHAVIOUR_METHODS
    public void Awake () {
		
        this.droneComponents = this.GetComponent <AI_DroneComponent> ();

    }
	#endregion


	#region DRONE_METHODS
    public bool ReturnToPatrol () {

		if (!this.droneComponents.isPathRandomized) {

			if (this.droneComponents.destPoint == 0)
				this.droneComponents.destPoint = this.droneComponents.points.Length - 1;
			
			else
				this.droneComponents.destPoint--;
				
		}


        this.droneComponents.agent.destination = this.droneComponents.points [this.droneComponents.destPoint].position;
        return true;

    }
	#endregion


    #region IMPLEMENTED_STRATEGY_METHOD
    public StrategyState ExecuteImplementedStrategy () {

		if (!this.droneComponents.enemyHasBeenStunned) {

			Debug.Log ("Drone is in <<Falling Into Line>>");


			if (!this.droneComponents.droneIsFallingIntoLine)
				this.droneComponents.droneIsFallingIntoLine = this.ReturnToPatrol ();


			if (this.droneComponents.playerInSight) {

				Debug.Log ("Drone switches from <<Falling Into Line>> to <<Defending>>");
				this.droneComponents.droneIsFallingIntoLine = false;

				if (this.TriggerDefendingFeedBacks != null)
					this.TriggerDefendingFeedBacks ();

				return StrategyState.Defending;

			} else if (this.droneComponents.agent.remainingDistance < 1f) {

				Debug.Log ("Drone switches from <<Falling Into Line>> to <<Guarding>>");
				this.droneComponents.droneIsFallingIntoLine = false;
				return StrategyState.Guarding;

			} else {

				Debug.Log ("Drone does not change strategy");
				return StrategyState.NoStrategyChanging;

			}

		} else {
			
			Debug.Log ("Drone has been stunned");
			return StrategyState.NoStrategyChanging;

		}

	}
	#endregion

}