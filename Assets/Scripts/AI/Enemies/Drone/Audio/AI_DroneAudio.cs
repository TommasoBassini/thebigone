﻿using UnityEngine;
using System.Collections;

public class AI_DroneAudio : AudioFiniteStateMachine {

	#region DRONE_AUDIO_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneGuardingAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneDefendingAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneFallingIntoLineAudio> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneInspectingAudio> ());

	}
	#endregion

}