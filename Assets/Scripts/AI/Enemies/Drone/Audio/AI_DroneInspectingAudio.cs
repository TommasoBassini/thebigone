﻿using UnityEngine;
using System.Collections;

public class AI_DroneInspectingAudio : AI_StateFeedBack <IAI_Inspecting>, IAI_ImplementedAudioStrategy {

	#region FEEDBACK_AUDIO_METHODS
	protected override void InitializeFeedBacks (IAI_Inspecting[] entrances) {

		foreach (IAI_Inspecting entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_AUDIO_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Inspecting Audio");
			this.CleanTriggers ();

		}

	}
	#endregion

}