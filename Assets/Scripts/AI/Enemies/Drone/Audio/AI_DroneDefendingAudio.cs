﻿using UnityEngine;
using System.Collections;

public class AI_DroneDefendingAudio : AI_StateFeedBack <IAI_Defending>, IAI_ImplementedAudioStrategy {

	#region FEEDBACK_AUDIO_METHODS
	protected override void InitializeFeedBacks (IAI_Defending[] entrances) {

		foreach (IAI_Defending entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_AUDIO_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Defending Audio");
			this.CleanTriggers ();

		}

	}
	#endregion

}