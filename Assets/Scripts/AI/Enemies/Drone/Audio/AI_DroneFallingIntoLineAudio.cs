﻿using UnityEngine;
using System.Collections;

public class AI_DroneFallingIntoLineAudio : AI_StateFeedBack <IAI_FallingIntoLine>, IAI_ImplementedAudioStrategy {

	#region FEEDBACK_AUDIO_METHODS
	protected override void InitializeFeedBacks (IAI_FallingIntoLine[] entrances) {

		foreach (IAI_FallingIntoLine entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_AUDIO_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test FallingIntoLine Audio");
			this.CleanTriggers ();

		}

	}
	#endregion

}