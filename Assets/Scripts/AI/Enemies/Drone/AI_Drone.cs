﻿using UnityEngine;
using System.Collections;

public class AI_Drone : GamePlayFiniteStateMachine {

	#region DRONE_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_DroneGuarding> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_DroneDefending> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_DroneFallingIntoLine> ());
		this.enemyReference.enemyStrategyList.Add (this.GetComponent <AI_DroneInspecting> ());

	}
	#endregion

}