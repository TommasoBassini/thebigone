﻿using UnityEngine;
using System.Collections;

public class AI_DroneLight : LightFiniteStateMachine {

	#region DRONE_LIGHT_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		base.Awake ();

		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneGuardingLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneDefendingLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneFallingIntoLineLight> ());
		this.enemyFeedBackReference.enemyStrategyList.Add (this.GetComponent <AI_DroneInspectingLight> ());

	}
	#endregion

}