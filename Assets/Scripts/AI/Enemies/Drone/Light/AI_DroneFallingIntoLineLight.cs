﻿using UnityEngine;
using System.Collections;

public class AI_DroneFallingIntoLineLight : AI_StateFeedBack <IAI_FallingIntoLine>, IAI_ImplementedLightStrategy {

	#region FEEDBACK_LIGHT_METHODS
	protected override void InitializeFeedBacks (IAI_FallingIntoLine[] entrances) {

		foreach (IAI_FallingIntoLine entrance in entrances)
			entrance.FeedBacks += this.TriggerFeedBack;

	}
	#endregion


	#region IMPLEMENTED_LIGHT_STRATEGY_METHOD
	public void ExecuteImplementedStrategy (StrategyState previousStrategy) {

		if (this.feedBackIsTriggered) {

			Debug.Log ("Test Falling Into Line Light");
			this.CleanTriggers ();

		}

	}
	#endregion

}