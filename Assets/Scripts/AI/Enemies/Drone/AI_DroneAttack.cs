﻿using UnityEngine;
using System.Collections;

public class AI_DroneAttack : MonoBehaviour, IAI_Attack {

	#region DRONE_ATTACK_DELEGATES
	public EnemyTriggerDelegate <AI_DroneAttack> DelegatedMethod = delegate (AI_DroneAttack droneAttackReference, Collider playerCollider) {

		float angle;
		Vector3 distance;
		GameObject projectile;

		Debug.LogWarning ("Shooting!");

		distance = playerCollider.transform.position - droneAttackReference.transform.position;
		angle = Vector3.Angle (distance, droneAttackReference.transform.forward);

		projectile = Instantiate <GameObject> (droneAttackReference.enemyProjectile);

		projectile.transform.position = droneAttackReference.transform.position;
		projectile.transform.LookAt (playerCollider.transform);

		projectile.transform.position += projectile.transform.forward;

		if (droneAttackReference.TriggerAttackFeedBacks != null)
			droneAttackReference.TriggerAttackFeedBacks ();

	};
	#endregion


	#region DRONE_ATTACK_PARAMETERS
	[Header ("Variables")]

	[Tooltip ("DO NOT TOUCH!")]
	public float angle;


	[Header ("Structs")]

	[Tooltip ("DO NOT TOUCH!")]
	public Vector3 distance;


	[Header ("Classes")]

	[Tooltip ("DO NOT TOUCH!")]
	public Coroutine attackCoroutine;
	[Tooltip ("DO NOT TOUCH!")]
	public Collider playerCollider;
	[Tooltip ("DO NOT TOUCH!")]
	public LineRenderer attackRay;


	[Header ("Scripts")]

	[Tooltip ("DO NOT TOUCH!")]
	public AI_DroneComponent droneComponents;


	[Header ("Prefabs")]

	[Tooltip ("DO NOT MODIFY!")]
	public GameObject enemyProjectile;


	private event EnemyFeedBackDelegate TriggerPreAttackingFeedBacks;
	private event EnemyFeedBackDelegate TriggerPostAttackingFeedBacks;
	private event EnemyFeedBackDelegate TriggerAttackFeedBacks;
	#endregion


	#region DRONE_ATTACK_INTERFACES_PROPERTIES
	public event EnemyFeedBackDelegate StartingFeedBacks {

		add {

			this.TriggerPreAttackingFeedBacks += value;

		}


		remove {

			this.TriggerPreAttackingFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate EndingFeedBacks {

		add {

			this.TriggerPostAttackingFeedBacks += value;

		}


		remove {

			this.TriggerPostAttackingFeedBacks -= value;

		}

	}


	public event EnemyFeedBackDelegate AttackFeedBacks {

		add {

			this.TriggerAttackFeedBacks += value;

		}


		remove {

			this.TriggerAttackFeedBacks -= value;

		}

	}
	#endregion


	#region DRONE_ATTACK_MONOBEHAVIOUR_METHODS
	public void Awake () {

		this.playerCollider = GameObject.FindGameObjectWithTag ("Player").GetComponent <Collider> ();
		this.attackRay = this.GetComponent <LineRenderer> ();
		this.droneComponents = this.GetComponentInParent <AI_DroneComponent> ();

	}


	public void Start () {

		this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);

	}


	public void Update () {

		this.attackRay.SetPosition (0, this.transform.position);
		this.attackRay.SetPosition (1, this.transform.position + (this.droneComponents.col.radius * this.transform.forward));

		if (this.droneComponents.inputCheckingCoroutine != null) {

			this.distance = this.droneComponents.player.transform.position - this.droneComponents.transform.position;
			this.angle = Vector3.Angle (this.distance, this.droneComponents.transform.forward);

			if (this.angle < this.droneComponents.fieldOfViewAngle * 0.5f) {

				if (this.distance.sqrMagnitude < Mathf.Pow (this.droneComponents.attackDistance, 2f)) {

					this.droneComponents.StopAgent ();

					if (this.droneComponents.agent.velocity == Vector3.zero) {

						if (this.angle > this.droneComponents.angularDegreeSpeed) {

							this.distance.y = this.droneComponents.transform.position.y;

							if (Vector3.Dot (Vector3.Cross (this.droneComponents.transform.forward, this.distance), Vector3.up) > 0f)
								this.droneComponents.transform.eulerAngles += Quaternion.AngleAxis (this.droneComponents.angularDegreeSpeed * Time.deltaTime, Vector3.up).eulerAngles;
							else
								this.droneComponents.transform.eulerAngles += Quaternion.AngleAxis (this.droneComponents.angularDegreeSpeed * Time.deltaTime, Vector3.down).eulerAngles;

						} else {

							this.droneComponents.transform.LookAt (new Vector3 (this.droneComponents.player.transform.position.x,
								this.droneComponents.transform.position.y, this.droneComponents.player.transform.position.z));

						}

					}

				} else if (this.droneComponents.agentHasBeenStopped)
					this.droneComponents.ResumeAgent ();

			}

		}

		if (this.droneComponents.enemyMayAttack && this.droneComponents.playerInSight) {
			
			if (this.attackCoroutine == null) {
				
				this.attackCoroutine = this.StartCoroutine_Auto (this.CO_Attack (this.droneComponents.attackTime, this.DelegatedMethod));

				if (this.TriggerPreAttackingFeedBacks != null)
					this.TriggerPreAttackingFeedBacks ();

			}
			
		} else {
			
			this.attackCoroutine = this.KillPreviousCoroutine (this.attackCoroutine);

			if (this.TriggerPostAttackingFeedBacks != null)
				this.TriggerPostAttackingFeedBacks ();
			
		}

	}
	#endregion


	#region DRONE_ATTACK_METHODS
	public Coroutine KillPreviousCoroutine (Coroutine coroutine) {

		if (coroutine != null)
			this.StopCoroutine (coroutine);

		return null;

	}
	#endregion


	#region DRONE_ATTACK_COROUTINES
	public IEnumerator CO_Attack (float attackTime, EnemyTriggerDelegate <AI_DroneAttack> DelegatedMethod) {

		while (true) {

			yield return new WaitForSeconds (attackTime);
			DelegatedMethod (this, this.playerCollider);

		}

	}
	#endregion

}