﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Enum used to establish the AIs' states
public enum StrategyState : byte {NoStrategyChanging, Guarding, Defending, FallingIntoLine, Inspecting, Scanning};


//AI Delegates declaration
#region ENEMY_DELEGATES
public delegate void EnemyFeedBackDelegate ();
public delegate void EnemyStateDelegate <TEnemy> (TEnemy enemyReference) where TEnemy : MonoBehaviour;
public delegate void EnemyTriggerDelegate <TEnemy> (TEnemy enemyReference, Collider other) where TEnemy : MonoBehaviour;
public delegate IEnumerator CO_EnemyCoroutineDelegate <TEnemy> (float waitingTime, EnemyStateDelegate <TEnemy> enemyReference) where TEnemy : MonoBehaviour;
#endregion


//Interfaces used for AIs' Strategy Pattern Implementation
#region ENEMY_INTERFACES
#region ENEMY_STRATEGIES
public interface IAI_ImplementedGamePlayStrategy {

	StrategyState ExecuteImplementedStrategy ();

}


public interface IAI_ImplementedAudioStrategy {
	
	void ExecuteImplementedStrategy (StrategyState previousStrategy);
	
}


public interface IAI_ImplementedLightStrategy {

	void ExecuteImplementedStrategy (StrategyState previousStrategy);

}
#endregion


#region STATE_INTERFACES
public interface IAI_Guarding {

	event EnemyFeedBackDelegate FeedBacks;

}


public interface IAI_Defending {

	event EnemyFeedBackDelegate FeedBacks;

}


public interface IAI_FallingIntoLine {

	event EnemyFeedBackDelegate FeedBacks;

}


public interface IAI_Inspecting {

	event EnemyFeedBackDelegate FeedBacks;

}


public interface IAI_Scanning {

	event EnemyFeedBackDelegate FeedBacks;

}
#endregion


public interface IAI_Area {

	event EnemyFeedBackDelegate StartFeedBacks;
	event EnemyFeedBackDelegate EndFeedBacks;

}


public interface IAI_Attack {

	event EnemyFeedBackDelegate StartingFeedBacks;
	event EnemyFeedBackDelegate EndingFeedBacks;
	event EnemyFeedBackDelegate AttackFeedBacks;

}


public interface IAI_SpecialTrigger {

	bool setSpecialTriggering { set; }

}


public interface IAI_FiniteStateMachineFeedBacks {

	event EnemyFeedBackDelegate FeedBacks;

}


public interface IAI_FiniteStateMachineStrategy {

	StrategyState currentStrategy { get; }

}


public interface IAI_ObserverFiniteStateMachine : IAI_FiniteStateMachineFeedBacks, IAI_FiniteStateMachineStrategy {}
#endregion


//Classes used for common initialization puproses
#region ENEMY_CLASSES
public class EnemyReference <TAI_Strategy> {
	
	//Enum
	public StrategyState enemyStrategyState;
	
	//Interface
	public TAI_Strategy enemyImplementedStrategy;

	//List of Classes
	public List <TAI_Strategy> enemyStrategyList;

}


public class EnemyFeedBackReference <TAI_Strategy> : EnemyReference <TAI_Strategy> {

	//Enum
	public StrategyState previousEnemyStrategyState;

}
#endregion