﻿using UnityEngine;
using System.Collections;

public abstract class AI_StateFeedBack <TAI_State> : AI_FeedBack {

	#region STATE_FEEDBACK_AUDIO_MONOBEHAVIOUR_METHODS
	protected override void Awake () {

		TAI_State[] entrances = this.GetComponentsInParent <TAI_State> ();
		this.InitializeFeedBacks (entrances);

	}
	#endregion


	#region STATE_FEEDBACK_AUDIO_METHODS
	protected abstract void InitializeFeedBacks (TAI_State[] entrances);
	#endregion

}