﻿using UnityEngine;
using System.Collections;

public abstract class AI_FeedBack : MonoBehaviour {

	#region FEEDBACK_AUDIO_PARAMETERS
	protected bool feedBackIsTriggered;
	#endregion


	#region FEEDBACK_AUDIO_MONOBEHAVIOUR_METHODS
	protected abstract void Awake ();


	protected virtual void Start () {

		this.CleanTriggers ();

	}
	#endregion


	#region FEEDBACK_AUDIO_METHODS
	protected virtual void TriggerFeedBack () {

		this.feedBackIsTriggered = true;

	}


	protected virtual void CleanTriggers () {

		this.feedBackIsTriggered = false;

	}
	#endregion

}