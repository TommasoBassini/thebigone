﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum LightBehavior
{
    nothing,
    broken,
    rotating,
    pulse
}

public class LightSetting : MonoBehaviour
{
    public LightBehavior lightBehavior;
    public float speed;
    private float startIntensity;

    public bool antiClockwise;

    void Start()
    {
        CheckLight();
    }

    public void CheckLight()
    {
        if (GetComponent<Light>())
        {
            if (GetComponent<Light>().isActiveAndEnabled)
            {
                switch (lightBehavior)
                {
                    case LightBehavior.broken:
                        {
                            StartCoroutine(BrokenLightCO());
                            break;
                        }
                    case LightBehavior.rotating:
                        {
                            StartCoroutine(RotatingLightCO());
                            break;
                        }
                    case LightBehavior.pulse:
                        {
                            startIntensity = GetComponent<Light>().intensity;
                            StartCoroutine(PulsingLightCO());
                            break;
                        }
                    default:
                        break;
                }
            }
            else
            {
                TurnOffLight();
            }
        }
        else
        {
            Light[] lights = GetComponentsInChildren<Light>();
            foreach (Light light in lights)
            {
                if (!light.gameObject.isStatic)
                {
                    light.gameObject.AddComponent<LightSetting>();
                    light.GetComponent<LightSetting>().speed = speed;
                    light.GetComponent<LightSetting>().lightBehavior = lightBehavior;
                }
            }
        }
    }

    public void TurnOffLight()
    {
        if (GetComponentInChildren<CanvasGroup>())
        {
            GetComponentInChildren<CanvasGroup>().alpha = 0f;
        }
        if (GetComponentInChildren<MeshRenderer>())
        {
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
        }
        if (GetComponentInChildren<ParticleSystem>() && GetComponentInChildren<ParticleSystem>().isPlaying)
        {
            GetComponentInChildren<ParticleSystem>().Stop();
        }
    }

    public void TurnOnLight()
    {
        if (GetComponentInChildren<CanvasGroup>())
        {
            GetComponentInChildren<CanvasGroup>().alpha = 1f;
        }
        if (GetComponentInChildren<MeshRenderer>())
        {
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", GetComponent<Light>().color);
        }
        if (GetComponentInChildren<ParticleSystem>() && GetComponentInChildren<ParticleSystem>().isStopped)
        {
            GetComponentInChildren<ParticleSystem>().Play();
        }
    }

    IEnumerator BrokenLightCO()
    {
        Light light = GetComponent<Light>();
        CanvasGroup canvas = null;
        MeshRenderer mesh = null;
        ParticleSystem particle = null;
        if (GetComponentInChildren<CanvasGroup>())
        {
            canvas = GetComponentInChildren<CanvasGroup>();
        }
        if (GetComponentInChildren<MeshRenderer>())
        {
            mesh = GetComponentInChildren<MeshRenderer>();
        }
        if (GetComponentInChildren<ParticleSystem>())
        {
            particle = GetComponentInChildren<ParticleSystem>();
        }
        while (true)
        {
            light.enabled = Mathf.PerlinNoise(Time.time * speed, 0) > 0.5f;

            if (canvas)
            {
                canvas.alpha = Mathf.PerlinNoise(Time.time * speed, 0) > 0.5f ? 1f : 0f;
            }
            if (mesh)
            {
                mesh.material.SetColor("_EmissionColor", Mathf.PerlinNoise(Time.time * speed, 0) > .5f ? light.color : Color.black);
            }
            if (particle)
            {
                if (Mathf.PerlinNoise(Time.time * speed, 0) > 0.5f)
                {
                    particle.gameObject.SetActive(true);
                    //mettere suono luce rotta
                }
                else
                {
                    particle.gameObject.SetActive(false);
                }
            }
            yield return null;
        }
    }

    IEnumerator RotatingLightCO()
    {
        if (!antiClockwise)
        {
            while (true)
            {
                this.transform.localEulerAngles += new Vector3(0, 1 * speed * Time.deltaTime, 0);
                yield return null;
            }
        }
        else
        {
            while (true)
            {
                this.transform.localEulerAngles += new Vector3(0, -1 * speed * Time.deltaTime, 0);
                yield return null;
            }
        }
    }

    IEnumerator PulsingLightCO()
    {
        Light light = GetComponent<Light>();
        CanvasGroup canvas = null;
        MeshRenderer mesh = null;
        ParticleSystem particle = null;
        if (GetComponentInChildren<CanvasGroup>())
        {
            canvas = GetComponentInChildren<CanvasGroup>();
        }
        if (GetComponentInChildren<MeshRenderer>())
        {
            mesh = GetComponentInChildren<MeshRenderer>();
        }
        if (GetComponentInChildren<ParticleSystem>())
        {
            particle = GetComponentInChildren<ParticleSystem>();
        }
        bool isZero = light.intensity < (startIntensity / 2);
        float elapsedTime = 0.0f;

        if (isZero)
        {
            while (elapsedTime < speed)
            {
                light.intensity = Mathf.Lerp(0, startIntensity, (elapsedTime / speed));
                if (canvas)
                {
                    canvas.alpha = Mathf.Lerp(0, 1, (elapsedTime / speed));
                }
                if (mesh)
                {
                    mesh.material.SetColor("_EmissionColor", Color.Lerp(Color.black, light.color, (elapsedTime / speed)));
                }
                if (particle)
                {
                    ParticleSystemRenderer renderer = particle.GetComponent<ParticleSystemRenderer>();
                    renderer.material.SetColor("_TintColor", Color.Lerp(new Color(light.color.r, light.color.g, light.color.b, 0f), new Color(light.color.r, light.color.g, light.color.b, 0.1f), (elapsedTime / speed)));
                }
                elapsedTime += Time.deltaTime;
                yield return null;
            }
        }
        else
        {
            while (elapsedTime < speed)
            {
                light.intensity = Mathf.Lerp(startIntensity, 0, (elapsedTime / speed));
                if (canvas)
                {
                    canvas.alpha = Mathf.Lerp(1, 0, (elapsedTime / speed));
                }
                if (mesh)
                {
                    mesh.material.SetColor("_EmissionColor", Color.Lerp(light.color, Color.black, (elapsedTime / speed)));
                }
                if (particle)
                {
                    ParticleSystemRenderer renderer = particle.GetComponent<ParticleSystemRenderer>();
                    renderer.material.SetColor("_TintColor", Color.Lerp(new Color(light.color.r, light.color.g, light.color.b, 0.1f), new Color(light.color.r, light.color.g, light.color.b, 0f), (elapsedTime / speed)));
                }
                elapsedTime += Time.deltaTime;
                yield return null;
            }
        }
        StartCoroutine(PulsingLightCO());
    }
}
