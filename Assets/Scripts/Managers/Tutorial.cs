﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour
{

    public Transform pnlTutorial;
    public int activeTutorial;
    public int activeStep;

    public void InitializeTutorial(int n)
    {
        // Prendo sempre come riferimento il Tutorial inserito in gioco in pnlTutorials (utile per eventi relativi a prefab che si istanziano)
        Tutorial pnlTutorials = FindObjectOfType<Tutorial>();
        pnlTutorials.pnlTutorial = transform.GetChild(n);
        pnlTutorials.pnlTutorial.gameObject.SetActive(true);
        StartCoroutine(StartTutorial());
        pnlTutorials.activeStep = 1;
        pnlTutorials.activeTutorial = n;
    }

    public void CheckTutorialStep()
    {
        Tutorial pnlTutorials = FindObjectOfType<Tutorial>();
        Transform activeStepTransform = pnlTutorials.pnlTutorial.GetChild(pnlTutorials.activeStep);
        activeStepTransform.Find("pnlCheckmark/imgMark").GetComponent<CanvasGroup>().alpha = 1f;
        activeStepTransform.GetComponent<CanvasGroup>().alpha = 0.3f;

        if (pnlTutorials.activeStep < pnlTutorials.pnlTutorial.childCount - 1)
        {
            Transform newStepObj = pnlTutorials.pnlTutorial.GetChild(pnlTutorials.activeStep + 1);
            newStepObj.GetComponent<CanvasGroup>().alpha = 1f;
            pnlTutorials.activeStep++;
        }
        else
        {
            StartCoroutine(EndTutorial());
        }
    }

    IEnumerator StartTutorial()
    {
        Tutorial pnlTutorials = FindObjectOfType<Tutorial>();
        CanvasGroup tutorialCanvas = pnlTutorials.pnlTutorial.gameObject.GetComponent<CanvasGroup>();
        float elapsedTime = 0.0f;
        while (elapsedTime < 3.0f)
        {
            tutorialCanvas.alpha = Mathf.Lerp(0f, 1.0f, (elapsedTime / 1.0f));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator EndTutorial()
    {
        Tutorial pnlTutorials = FindObjectOfType<Tutorial>();
        CanvasGroup tutorialCanvas = pnlTutorials.pnlTutorial.gameObject.GetComponent<CanvasGroup>();
        float elapsedTime = 0.0f;
        while (elapsedTime < 6.0f)
        {
            if (elapsedTime >= 3.0f)
            {
                tutorialCanvas.alpha = Mathf.Lerp(1.0f, 0f, ((elapsedTime - 3.0f) / 1.0f));
            }
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        pnlTutorials.pnlTutorial.gameObject.SetActive(false);
    }
}