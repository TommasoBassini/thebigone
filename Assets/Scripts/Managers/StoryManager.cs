﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class StoryManager : MonoBehaviour
{
    public List<StoryEvent> storyEvents; // http://dfiles.eu/files/efvlkdxv4
    public List<StoryItem> storyItems;

    public void StartStoryEvent(int n)
    {
        if (!storyEvents[n].isDone)
        {
            storyEvents[n].storyEvent.Invoke();
            storyEvents[n].isDone = true;
        }
    }
}