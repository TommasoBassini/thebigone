﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class ScriptMovement : MonoBehaviour
{
    private bool activate = false;

    private Transform[] pointTransform;
    private Vector3 storePoint;

    private List<Transform> listTransform = new List<Transform>();
    public GameObjectToLook[] gameObjectToLook;
    private FirstPersonController fpsScript;
    private int destPoint = 1;
    private NavMeshAgent agent;

    public bool isLookObject = false;
    public Transform lookObject;

    bool lookNewGameobject = false;
    private int nObjToLook = 0;

    void Start()
    {
        fpsScript = GetComponent<FirstPersonController>();
        agent = GetComponent<NavMeshAgent>();
    }

    public void StartAutoMovement(GameObject go)
    {
        fpsScript.enabled = false;
        isLookObject = false;
        agent.enabled = true;
        listTransform.Add(go.transform);
        foreach (Transform item in go.transform)
        {
            listTransform.Add(item.transform);
        }

        pointTransform = listTransform.ToArray();
        listTransform.Clear();
        activate = true;
        StartCoroutine(GoToNextObj());
    }

    public void StartAutoMovement(GameObject go, GameObjectToLook[] _gameObjectToLook)
    {
        //Disabilito l'fpsController e abilito il navmesh
        fpsScript.enabled = false;
        agent.enabled = true;
        isLookObject = true;
        //prendo il go e metto in un array tutte le transform del go
        foreach (Transform item in go.transform)
        {
            listTransform.Add(item.transform);
        }
        pointTransform = listTransform.ToArray();
        listTransform.Clear();

        // prendo la struct degli oggetti da guardare
        gameObjectToLook = new GameObjectToLook[_gameObjectToLook.Length];

        for (int i = 0; i < _gameObjectToLook.Length; i++)
        {
            gameObjectToLook[i].go = _gameObjectToLook[i].go;
            gameObjectToLook[i].timeToview = _gameObjectToLook[i].timeToview;
        }

        //faccio partire il movimento automatico
        activate = true;
        StartCoroutine(GoToNextObj());
    }

    void Update()
    {
        //    if (activate)
        //    {
        //       transform.LookAt(lookObject);
        //        // Check Lenght if zero log an Error
        //        if (pointTransform.Length == 0)
        //        {
        //            Debug.LogWarning("WARNING! " + this.ToString() + " Has the transform's array length equal to zero!");
        //            return;
        //        }
        //        // Set the agent to go to the currently selected destination.
        //        if (destPoint == 0)
        //        {
        //            destPoint = 1;
        //            activate = false;
        //            fpsScript.activatePatrolPlayer = false;
        //            agent.enabled = false;
        //        }
        //        agent.destination = pointTransform[destPoint].position;
        //    }

        //bb.transform.forward = Vector3.RotateTowards(bb.transform.forward, myDir.normalized, Mathf.Deg2Rad * (Vector3.Angle(bb.transform.forward, myDir.normalized)) / 20, 0.1f);

        //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, bb.transform.rotation, tt);
    }

    IEnumerator GoToNextObj()
    {
        while (activate)
        {
            if (isLookObject)
            {
                //fpsScript.transform.LookAt(lookObject);
                if (!lookNewGameobject)
                {
                    GameObject goToLook = null;


                    if (gameObjectToLook[nObjToLook].go == null)
                    {
                        goToLook = pointTransform[destPoint].gameObject;
                    }
                    else
                    {
                        goToLook = gameObjectToLook[nObjToLook].go;
                    }

                    Debug.Log(goToLook);
                    StartCoroutine(LookObj(goToLook, gameObjectToLook[nObjToLook].timeToview));
                }
            }
            else
            {
                Vector3 myDir = pointTransform[destPoint].position - fpsScript.transform.position;
                fpsScript.transform.forward = Vector3.RotateTowards(fpsScript.transform.forward, myDir.normalized, 0.1f, 0.1f);
            }
            
            // Check Lenght if zero log an Error
            if (pointTransform.Length == 0)
            {
                Debug.LogWarning("WARNING! " + this.ToString() + " Has the transform's array length equal to zero!");
                StopAllCoroutines();
            }

            // Set the agent to go to the currently selected destination.
            if (destPoint == 0)
            {
                destPoint = 1;
                fpsScript.enabled = true;
                agent.enabled = false;
                activate = false;
                gameObjectToLook = new GameObjectToLook[0];
                nObjToLook = 0;
                pointTransform = new Transform[0];
            }
            else
                agent.destination = pointTransform[destPoint].position;
            yield return null;
        }
    }

    IEnumerator LookObj(GameObject goToLook, float timeToLook)
    {
        lookNewGameobject = true;
        float elapsedTime = 0.0f;
        while (elapsedTime < timeToLook)
        {
            Vector3 myDir = goToLook.transform.position - this.transform.position;
            this.transform.forward = Vector3.RotateTowards(this.transform.forward, myDir.normalized, 0.1f, 0.1f);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        nObjToLook++;
        lookNewGameobject = false;
        if (nObjToLook >= gameObjectToLook.Length)
        {
            isLookObject = false;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "PatrolPoint" && activate)
        {
            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            destPoint = (destPoint + 1) % pointTransform.Length;
        }
        return;
    }


}

