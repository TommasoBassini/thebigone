﻿using UnityEngine;
using System.Collections;

public class Initializer : MonoBehaviour
{
    void Start ()
    {
        SubtitlesHandler.ReadCSV();

        // Inizializzo tutte le point source di SECTR a tempo
        foreach (SECTR_PointSource pointSource in FindObjectsOfType<SECTR_PointSource>())
        {
            GeneralEvents generalEvents = pointSource.GetComponent<GeneralEvents>();
            if (generalEvents)
            {
                generalEvents.timedEvents[0].timedEvent.Invoke();
            }
        }
    }
}