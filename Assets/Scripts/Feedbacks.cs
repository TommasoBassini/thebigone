﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;


public class Feedbacks : MonoBehaviour
{
    [Header("Feedback")]
    public Texts[] texts;
    public Images[] images;
    public Audios[] audios;
    public Animations[] animations;
    public Lights[] lights;
    public Gameobjects[] gameobjects;
    public Subtitles[] subtitles;
    public UpgradePermission[] upgradePermission;
    public Particles[] particles;
    public AutoMovePlayer autoMovePlayer;

    private AsyncOperation async;
    public SceneLoader[] sceneLoader;
    private Scene nextScene;
    private GameObject txtSubtitles;
    private FirstPersonController fpsController;

    void Start()
    {
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].textString = texts[i].textString.Replace("<br>", "\n");
        }
        txtSubtitles = FindObjectOfType<MenuControl>().transform.Find("pnlMainArea/pnlSubtitles/txtSubtitles").gameObject;
        fpsController = FindObjectOfType<FirstPersonController>();
    }

    public void ChangeText(int n)
    {
        if (texts[n].textColors != new Color(0, 0, 0, 0))
        {
            texts[n].textToChange.color = texts[n].textColors;
        }

        if (texts[n].textString.Length != 0)
        {
            texts[n].textToChange.text = texts[n].textString;
        }
    }

    public void ChangeImage(int n)
    {
        if (images[n].imageSprite != null)
        {
            images[n].imageToChange.sprite = images[n].imageSprite;
        }

        if (images[n].imageColor != new Color(0, 0, 0, 0))
        {
            images[n].imageToChange.color = images[n].imageColor;
        }
        if (images[n].imageToChange.fillAmount != images[n].imageFill)
        {
            if (images[n].timerToFill > 0)
            {
                if (!images[n].isWorking)
                {
                    StartCoroutine(FillImageOverTime(n));
                }
            }
            else
            {
                images[n].imageToChange.fillAmount = images[n].imageFill;
            }
        }
    }

    public IEnumerator FillImageOverTime(int n)
    {
        images[n].isWorking = true;
        float time = images[n].timerToFill;
        float elapsedTime = 0.0f;
        float startFill = images[n].imageToChange.fillAmount;
        while (elapsedTime < time)
        {
            images[n].imageToChange.fillAmount = Mathf.Lerp(startFill, images[n].imageFill, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        images[n].isWorking = false;
    }

    public void PlayAudio(int n)
    {
        AudioSource audioSource = GetComponent<AudioSource>();

        audioSource.Stop();
        audioSource.PlayOneShot(audios[n].audioClip);
    }

    public void PlayAnimation(int n)
    {
        animations[n].objAnimator.SetTrigger(animations[n].triggerName);
    }

    public void LightOnOff(int n)
    {
        Light[] lightList = lights[n].lightsParent.GetComponentsInChildren<Light>();

        foreach (var light in lightList)
        {
            LightSetting lightSetting = light.GetComponent<LightSetting>();
            if (light.enabled)
            {
                light.enabled = false;
                lightSetting.TurnOffLight();
                continue;
            }
            else if (!light.enabled)
            {
                light.enabled = true;
                lightSetting.TurnOnLight();
                continue;
            }
        }
    }

    public void ParticleOnOff(int n)
    {
        ParticleSystem[] particlesList = particles[n].particlesParent.GetComponentsInChildren<ParticleSystem>();

        foreach (var particle in particlesList)
        {
            if (particle.isStopped)
            {
                particle.Play();
                continue;
            }
            else if (particle.isPlaying)
            {
                particle.Stop();
                continue;
            }
        }
    }

    public void SetGameobject(int n)
    {
        bool objStatus = gameobjects[n].gameobject.activeInHierarchy;

        gameobjects[n].gameobject.SetActive(!objStatus);
    }

    public void ToggleCollider(int n)
    {
        bool objStatus = gameobjects[n].gameobject.GetComponent<Collider>().enabled;

        gameobjects[n].gameobject.GetComponent<Collider>().enabled = !objStatus;
    }

    public void FlashGameobject(int n)
    {
        if (!gameobjects[n].isWorking)
        {
            StartCoroutine(FlashGameobjectCO(n));
        }
    }

    IEnumerator FlashGameobjectCO(int n)
    {
        gameobjects[n].isWorking = true;
        bool initialState = gameobjects[n].gameobject.activeInHierarchy;
        for (int i = 0; i < (gameobjects[n].nSecondiFlesh * 2); i++)
        {
            gameobjects[n].gameobject.SetActive(!gameobjects[n].gameobject.activeInHierarchy);
            yield return new WaitForSeconds(0.5f);
        }
        gameobjects[n].gameobject.SetActive(initialState);
        gameobjects[n].isWorking = false;
    }

    public void ChangeTerminalPanel(GameObject panelToShow)
    {
        TerminalStatus canvas = GetComponent<TerminalStatus>();

        if (canvas)
        {

            foreach (var panel in canvas.panels)
            {
                if (panel.panel.activeInHierarchy)
                {

                    canvas.orderOfLastPanel.Add(panel);
                    canvas.activePanel.SetActive(false);
                    break;
                }
            }

            panelToShow.SetActive(true);
            canvas.activePanel = panelToShow;

            foreach (var panel in canvas.panels)
            {
                if (panel.panel.activeInHierarchy)
                {

                    if (panel.firstSelectButtonInPanel != null)
                    {
                        panel.firstSelectButtonInPanel.Select();
                    }
                    break;
                }
            }
        }
    }

    public void ChangeScene(int n)
    {
        StartCoroutine(ChangeSceneCO(n));
    }

    IEnumerator ChangeSceneCO(int n)
    {
        GameObject player = FindObjectOfType<PlayerStatus>().gameObject;
        Rigidbody player_r = player.GetComponent<Rigidbody>();
        player_r.useGravity = false;
        yield return new WaitForSeconds(5);
        Image loadScreen = FindObjectOfType<MenuControl>().transform.Find("ChangeSceneImage").GetComponent<Image>();
        loadScreen.sprite = sceneLoader[n].imageLoad;
        float elapsedTime = 0.0f;
        while (elapsedTime < 1.0f)
        {
            float alpha = Mathf.Lerp(0f, 1.0f, (elapsedTime / 1.0f));
            loadScreen.color = new Color(loadScreen.color.r, loadScreen.color.g, loadScreen.color.b, alpha);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        player.transform.position = sceneLoader[n].newScenePlayerPosition;
        async = SceneManager.LoadSceneAsync(sceneLoader[n].nScena,LoadSceneMode.Additive);
        nextScene = SceneManager.GetSceneByName(sceneLoader[n].nomeScena);
        async.allowSceneActivation = false;
        if (nextScene.IsValid())
        {
            StartCoroutine(LoadScene(n));
        }
    }

    IEnumerator LoadScene(int n)
    {
        GameObject player = FindObjectOfType<PlayerStatus>().gameObject;

        Scene activeScene = SceneManager.GetActiveScene();
        async.allowSceneActivation = true;

        Rigidbody player_r = player.GetComponent<Rigidbody>();
        player_r.useGravity = true;
        Image loadScreen = FindObjectOfType<MenuControl>().transform.Find("ChangeSceneImage").GetComponent<Image>();
        float elapsedTime = 0.0f;
        while (elapsedTime < 1.0f)
        {
            float alpha = Mathf.Lerp(1f, 0f, (elapsedTime / 1.0f));
            loadScreen.color = new Color(loadScreen.color.r, loadScreen.color.g, loadScreen.color.b, alpha);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        SceneManager.SetActiveScene(nextScene);
        SceneManager.UnloadScene(activeScene);
    }

    public void GeneralButton(int n)
    {
        GetComponentInParent<TerminalEvents>().generalFeedbackEvent.generalEvent[n].Invoke();
    }

    public void ExitFromTerminal()
    {
        ObjectInteract objectInteract = FindObjectOfType<ObjectInteract>();
        objectInteract.ExitFromTerminal();
    }

    public void NoHelmetCountdown()
    {
        FindObjectOfType<PlayerStatus>().NoOxygen();
    }

    public void HaveOxygen()
    {
        FindObjectOfType<PlayerStatus>().TakeHelmet();
    }

    public void ReturnBack(GameObject targetRotGameobject)
    {
        StartCoroutine(ReturnBackCO(targetRotGameobject));
    }

    IEnumerator ReturnBackCO(GameObject playerRot)
    {
        FirstPersonController player = FindObjectOfType<FirstPersonController>();
        player.stopGetInput = true;
        player.m_Input = new Vector2(0, 0);
        float elapsedTime = 0.0f;
        Quaternion startPos = player.transform.rotation;

        while (elapsedTime < 1.5f)
        {
            //player.transform.eulerAngles = new Vector3(player.transform.eulerAngles.x, Mathf.Lerp(startPos,(float)playerRot, (elapsedTime / 1.5f)), player.transform.eulerAngles.x);
            player.transform.rotation = Quaternion.Slerp(startPos, playerRot.transform.rotation, elapsedTime / 1.5f);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        Vector3 startMovementPos = player.transform.position;
        Vector3 endMovementPos = player.transform.position + (player.transform.forward * 4);
        float elapsedTimeMove = 0.0f;
        player.m_Input = new Vector2(0, 1);

        while (elapsedTimeMove < 0.5f)
        {
            elapsedTimeMove += Time.deltaTime;
            yield return null;
        }
        player.stopGetInput = false;
    }

    public void OxygenControl(bool status)
    {
        OxygenScript os = FindObjectOfType<OxygenScript>();
        if (status)
            os.Oxygen();
        else
            os.NoOxygen();
    }

    public void SetReminderText(string textToInsert)
    {
        GameObject textPanel = Instantiate(Resources.Load<GameObject>("PanelTextReminder"));
        GameObject pnlReminders = FindObjectOfType<MenuControl>().reminderPanel;
        textPanel.transform.SetParent(pnlReminders.transform);
        textPanel.transform.SetAsFirstSibling();
        textPanel.GetComponent<ReminderPanelFitter>().InitializeReminderText(textToInsert);
        // mettere suono che dice "reminder aggiunto"?
    }

    public void SendPodMessage(string text)
    {
        MessageController mc = FindObjectOfType<MessageController>();
        GameObject podMessage = Instantiate(mc.messagePrefab);
        podMessage.SetActive(false);
        podMessage.transform.SetParent(mc.gameObject.transform);
        RectTransform rc = podMessage.GetComponent<RectTransform>();
        rc.anchorMin = Vector2.zero;
        rc.anchorMax = Vector2.zero;
        rc.anchoredPosition = mc.podMessagePosition;
        rc.transform.GetChild(0).gameObject.GetComponent<Text>().text = text;
        podMessage.SetActive(true);
        mc.QueueLeftMessage(podMessage);
        // mettere suono pod message?
    }

    public void SendSuitMessage(string text)
    {
        MessageController mc = FindObjectOfType<MessageController>();
        GameObject podMessage = Instantiate(mc.messagePrefab);
        podMessage.SetActive(false);
        podMessage.transform.SetParent(mc.gameObject.transform);
        RectTransform rc = podMessage.GetComponent<RectTransform>();
        rc.anchorMin = new Vector2(1, 0);
        rc.anchorMax = new Vector2(1, 0);
        rc.anchoredPosition = mc.suitMessagePosition;
        rc.transform.GetChild(0).gameObject.GetComponent<Text>().text = text;
        podMessage.SetActive(true);
        mc.QueueRightMessage(podMessage);
        // mettere suono suit message?
    }

    public void StartSubtitles(int n)
    {
        StartCoroutine(SubtitlesHandler.subtitlesEvent(subtitles[n], txtSubtitles, fpsController.transform.Find("FirstPersonCharacter/DialoguesSource").GetComponent<AudioSource>()));
    }

    public void Debugga(string text)
    {
        Debug.Log(text);
    }

    public void ChangePermission(int n)
    {
        TerminalEvents terminalEvents = GetComponentInParent<TerminalEvents>();
        if (!upgradePermission[n].isDone)
        {
            PlayerStatus playerStatus = FindObjectOfType<PlayerStatus>();
            upgradePermission[n].isDone = true;
            terminalEvents.feedbackEventUpgrade.upgradedPodEvent.Invoke();

            switch (upgradePermission[n].permission)
            {
                case SecurityType.medic:
                    {
                        playerStatus.medicLvl = upgradePermission[n].level;
                        break;
                    }
                case SecurityType.engineer:
                    {
                        playerStatus.engineerLvl = upgradePermission[n].level;
                        break;
                    }
                case SecurityType.guard:
                    {
                        playerStatus.guardLvl = upgradePermission[n].level;
                        break;
                    }
            }
        }
        else
        {
            terminalEvents.feedbackEventUpgrade.alreadyUpgradedEvent.Invoke();
        }
    }

    public void ChangeLightStatus(int n)
    {
        LightSetting lightSetting = lights[n].lightsParent.GetComponent<LightSetting>();
        lightSetting.lightBehavior = lights[n].lightBehaviour;
        lightSetting.CheckLight();
    }

    public void StartAutoMovement()
    {
        ScriptMovement sm = FindObjectOfType<ScriptMovement>();
        
        if (autoMovePlayer.lookOtherGameobject)
        {
            sm.StartAutoMovement(autoMovePlayer.parentTransform, autoMovePlayer.gameObjectToLook);
        }
        else
        {
            sm.StartAutoMovement(autoMovePlayer.parentTransform);
        }
    }
}
