﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class TerminalCode : MonoBehaviour
{
    public TerminalStatus ts;
    public string currentPin;
    public string passText;
    public Text pinText;

    [Header("Per Designer")]
    public string[] unlockPin;
    public bool showNumber;
    public int passLenght;
    public char symbolHiddenCode;

    public void StampNumber (string n)
    {
        if (passText.Length < passLenght * 3)
        {
            currentPin = currentPin + n;
            if (!showNumber)
                passText += " " + symbolHiddenCode + " ";
            else
                passText += " " + n + " ";

            pinText.text = passText;
        }
    }

    public void CheckPin()
    {
        bool findPin = false;
        int nEventToInvoke = 0;
        for (int i = 0; i < unlockPin.Length; i++)
        {
            if (currentPin == unlockPin[i])
            {
                nEventToInvoke = i;
                findPin = true;
            }
        }
        if (findPin)
        {
            currentPin = "";
            passText = "";
            GetComponentInParent<TerminalEvents>().feedbackEventPin.rightPinEvent[nEventToInvoke].Invoke();
        }
        else
        {
            passText = "";
            currentPin = "";
            GetComponentInParent<TerminalEvents>().feedbackEventPin.wrongPinEvent.Invoke();
        }
    }

    void Start()
    {
        ts = GetComponentInParent<TerminalStatus>();
        if (ts == null)
        {
            Debug.Log(transform.position);
            Debug.Log(transform.parent.gameObject.name);
        }
    }

    void Update()
    {
        if ((Input.GetKeyUp(KeyCode.Joystick1Button2)) && ts.isUsed)
        {
            DeleteLast();
        }
    }

    public void DeleteLast()
    {
        if (passText.Length > 0)
        {
            currentPin = currentPin.Substring(0, currentPin.Length - 1);
            passText = passText.Substring(0, passText.Length - 3);

            pinText.text = passText;
        }

    }
}
