﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public struct OneTimeOpenable
{
    public bool oneTimeOpen;
    public bool oneTimeClose;
    public UnityEvent oneTimeOpenEvent;
    public UnityEvent oneTimeCloseEvent;
}

public class OpenableControl : ActionObj
{
    public bool isOpen;
    public bool haveCode;
    public string lockerName;

    private Animator anim;
    private GameObject keyboard;

    public UnityEvent openEvent;
    public UnityEvent closeEvent;

    public OneTimeOpenable oneTimeOpenable;

    void Start()
    {
        anim = GetComponentInParent<Animator>();

        transform.Find("cnvLockerName/pnlLockerName/txtLockerName").GetComponent<Text>().text = lockerName;

        if (haveCode)
        {
            keyboard = transform.Find("keyboard").gameObject;
            keyboard.SetActive(true);
            keyboard.transform.Find("Main/pnlKeyboardLocker/txtTitle").GetComponent<Text>().text = "Armadietto - " + lockerName;
            if (isOpen)
            {
                //mettere suono apertura armadietto
                anim.SetTrigger("Inverti");
                Image status1 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus1").GetComponent<Image>();
                Image status2 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus2").GetComponent<Image>();
                status1.color = Color.green;
                status2.color = Color.green;
                keyboard.GetComponent<Collider>().enabled = false;
                actionType = ActionType.openable;
            }
            else
            {
                Image status1 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus1").GetComponent<Image>();
                Image status2 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus2").GetComponent<Image>();
                status1.color = Color.red;
                status2.color = Color.red;
                actionType = ActionType.nothing;
                //mettere suono chiusura armadietto
            }
        }
        else
        {
            actionType = ActionType.openable;
            if (isOpen)
            {
                anim.SetTrigger("Inverti");
                //mettere suono chiusura armadietto
            }
        }
	}

    public override void DoStuff()
    {
        if (isOpen)
        {
            closeEvent.Invoke();
            if (oneTimeOpenable.oneTimeClose)
            {
                oneTimeOpenable.oneTimeCloseEvent.Invoke();
            }
            isOpen = false;
            anim.SetTrigger("Inverti");

            if (haveCode)
            {
                StartCoroutine(ReactivateKeyboard());
                actionType = ActionType.nothing;
            }
        }
        else
        {
            openEvent.Invoke();
            if (oneTimeOpenable.oneTimeOpen)
            {
                oneTimeOpenable.oneTimeOpenEvent.Invoke();
            }
            isOpen = true;
            anim.SetTrigger("Inverti");
        }
    }

    public void OpenCloseWithCode()
    {
        if (isOpen)
        {
            closeEvent.Invoke();
            if (oneTimeOpenable.oneTimeClose)
            {
                oneTimeOpenable.oneTimeCloseEvent.Invoke();
            }
            isOpen = false;
            StartCoroutine(ReactivateKeyboard());
            anim.SetTrigger("Inverti");
            actionType = ActionType.nothing;
        }
        else
        {
            openEvent.Invoke();
            if (oneTimeOpenable.oneTimeOpen)
            {
                oneTimeOpenable.oneTimeOpenEvent.Invoke();
            }
            isOpen = true;
            StartCoroutine(ReactivateDoor());
            anim.SetTrigger("Inverti");
            keyboard.GetComponent<Collider>().enabled = false;
            Image status1 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus1").GetComponent<Image>();
            Image status2 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus2").GetComponent<Image>();
            status1.color = Color.green;
            status2.color = Color.green;
        }
    }

    IEnumerator ReactivateDoor()
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < 1.0f)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        actionType = ActionType.openable;
    }

    IEnumerator ReactivateKeyboard()
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < 1.0f)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        keyboard.GetComponent<Collider>().enabled = true;
        Image status1 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus1").GetComponent<Image>();
        Image status2 = keyboard.transform.Find("Main/pnlKeyboardLocker/imgStatus2").GetComponent<Image>();
        status1.color = Color.red;
        status2.color = Color.red;
    }
}
