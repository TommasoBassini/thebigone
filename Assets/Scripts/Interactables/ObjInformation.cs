﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ObjInformation : MonoBehaviour
{
    // Set della distanza della telecamera quando si ispeziona un oggetto
    [Range(0f, 1f)]
    [Tooltip("Setta la distanza dell'oggetto ispezionato dalla camera. 0 = vicino, 1 = lontano")]
    public float near;
    public Vector3 rotInInspect;

    public GameObject objToView;
    public string dati;
    public Sprite objPreview;

    public bool isScanning = false;

    public UnityEvent inspectEvent;
    public UnityEvent stopInspectEvent;
}
