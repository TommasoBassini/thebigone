﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class PodController : ActionObj
{
    public UnityEvent generalFeedbackEvent;

    public override void DoStuff()
    {
        FindObjectOfType<PlayerStatus>().TakePod();
        generalFeedbackEvent.Invoke();
        Destroy(this.gameObject);
    }
}