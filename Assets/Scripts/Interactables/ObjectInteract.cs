﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class ObjectInteract : MonoBehaviour
{
    #region Variabili
    //Roba che serve per inspezionare gli oggetti
    private Vector3 cameraPos;
    private Vector3 cameraLookPos;
    private Quaternion cameraRot;
    public GameObject pickObjParent;
    private Vector3 lastObjPos;
    private Quaternion lastObjRot;
    public GameObject pickubleObj;
    private PlayerStatus ps;
    public OxygenScript os;

    public bool isInteracting = false;
    public bool isInspecting = false;
    public bool isTerminal = false;

    public GameObject inspect;
    private TerminalStatus activeCanvas;
    public GameObject feedbackGameobject;
    public Text feedbacktext;
    public Image buttonFeedbackImage;
    public Button dummyButton;
    private Ray interactionRay;
    public GameObject torcia;
    public ScanButtonManager scan;
    private MenuControl menu;
    private float scanTime = 0.0f;
    public GameObject body;
    private bool pauseAbsorb = false;
    private RaycastTarget raycastTarget;
    private bool isCoroutineWork = false;
    private BatteryScript bs;
    public LayerMask playerLayer;
    private bool isScanning = false;
    private GameObject raycastGameobject;
    private bool isLerpPickable = false;

    [Header("Per i designer")]
    [Tooltip("Setta la distanza di interazione con gli oggetti")]
    public float dropDistance;

    [Tooltip("Setta la distanza di interazione con gli oggetti")]
    [Range(0.0f,1.0f)]
    public float LightPickable;

    [Tooltip("Setta la velocità di rotazione durante l'inspezione")]
    public int rotationSpeed;

    [Tooltip("Mettere Immagine del mirino")]
    public Image viewFinder;
    [Tooltip("Mettere Immagine delle azioni")]
    public Image actionImage;
    [Tooltip("Valore di riduzione batteria scanner")]
    public int scannerBatteryCost;

    public GameObject scanPerc;

    [Header("Input Feedback manager")]
    public ObjectInputFeedback objectInputFeedback;
    public TerminalInputFeedback terminalInputFeedback;
    public ActionButtonInputFeedback actionButtonInputFeedback;
    public ActionLeverInputFeedback actionLeverInputFeedback;
    public ActionOpenableInputFeedback actionOpenableInputFeedback;
    public AbsorbInputFeedback absorbInputFeedback;
    public ExpelInputFeedback expelbInputFeedback;
    public HelmetInputFeedback helmetInputFeedback;
    public PodInputFeedback podInputFeedback;
    public PutRechargeInputFeedback putRechargeInputFeedback;
    public TakeRechargeInputFeedback takeRechargeInputFeedback;
    public RepairInputFeedback repairInputFeedback;
    public GameObject pnlRotateObjectInput;


    #endregion
    [Range(0.0f,1.0f)]
    public float testZoom = 0.56f;
    void Start()
    {
        //Setta la posizione della telecamera all'inizio del gioco
        cameraPos = Camera.main.transform.position;
        menu = FindObjectOfType<MenuControl>();
        bs = this.GetComponent<BatteryScript>();
        os = GetComponent<OxygenScript>();
        ps = GetComponent<PlayerStatus>();
        //playerLayer = ~playerLayer;
    }

    void Update()
    {
        #region !isInteracting Input
        if (!isInteracting)
        {
            if ((Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.E)) && (!menu.isMenu) && !isCoroutineWork)
            {

                if (raycastTarget == RaycastTarget.pickable && !isLerpPickable)
                {
                    MeshRenderer mr = raycastGameobject.GetComponent<MeshRenderer>();
                    Material mat = mr.material;
                    GetComponent<FirstPersonController>().enabled = false;
                    ObjInformation objInfo = raycastGameobject.transform.gameObject.GetComponent<ObjInformation>();
                    isInteracting = true;
                    isInspecting = true;
                    mat.SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f));
                    actionImage.gameObject.SetActive(false);
                    viewFinder.gameObject.SetActive(false);
                    feedbackGameobject.SetActive(false);
                    BlurOptimized blur = GetComponentInChildren<BlurOptimized>();
                    blur.blurSize = 5;
                    blur.blurIterations = 3;
                    blur.enabled = true;
                    raycastGameobject.layer = 5;
                    foreach (Transform item in raycastGameobject.transform)
                    {
                        item.gameObject.layer = 5;
                    }
                    pickObjParent = raycastGameobject.transform.parent.gameObject;
                    lastObjPos = raycastGameobject.transform.position;
                    lastObjRot = raycastGameobject.transform.rotation;

                    inspect.transform.localPosition = new Vector3(0, 0, 0.13f) + new Vector3(0, 0, (1 * objInfo.near));

                    pickubleObj.transform.SetParent(inspect.transform);
                    StartCoroutine (LerpPickuble(pickubleObj));
                    pnlRotateObjectInput.SetActive(true);

                    objInfo.inspectEvent.Invoke();

                    if (!objInfo.isScanning)
                    {
                        StartCoroutine(ScanObj());
                    }
                }
                else if (raycastTarget == RaycastTarget.terminal)
                {
                    isInteracting = true;
                    isTerminal = true;

                    actionImage.gameObject.SetActive(false);
                    viewFinder.gameObject.SetActive(false);
                    feedbackGameobject.SetActive(false);

                    cameraRot = Camera.main.transform.rotation;
                    cameraPos = Camera.main.transform.position;
                    cameraLookPos = Camera.main.transform.position + Camera.main.transform.forward;
                    Vector3 endPos = raycastGameobject.transform.Find("Main").position + (-raycastGameobject.transform.Find("Main").transform.forward * testZoom);
                    StartCoroutine(LerpCameraMovement(endPos, raycastGameobject.transform.Find("Main").position));
                    body.SetActive(false);
                    //StartCoroutine(LerpLookAt(hit.transform.Find("Main").position));

                    GetComponent<FirstPersonController>().enabled = false;
                    activeCanvas = raycastGameobject.transform.FindChild("Main").gameObject.GetComponent<TerminalStatus>();
                    activeCanvas.isUsed = true;
                    activeCanvas.interactionEvent.Invoke();
                    foreach (var panel in activeCanvas.panels)
                    {
                        if (panel.panel.activeInHierarchy)
                        {
                            panel.firstSelectButtonInPanel.Select();
                            break;
                        }
                    }
                    torcia.SetActive(false);
                }
                else if (raycastTarget == RaycastTarget.actionButton || raycastTarget == RaycastTarget.actionHelmet || 
                         raycastTarget == RaycastTarget.actionLever || raycastTarget == RaycastTarget.actionOpenable || 
                         raycastTarget == RaycastTarget.actionPod || raycastTarget == RaycastTarget.putRecharge || 
                         raycastTarget == RaycastTarget.takeRecharge)
                {
                    raycastGameobject.GetComponent<ActionObj>().DoStuff();
                }
            }
        }
        #endregion

        #region isinteracting
        if (isInteracting)
        {
            if (isInspecting)
            {
                float angH = Input.GetAxis("RightH");
                float angV = Input.GetAxis("RightV");

                if (Input.GetAxis("RightH") > 0.25f || Input.GetAxis("RightH") < -0.25f)
                {
                    pickubleObj.transform.localEulerAngles += new Vector3(0, angH * rotationSpeed, 0);
                }
                if (Input.GetAxis("RightV") > 0.25f || Input.GetAxis("RightV") < -0.25f)
                {
                    inspect.transform.Rotate(new Vector3(-angV * rotationSpeed, 0, 0));
                }

                if ((Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Escape)) && isInspecting && (!menu || !menu.isMenu) && !isScanning && !isLerpPickable)
                {
                    pickubleObj.GetComponent<ObjInformation>().stopInspectEvent.Invoke();
                    StartCoroutine(LerpBackPickuble(pickubleObj));
                    pnlRotateObjectInput.SetActive(false);

                    if (pickObjParent != null)
                    {
                        pickubleObj.transform.SetParent(pickObjParent.transform);
                        pickObjParent = null;
                    }
                    else
                    {
                        pickubleObj.transform.SetParent(null);
                    }
                    pickubleObj.layer = 9;
                    pickubleObj = null;

                    isInspecting = false;
                    isInteracting = false;
                    inspect.transform.localEulerAngles = Vector3.zero;
                    actionImage.gameObject.SetActive(true);
                    viewFinder.gameObject.SetActive(true);
                    feedbackGameobject.SetActive(true);
                    BlurOptimized blur = GetComponentInChildren<BlurOptimized>();
                    blur.blurSize = 0;
                    blur.blurIterations = 1;
                    blur.enabled = false;
                }
            }

            if (isTerminal)
            {
                if ((Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Escape)) && !isCoroutineWork && !activeCanvas.isBlocked)
                {
                    if (activeCanvas.orderOfLastPanel.Count > 1)
                    {
                        // mettere suono back del pod
                        activeCanvas.activePanel.gameObject.SetActive(false);
                        activeCanvas.orderOfLastPanel[activeCanvas.orderOfLastPanel.Count - 1].panel.SetActive(true);
                        activeCanvas.activePanel = activeCanvas.orderOfLastPanel[activeCanvas.orderOfLastPanel.Count - 1].panel;
                        activeCanvas.orderOfLastPanel.RemoveAt(activeCanvas.orderOfLastPanel.Count - 1);
                        if (activeCanvas.orderOfLastPanel[activeCanvas.orderOfLastPanel.Count - 1].firstSelectButtonInPanel != null)
                        {
                            activeCanvas.orderOfLastPanel[activeCanvas.orderOfLastPanel.Count - 1].firstSelectButtonInPanel.Select();
                        }
                    }
                    else
                    {
                        // mettere suono uscita dal pod
                        ExitFromTerminal();
                    }
                }
            }
        }
        #endregion
    }

    void FixedUpdate()
    {
        // Ray per l'interazione con gli oggetti dal centro dello schermo
        interactionRay = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward, Color.red);
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.right, Color.green);
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.up, Color.blue);

        // Viene chiamato il metodo per controllare l'interazione con gli oggetti interagibili
        CheckInteract();
    }

    void CheckInteract()
    {
        #region !isInteracting
        if (!isInteracting)
        {
            RaycastHit hit;
            if (Physics.Raycast(interactionRay, out hit, dropDistance, ~playerLayer))
            {
                #region pickable
                // SE L'OGGETTO DEL RAYCAST E' PICKUBLE
                if (hit.collider.CompareTag("Pickuble"))
                {
                    raycastTarget = RaycastTarget.pickable;
                    InputUIFeedback();
                    raycastGameobject = hit.transform.gameObject;
                    pickubleObj = raycastGameobject;

                    MeshRenderer mr = raycastGameobject.GetComponent<MeshRenderer>();
                    Material mat = mr.material;
                    mat.SetColor("_EmissionColor", new Color(LightPickable, LightPickable, LightPickable));
                }
                #endregion

                #region terminal
                // SE L'OGGETTO DEL RAYCAST E' Terminal
                if (hit.collider.CompareTag("Terminal"))
                {
                    raycastGameobject = hit.transform.gameObject;
                    raycastTarget = RaycastTarget.terminal;
                    InputUIFeedback();
                }
                #endregion

                #region actionObj
                // SE L'OGGETTO DEL RAYCAST E' ActionObj
                if (hit.collider.CompareTag("ActionObj"))
                {
                    raycastGameobject = hit.transform.gameObject;
                    ActionObj actionObj = raycastGameobject.GetComponent<ActionObj>();
                    switch (actionObj.actionType)
                    {
                        case ActionType.button:
                            {
                                raycastTarget = RaycastTarget.actionButton;
                                break;
                            }
                        case ActionType.doorButton:
                            {
                                raycastTarget = RaycastTarget.actionButton;
                                if (actionObj.GetComponent<DoorButton>())
                                {
                                    actionObj.GetComponent<DoorButton>().CheckPermission();
                                }
                                break;
                            }
                        case ActionType.lever:
                            {
                                raycastTarget = RaycastTarget.actionLever;
                                break;
                            }
                        case ActionType.openable:
                            {
                                raycastTarget = RaycastTarget.actionOpenable;
                                break;
                            }

                        case ActionType.helmet:
                            {
                                raycastTarget = RaycastTarget.actionHelmet;
                                break;
                            }

                        case ActionType.pod:
                            {
                                raycastTarget = RaycastTarget.actionPod;
                                break;
                            }
                        case ActionType.putRecharge:
                            {
                                raycastTarget = RaycastTarget.putRecharge;
                                break;
                            }
                        case ActionType.takeRecharge:
                            {
                                raycastTarget = RaycastTarget.takeRecharge;
                                break;
                            }
                        case ActionType.nothing:
                            {
                                raycastTarget = RaycastTarget.nothing;
                                break;
                            }
                    }
                    InputUIFeedback();
                }
                #endregion

                #region Absorb
                // SE L'OGGETTO DEL RAYCAST E' ActionObj
                if (hit.collider.CompareTag("Absorb"))
                {
                    InputUIFeedback();
                    raycastTarget = RaycastTarget.absorb;
                    PlayerStatus ps = GetComponent<PlayerStatus>();
                    AbsorbMaterial am = hit.collider.transform.GetComponent<AbsorbMaterial>();

                    if (am.material == AbrsorbType.nessuno && ps.storageMaterial != AbrsorbType.nessuno)
                    {
                        raycastTarget = RaycastTarget.expel;
                    }
                    else if (am.material != AbrsorbType.nessuno && ps.storageMaterial == AbrsorbType.nessuno)
                    {
                        raycastTarget = RaycastTarget.absorb;
                    }
                    else
                    {
                        raycastTarget = RaycastTarget.nothing;
                    }


                    if (Input.GetAxis("Trigger") < -0.9f)
                    {
                        GetComponent<FirstPersonController>().enabled = false;
                        if (!pauseAbsorb)
                        {
                            if (ps.storageMaterial == AbrsorbType.nessuno)
                            {
                                am = hit.collider.transform.GetComponent<AbsorbMaterial>();
                                if (am.material != AbrsorbType.nessuno)
                                {
                                    scanPerc.SetActive(true);
                                    scanTime += 0.5f * Time.deltaTime;
                                    scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                                    hit.collider.transform.Find("cnvData/pnlData/pnlQuantity/imgQuantityTotal/imgQuantityFill").GetComponent<Image>().fillAmount = scanTime;
                                    if (scanTime > 1)
                                    {
                                        pauseAbsorb = true;
                                        ps.storageMaterial = am.material;
                                        FindObjectOfType<MenuControl>().CheckMaterial(am.material);
                                        am.CheckMaterial(AbrsorbType.nessuno);
                                        am.material = 0;
                                        scanTime = 0.0f;
                                        scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                                        hit.collider.transform.Find("cnvData/pnlData/pnlQuantity/imgQuantityTotal/imgQuantityFill").GetComponent<Image>().fillAmount = scanTime;
                                        scanPerc.SetActive(false);
                                        am.absorbEvent.Invoke();
                                        if (am.oneTimeAbsorb)
                                        {
                                            am.oneTimeAbsorbEvent.Invoke();
                                            am.oneTimeAbsorb = false;
                                        }

                                        //Mettere suono assorbimento
                                    }
                                }
                            }
                            else
                            {
                                am = hit.collider.transform.GetComponent<AbsorbMaterial>();
                                if (am.material == AbrsorbType.nessuno && !pauseAbsorb)
                                {
                                    scanPerc.SetActive(true);
                                    scanTime += 0.5f * Time.deltaTime;
                                    scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                                    hit.collider.transform.Find("cnvData/pnlData/pnlQuantity/imgQuantityTotal/imgQuantityFill").GetComponent<Image>().fillAmount = scanTime;
                                    if (scanTime > 1)
                                    {
                                        pauseAbsorb = true;
                                        am.CheckMaterial(ps.storageMaterial);
                                        am.material = ps.storageMaterial;
                                        ps.storageMaterial = 0;
                                        scanTime = 0.0f;
                                        FindObjectOfType<MenuControl>().CheckMaterial(AbrsorbType.nessuno);
                                        scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                                        hit.collider.transform.Find("cnvData/pnlData/pnlQuantity/imgQuantityTotal/imgQuantityFill").GetComponent<Image>().fillAmount = scanTime;
                                        scanPerc.SetActive(false);
                                        am.expelEvent.Invoke();
                                        if (am.oneTimeExpel)
                                        {
                                            am.oneTimeExpelEvent.Invoke();
                                            am.oneTimeExpel = false;
                                            //Mettere suono expel
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        pauseAbsorb = false;
                        GetComponent<FirstPersonController>().enabled = true;
                        scanTime = 0.0f;
                        scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                        hit.collider.transform.Find("cnvData/pnlData/pnlQuantity/imgQuantityTotal/imgQuantityFill").GetComponent<Image>().fillAmount = scanTime;
                        scanPerc.SetActive(false);
                    }
                }
                #endregion

                #region Repair

                if (hit.collider.CompareTag("Repair"))
                {
                    InputUIFeedback();
                    raycastTarget = RaycastTarget.repair;
                    raycastGameobject = hit.transform.gameObject;

                    PlayerStatus ps = GetComponent<PlayerStatus>();
                    RepairController rc = hit.collider.GetComponent<RepairController>();
                    rc.ShowRepairUI();
                    if (rc.repairMaterial == ps.storageMaterial)
                    {
                        raycastTarget = RaycastTarget.expel;
                        if (Input.GetAxis("Trigger") < -0.9f)
                        {
                            GetComponent<FirstPersonController>().enabled = false;
                            scanPerc.SetActive(true);
                            scanTime += 0.5f * Time.deltaTime;
                            scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                            if (scanTime > 1)
                            {
                                pauseAbsorb = true;
                                ps.storageMaterial = AbrsorbType.nessuno;
                                FindObjectOfType<MenuControl>().CheckMaterial(AbrsorbType.nessuno);
                                scanTime = 0.0f;
                                scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                                scanPerc.SetActive(false);
                                rc.repairEvent.Invoke();
                                //Mettere suono repair
                            }
                        }
                    }
                    else
                    {
                        pauseAbsorb = false;
                        GetComponent<FirstPersonController>().enabled = true;
                        scanTime = 0.0f;
                        scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                        scanPerc.SetActive(false);
                    }
                }

                if (hit.collider.CompareTag("Untagged"))
                {
                    raycastTarget = RaycastTarget.nothing;
                    InputUIFeedback();
                }
                #endregion
            }
            else
            {
                raycastTarget = RaycastTarget.nothing;
                InputUIFeedback();
            }
        }
        #endregion
    }


    public void ViewObjectMenu(GameObject obj)
    {
        pickubleObj = Instantiate(obj);

        isInteracting = true;
        isInspecting = true;
        menu.objActive = pickubleObj;
        inspect.transform.localPosition = new Vector3(0, 0, 0.2f) + new Vector3(0, 0, (1 * pickubleObj.GetComponent<ObjInformation>().near));
        pickubleObj.transform.position = inspect.transform.position;
        pickubleObj.transform.SetParent(inspect.transform);
        pickubleObj.transform.localEulerAngles = new Vector3(-90, 0, 0);
    }

    IEnumerator LerpCameraMovement(Vector3 pos, Vector3 lookAt)
    {
        isCoroutineWork = true;

        float elapsedTime = 0.0f;
        Vector3 startPos = Camera.main.transform.position;
        Vector3 startLook = Camera.main.transform.forward + Camera.main.transform.position;

        while (elapsedTime < 0.5f)
        {
            Camera.main.transform.position = Vector3.Lerp(startPos, pos, (elapsedTime / 0.4f));
            Camera.main.transform.LookAt(Vector3.Lerp(startLook, lookAt, (elapsedTime / 0.4f)));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        isCoroutineWork = false;
    }

    IEnumerator LerpCameraToMovement(Vector3 pos, Vector3 lookAt)
    {
        isCoroutineWork = true;

        float elapsedTime = 0.0f;
        Vector3 startPos = Camera.main.transform.position;
        Vector3 startLook = Camera.main.transform.forward + Camera.main.transform.position;

        while (elapsedTime < 0.5f)
        {
            Camera.main.transform.position = Vector3.Lerp(startPos, pos, (elapsedTime / 0.4f));
            Camera.main.transform.LookAt(Vector3.Lerp(startLook, lookAt, (elapsedTime / 0.4f)));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        isCoroutineWork = false;
        GetComponent<FirstPersonController>().enabled = true;
    }

    public void ExitFromTerminal()
    {
        StartCoroutine(LerpCameraToMovement(cameraPos, cameraLookPos));
        activeCanvas.isUsed = false;
        activeCanvas = null;
        dummyButton.Select();
        body.SetActive(true);
        torcia.SetActive(true);
        isTerminal = false;
        isInteracting = false;
        actionImage.gameObject.SetActive(true);
        viewFinder.gameObject.SetActive(true);
        feedbackGameobject.SetActive(true);
    }

    IEnumerator LerpPickuble(GameObject inspectObj)
    {
        isLerpPickable = true;

        float elapsedTime = 0.0f;
        Vector3 startPos = inspectObj.transform.localPosition;
        Vector3 startRot = inspectObj.transform.localEulerAngles;
        ObjInformation objInfo = raycastGameobject.transform.gameObject.GetComponent<ObjInformation>();

        while (elapsedTime < 0.5f)
        {
            inspectObj.transform.localPosition = Vector3.Lerp(startPos, Vector3.zero, (elapsedTime / 0.5f));
            inspectObj.transform.localEulerAngles = Vector3.Lerp(startRot, objInfo.rotInInspect, (elapsedTime / 0.5f));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        inspectObj.transform.localPosition = Vector3.zero;
        inspectObj.transform.localEulerAngles = objInfo.rotInInspect;
        isLerpPickable = false;
    }

    IEnumerator LerpBackPickuble(GameObject inspectObj)
    {
        isLerpPickable = true;

        float elapsedTime = 0.0f;
        Vector3 startPos = inspectObj.transform.position;
        Quaternion startRot = inspectObj.transform.rotation;
        ObjInformation objInfo = raycastGameobject.transform.gameObject.GetComponent<ObjInformation>();

        while (elapsedTime < 0.5f)
        {
            inspectObj.transform.position = Vector3.Lerp(startPos, lastObjPos, (elapsedTime / 0.5f));
            inspectObj.transform.rotation = Quaternion.Lerp(startRot, lastObjRot, (elapsedTime / 0.5f));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        inspectObj.transform.position = lastObjPos;
        inspectObj.transform.rotation = lastObjRot;
        isLerpPickable = false;
        GetComponent<FirstPersonController>().enabled = true;

    }

    IEnumerator ScanObj()
    {
        ObjInformation objInfo = pickubleObj.GetComponent<ObjInformation>();
        isScanning = true;
        // mettere suono della scannerizzazione
        while (isScanning)
        {
            scanPerc.SetActive(true);
            bs.batteryEnergyAmount -= (float)((float)scannerBatteryCost / 200) * Time.deltaTime;

            scanTime += 0.5f * Time.deltaTime;
            scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
            if (scanTime > 1.5f)
            {
                isScanning = false;
                scanTime = 0.0f;
                scanPerc.transform.GetChild(0).gameObject.GetComponent<Image>().fillAmount = scanTime;
                scanPerc.SetActive(false);
                objInfo.isScanning = true;
                scan.SetNewButton(objInfo.objToView, objInfo.dati, objInfo.objPreview);
            }
            //Mettere suono scansione oggetto
            yield return null;
        }
    }


    void InputUIFeedback()
    {
        switch (raycastTarget)
        {
            case RaycastTarget.nothing:
                {
                    if (pickubleObj != null && !isInspecting)
                    {
                        MeshRenderer mr = pickubleObj.GetComponent<MeshRenderer>();
                        Material mat = mr.material;
                        mat.SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f));
                        pickubleObj = null;
                    }
                    if (raycastGameobject != null)
                    {
                        if (raycastGameobject.CompareTag("ActionObj"))
                        {
                            ActionObj actionObj = raycastGameobject.GetComponent<ActionObj>();
                            if (actionObj.actionType == ActionType.doorButton)
                            {
                                if (actionObj.GetComponent<DoorButton>())
                                {
                                    actionObj.GetComponent<DoorButton>().ClosePanel();
                                }
                                raycastGameobject = null;
                            }
                        }
                        else if (raycastGameobject.CompareTag("Repair"))
                        {
                            RepairController rc = raycastGameobject.GetComponent<RepairController>();
                            rc.CloseRepairUI();
                            raycastGameobject = null;
                        }
                    }
                    actionImage.gameObject.SetActive(false);
                    buttonFeedbackImage.gameObject.SetActive(false);
                    feedbacktext.gameObject.SetActive(false);
                    break;
                }
            case RaycastTarget.pickable:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = objectInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = objectInputFeedback.button;
                    feedbacktext.text = objectInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.terminal:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = terminalInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = terminalInputFeedback.button;
                    feedbacktext.text = terminalInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.actionButton:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = actionButtonInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = actionButtonInputFeedback.button;
                    feedbacktext.text = actionButtonInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.actionLever:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = actionLeverInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = actionLeverInputFeedback.button;
                    feedbacktext.text = actionLeverInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.actionOpenable:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = actionOpenableInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = actionOpenableInputFeedback.button;
                    feedbacktext.text = actionOpenableInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.actionHelmet:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = helmetInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = helmetInputFeedback.button;
                    feedbacktext.text = helmetInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.actionPod:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = podInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = podInputFeedback.button;
                    feedbacktext.text = podInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.absorb:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = absorbInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = absorbInputFeedback.button;
                    feedbacktext.text = absorbInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.expel:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = expelbInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = expelbInputFeedback.button;
                    feedbacktext.text = expelbInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.putRecharge:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = putRechargeInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = putRechargeInputFeedback.button;
                    feedbacktext.text = putRechargeInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.takeRecharge:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = takeRechargeInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = takeRechargeInputFeedback.button;
                    feedbacktext.text = takeRechargeInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
            case RaycastTarget.repair:
                {
                    buttonFeedbackImage.gameObject.SetActive(true);
                    feedbacktext.gameObject.SetActive(true);
                    actionImage.sprite = repairInputFeedback.actionSprite;
                    buttonFeedbackImage.sprite = repairInputFeedback.button;
                    feedbacktext.text = repairInputFeedback.textToView;
                    actionImage.gameObject.SetActive(true);
                    break;
                }
        }
    }

}



