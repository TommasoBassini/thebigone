﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class DoorButton : ActionObj
{
    public DoorBaseScript door;
    private bool isInteractable = true;
    public UnityEvent generalFeedbackEvent;

    private bool isShow;
    private bool havePermission = true;

    public override void DoStuff()
    {
        PlayerStatus ps = FindObjectOfType<PlayerStatus>();

        if (door.isUnlocked && isInteractable && door.isPowered && !door.isBroken && havePermission)
        {
            isInteractable = false;
            Invoke("EnableInteraction", 1);
            generalFeedbackEvent.Invoke();
            door.OpenClosedDoor();
        }
    }

    void EnableInteraction()
    {
        isInteractable = true;
    }


    public void CheckPermission()
    {
        if (door.lvlPermession > 0 && !isShow)
        {
            StopAllCoroutines();
            isShow = true;
            StartCoroutine("ShowPanelCO");
        }
    }

    public void ClosePanel()
    {
        if (door.lvlPermession > 0 && isShow)
        {
            isShow = false;
            StopAllCoroutines();
            StartCoroutine("ClosePanelCO");
        }
    }

    public IEnumerator ShowPanelCO()
    {
        GameObject panel;
        if (transform.childCount != 0)
        {
            panel = transform.Find("CanvasPermessi/Panel").gameObject;
        }
        else
        {
            GameObject canvas = Instantiate(Resources.Load<GameObject>("CanvasPermessi"));
            canvas.name = "CanvasPermessi";
            canvas.transform.SetParent(this.transform);
            Canvas panelRect1 = canvas.GetComponent<Canvas>();
            panelRect1.transform.localEulerAngles = Vector3.zero;
            panelRect1.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
            panelRect1.transform.localPosition = new Vector3(-0.758f, -0.016f, -5.1f);
            panel = transform.Find("CanvasPermessi/Panel").gameObject;
        }
        RectTransform panelRect = panel.GetComponent<RectTransform>();

        Text permessoText = transform.Find("CanvasPermessi/Panel/PermessoRichiestoTxt").GetComponent<Text>();
        Text statoText = transform.Find("CanvasPermessi/Panel/StatoPermesso").GetComponent<Text>();
        PlayerStatus ps = FindObjectOfType<PlayerStatus>();
        switch (door.permessi)
        {
            case SecurityType.medic:
                {
                    permessoText.text = "Richiesto permesso medico " + door.lvlPermession.ToString();
                    if (door.lvlPermession <= ps.medicLvl)
                    {
                        havePermission = true;
                        statoText.text = "Sbloccato";
                        statoText.color = Color.green;
                    }
                    else
                    {
                        havePermission = false;
                        statoText.text = "Accesso Negato";
                        statoText.color = Color.red;
                    }
                    break;
                }
            case SecurityType.engineer:
                {
                    permessoText.text = "Richiesto permesso ingegnere " + door.lvlPermession.ToString();
                    if (door.lvlPermession <= ps.engineerLvl)
                    {
                        havePermission = true;
                        statoText.text = "Sbloccato";
                        statoText.color = Color.green;
                    }
                    else
                    {
                        havePermission = false;
                        statoText.text = "Accesso Negato";
                        statoText.color = Color.red;
                    }
                    break;
                }
            case SecurityType.guard:
                {
                    permessoText.text = "Richiesto permesso guadia " + door.lvlPermession.ToString();
                    if (door.lvlPermession <= ps.guardLvl)
                    {
                        havePermission = true;
                        statoText.text = "Sbloccato";
                        statoText.color = Color.green;
                    }
                    else
                    {
                        havePermission = false;
                        statoText.text = "Accesso Negato";
                        statoText.color = Color.red;
                    }
                    break;
                }
        }

        if (this.gameObject.name == "ButtonPorta (2)")
        {
            panelRect.localScale = new Vector3(-1, 0, 1);
        }

        float elapsedTime = 0.0f;
        float startSize = 0;

        while (elapsedTime < 0.3f)
        {
            float prova = Mathf.Lerp(startSize, 1.0f, (elapsedTime / 0.3f));
            panelRect.localScale = new Vector3(panelRect.localScale.x, prova, 1);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        foreach (Transform item in panel.transform)
        {
            item.gameObject.SetActive(true);
        }


    }

    public IEnumerator ClosePanelCO()
    {
        GameObject panel = transform.Find("CanvasPermessi/Panel").gameObject;
        RectTransform panelRect = panel.GetComponent<RectTransform>();
        float elapsedTime = 0.0f;
        float startSize = panelRect.localScale.y;

        foreach (Transform item in panel.transform)
        {
            item.gameObject.SetActive(false);
        }

        while (elapsedTime < 0.3f)
        {
            float prova = Mathf.Lerp(startSize, 0.0f, (elapsedTime / 0.3f));
            panelRect.localScale = new Vector3(panelRect.localScale.x, prova, 1);

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        panelRect.localScale = new Vector3(1, 0, 1);
    }
}
