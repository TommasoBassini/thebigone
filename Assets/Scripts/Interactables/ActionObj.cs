﻿using UnityEngine;
using System.Collections;

public enum ActionType
{
    nothing,
    doorButton,
    button,
    lever,
    openable,
    helmet,
    pod,
    putRecharge,
    takeRecharge
}

// Classe astratta per gli oggetti interagibili
public abstract class ActionObj : MonoBehaviour
{
    public ActionType actionType;
    // Metodo astratto per gli oggetti interagibili
    public abstract void DoStuff();

}
