﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class GeneralActionObj : ActionObj
{
    public UnityEvent generalFeedbackEvent;

    public override void DoStuff()
    {
        generalFeedbackEvent.Invoke();
    }
}

