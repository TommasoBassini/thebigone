﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DoorBaseScript : MonoBehaviour
{
    public bool isOpen;
    public bool isUnlocked;
    public bool isPowered;

    public bool isBroken;

    public GameObject[] buttons;

    public SecurityType permessi;
    public int lvlPermession;

    void Start()
    {
        UpdateButtons();
        UpdateLight();
    }

    public void UpdateButtons()
    {
        foreach (var item in buttons)
        {
            MeshRenderer mr = item.GetComponent<MeshRenderer>();
            Material mat = mr.material;

            if (isPowered && !isBroken)
            {
                if (isUnlocked)
                {
                    item.GetComponent<DoorButton>().actionType = ActionType.doorButton;
                    mat.SetColor("_Color", Color.white);
                    mat.SetColor("_EmissionColor", new Color(0.6f, 0.6f, 0.6f));
                }
                else
                {
                    item.GetComponent<DoorButton>().actionType = ActionType.doorButton;
                    mat.SetColor("_Color", Color.red);
                    mat.SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f));
                }
            }
            else
            {
                item.GetComponent<DoorButton>().actionType = ActionType.nothing;
                mat.SetColor("_Color", Color.white);
                mat.SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f));
            }

        }
    }

    public void UpdateLight()
    {
        foreach (Transform item in this.transform)
        {
            if (item.gameObject.GetComponentInChildren<Light>() && !item.gameObject.GetComponentInChildren<Light>().gameObject.isStatic)
            {
                Light light = item.gameObject.GetComponentInChildren<Light>();
                Image panel = item.gameObject.GetComponentInChildren<Image>();
                
                if (isUnlocked && isPowered && !isBroken)
                {
                    light.color = Color.green;
                    panel.color = Color.green;
                }
                else
                {
                    light.color = Color.red;
                    panel.color = Color.red;
                }
            }
        }
    }

    public void InvertLockStatus()
    {
        if (isUnlocked && isOpen)
        {
            OpenClosedDoor();
        }
        isUnlocked = !isUnlocked;
        UpdateButtons();
        UpdateLight();
    }

    public void InvertPowerStatus()
    {
        if (isPowered && isOpen)
        {
            OpenClosedDoor();
        }

        isPowered = !isPowered;
        UpdateButtons();
        UpdateLight();
    }

    public void ForceLockStatus(bool status)
    {
        if (!status && isOpen)
        {
            OpenClosedDoor();
        }
        isUnlocked = status;
        UpdateButtons();
        UpdateLight();
    }

    public void ForcePowerStatus(bool status)
    {
        if (!status && isOpen)
        {
            OpenClosedDoor();
        }
        isPowered = status;
        UpdateButtons();
        UpdateLight();
    }

    public void OpenClosedDoor()
    {
        if (isOpen)
        {
            isOpen = false;
            Animator anim = gameObject.GetComponent<Animator>();
            anim.SetTrigger("Chiudi");
            UpdateButtons();
            //mettere qui suono chiusura porta
            return;
        }
        else
        {
            isOpen = true;
            Animator anim = gameObject.GetComponent<Animator>();
            anim.SetTrigger("Chiudi");
            UpdateButtons();
            //mettere qui suono apertura porta
            return;
        }
    }

    public void ForceOpenCloseDoor(bool status)
    {
        if (status && !isOpen)
        {
            isOpen = true;
            Animator anim = gameObject.GetComponent<Animator>();
            anim.SetTrigger("Chiudi");
            UpdateButtons();
            return;
        }
        else if (!status && isOpen)
        {
            isOpen = false;
            Animator anim = gameObject.GetComponent<Animator>();
            anim.SetTrigger("Chiudi");
            UpdateButtons();
            return;
        }
    }
}