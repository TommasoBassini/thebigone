﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class HelmetController : ActionObj
{
    public UnityEvent generalFeedbackEvent;

    public override void DoStuff()
    {
        generalFeedbackEvent.Invoke();
        Destroy(this.gameObject);
    }
}
