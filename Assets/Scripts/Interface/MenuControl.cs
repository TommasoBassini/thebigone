﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class MenuControl : MonoBehaviour
{
    public bool isMenu = false;
    public GameObject pnlMenu;
    public GameObject[] panels;
    public GameObject panelShowScan;
    public GameObject panelShowReport;
    public Button[] buttons;
    public int nMenu = 0;
    public int nButtonReport = 0;
    public bool isSubMenu = false;
    public bool isShowReport = false;
    public bool isShowScan = false;
    public GameObject reportsPanel;
    private bool tutorialMenu = false;
    private List<GameObject> reportButtons = new List<GameObject>();

    private ObjectInteract player;
    private PlayerStatus playerStatus;

    public GameObject mirino;
    public GameObject scansPanel;
    public GameObject objActive;
    public Button scanButton;
    public BlurOptimized blur;

    public GameObject reminderPanel;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        foreach (Transform item in reportsPanel.transform)
        {
            reportButtons.Add(item.gameObject);
        }
        player = FindObjectOfType<ObjectInteract>();
        playerStatus = FindObjectOfType<PlayerStatus>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button3) || Input.GetKeyDown(KeyCode.P))
        {
            if (!player.isInteracting)
            {
                if (!isMenu)
                {
                    if (tutorialMenu)
                    {
                        isMenu = true;
                        FindObjectOfType<FirstPersonController>().enabled = false;
                        mirino.SetActive(false);
                        pnlMenu.SetActive(true);
                        panels[0].SetActive(true);
                        buttons[1].Select();
                        buttons[0].Select();
                        blur.blurSize = 5;
                        blur.blurIterations = 3;
                        blur.enabled = true;
                    }
                    else
                    {
                        GameObject pnlMenuTutorial = FindObjectOfType<MenuControl>().transform.Find("pnlMainArea/pnlOtherInputs/pnlMenu").gameObject;
                        if (pnlMenuTutorial.activeInHierarchy)
                        {
                            isMenu = true;
                            FindObjectOfType<FirstPersonController>().enabled = false;
                            mirino.SetActive(false);
                            pnlMenu.SetActive(true);
                            panels[0].SetActive(true);
                            buttons[1].Select();
                            buttons[0].Select();
                            blur.blurSize = 5;
                            blur.blurIterations = 3;
                            blur.enabled = true;
                            pnlMenuTutorial.SetActive(false);
                            tutorialMenu = true;
                        }
                    }
                }
                else
                {
                    isMenu = false;
                    FindObjectOfType<FirstPersonController>().enabled = true;
                    mirino.SetActive(true);
                    pnlMenu.SetActive(false);
                    panels[nMenu].SetActive(false);
                    nMenu = 0;
                    blur.enabled = false;
                    blur.blurIterations = 1;
                    blur.blurSize = 0;
                    return;
                }
            }
        }

        if ((Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Escape)) && isMenu)
        {
            if (isSubMenu)
            {
                if (isShowReport)
                {
                    isShowReport = false;
                    panelShowReport.SetActive(false);
                    panels[nMenu].SetActive(true);
                    reportButtons[nButtonReport].GetComponent<Button>().Select();
                    return;
                }

                if (isShowScan)
                {
                    ObjectInteract player = FindObjectOfType<ObjectInteract>();
                    player.isInspecting = false;
                    player.isInteracting = false;
                    isShowScan = false;
                    panelShowScan.SetActive(false);
                    panels[nMenu].SetActive(true);
                    scanButton.Select();
                    Destroy(objActive);
                    return;
                }

                if (!isShowReport && !isShowScan)
                {
                    buttons[nMenu].Select();
                    isSubMenu = false;
                    return;
                }
                return;
            }
            else
            {
                isMenu = false;
                FindObjectOfType<FirstPersonController>().enabled = true;
                mirino.SetActive(true);
                pnlMenu.SetActive(false);
                panels[nMenu].SetActive(false);
                nMenu = 0;
                blur.enabled = false;
                return;
            }
        }
    }

    public void ChangePanel(int n)
    {
        panels[nMenu].SetActive(false);
        nMenu = n;
        panels[nMenu].SetActive(true);
    }

    public void SelectPanel(Button buttonToSelect)
    {
        isSubMenu = true;
        buttonToSelect.Select();
    }

    public void SelectScrollBar(Scrollbar scroll)
    {
        isSubMenu = true;
        scroll.Select();
    }

    public void RefreshReport()
    {
        for (int i = 0; i < ReportManager.reportState.Count; i++)
        {
            if (ReportManager.reportState[i].discovered)
            {
                reportButtons[ReportManager.reportState[i].indexInList].GetComponentInChildren<Text>().text = ReportManager.reportState[i].titolo;
                PODReportButton podReportButton = reportButtons[ReportManager.reportState[i].indexInList].GetComponent<PODReportButton>();
                podReportButton.titolo = ReportManager.reportState[i].titolo;
                podReportButton.autore = ReportManager.reportState[i].autore;
                podReportButton.luogo = ReportManager.reportState[i].luogo;
                podReportButton.text = ReportManager.reportState[i].text;
                podReportButton.isUnlocked = true;
            }
        }
    }

    public void SelectScanButton()
    {
        if (scansPanel.transform.GetChild(0) != null)
        {
            isSubMenu = true;
            Button buttonToSelect = scansPanel.transform.GetChild(0).GetComponent<Button>();
            buttonToSelect.Select();
        }
    }

    public void CheckPodStatus()
    {

        PlayerStatus playerStatus = FindObjectOfType<PlayerStatus>();
        //Controllo livello medico
        switch (playerStatus.medicLvl)
        {
            case 1:
                {
                    this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlMedicPermissions/txtPermission1").GetComponent<Text>().color = Color.green;
                    break;
                }
            case 2:
                {
                    this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlMedicPermissions/txtPermission2").GetComponent<Text>().color = Color.green;
                    break;
                }
        }

        switch (playerStatus.engineerLvl)
        {
            case 1:
                {
                    this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlEngineerPermissions/txtPermission1").GetComponent<Text>().color = Color.green;
                    break;
                }
            case 2:
                {
                    this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlEngineerPermissions/txtPermission2").GetComponent<Text>().color = Color.green;
                    break;
                }
        }

        switch (playerStatus.guardLvl)
        {
            case 1:
                {
                    this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlSecurityPermissions/txtPermission1").GetComponent<Text>().color = Color.green;
                    break;
                }
            case 2:
                {
                    this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlSecurityPermissions/txtPermission2").GetComponent<Text>().color = Color.green;
                    break;
                }
        }
        this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlMedicPermissions/txtMedic").GetComponent<Text>().text = "Medico: " + playerStatus.medicLvl.ToString();
        this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlEngineerPermissions/txtEngineer").GetComponent<Text>().text = "Ingegnere: " + playerStatus.medicLvl.ToString();
        this.transform.Find("pnlLensMenu/pnlStatus/pnlPermissions/pnlSecurityPermissions/txtSecurity").GetComponent<Text>().text = "Guardia: " + playerStatus.medicLvl.ToString();
        //this.transform.Find("pnlLensMenu/pnlStatus/pnlValues/pnlMaterial/txtValue").GetComponent<Text>().text = " " + playerStatus.storageMaterial.ToString();
    }

    public void CheckMaterial(AbrsorbType material)
    {
        GameObject panelMat = this.transform.Find("pnlPODArea/pnlMaterial").gameObject;
        Text textMaterial = panelMat.transform.Find("txtMaterial").GetComponent<Text>();

        switch (material)
        {
            case AbrsorbType.nessuno:
                {
                    panelMat.SetActive(false);
                    break;
                }
            case AbrsorbType.silice:
                {
                    panelMat.SetActive(true);
                    textMaterial.text = "Silice";
                    break;
                }
            case AbrsorbType.titanio:
                {
                    panelMat.SetActive(true);
                    textMaterial.text = "Titanio";
                    break;
                }
            case AbrsorbType.fluidoCriogenico:
                {
                    panelMat.SetActive(true);
                    textMaterial.text = "Fluido Criogenico";
                    break;
                }
            case AbrsorbType.fluidoSolvente:
                {
                    panelMat.SetActive(true);
                    textMaterial.text = "Fluido solvente";
                    break;
                }
            case AbrsorbType.cristalliAzoto:
                {
                    panelMat.SetActive(true);
                    textMaterial.text = "Cristalli di azoto";
                    break;
                }
        }
    }
}
