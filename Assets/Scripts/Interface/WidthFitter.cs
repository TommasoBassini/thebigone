﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WidthFitter : MonoBehaviour
{
    private RectTransform rect;
    private float currentHeight = 670f;
    public float panelHeight;
	
    void OnEnable()
    {

    }

    public void InsertPanel(float _panelHeight)
    {
        panelHeight += (_panelHeight + 15);
        rect = GetComponent<RectTransform>();

        if (panelHeight > currentHeight)
        {
            rect.sizeDelta = new Vector3(rect.sizeDelta.x, panelHeight, 0);
        }
    }
}
