﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LerpScroll : MonoBehaviour
{
    private Scrollbar scroll;
    public RectTransform pnlReminder;

    void Start()
    {
        scroll = GetComponent<Scrollbar>();
    }

    public void ScrollLerp()
    {
        float angV = Input.GetAxis("RightV");
        scroll.value -= angV * Time.deltaTime;
    }
}
