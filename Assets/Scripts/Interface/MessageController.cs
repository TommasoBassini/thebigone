﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MessageController : MonoBehaviour
{
    public Queue<GameObject> leftMessageQueue = new Queue<GameObject>();
    public Queue<GameObject> rightMessageQueue = new Queue<GameObject>();

    private bool isLeftOccupied = false;
    private bool isRightOccupied = false;

    public GameObject messagePrefab;
    public Vector3 podMessagePosition;
    public Vector3 suitMessagePosition;

    public void QueueLeftMessage(GameObject _message)
    {
        leftMessageQueue.Enqueue(_message);
        if (!isLeftOccupied)
        {
            isLeftOccupied = true;
            StartCoroutine(_message.GetComponent<UIMessageMovement>().MessageMovement());
        }
    }

    public void CheckLeftQueue()
    {
        leftMessageQueue.Dequeue();
        if (leftMessageQueue.Count > 0)
        {
            StartCoroutine(leftMessageQueue.Peek().GetComponent<UIMessageMovement>().MessageMovement());
        }
        else
        {
            isLeftOccupied = false;
        }
    }

    public void QueueRightMessage(GameObject _message)
    {
        rightMessageQueue.Enqueue(_message);
        if (!isRightOccupied)
        {
            isRightOccupied = true;
            StartCoroutine(_message.GetComponent<UIMessageMovement>().MessageMovement());
        }
    }

    public void CheckRightQueue()
    {
        rightMessageQueue.Dequeue();
        if (rightMessageQueue.Count > 0)
        {
            StartCoroutine(rightMessageQueue.Peek().GetComponent<UIMessageMovement>().MessageMovement());
        }
        else
        {
            isRightOccupied = false;
        }
    }
}
