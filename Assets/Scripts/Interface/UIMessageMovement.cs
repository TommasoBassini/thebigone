﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIMessageMovement : MonoBehaviour
{
    public float secVisible;
    public float secDelay;

    public IEnumerator MessageMovement()
    {
        RectTransform rt = GetComponent<RectTransform>();
        Vector3 startPos = rt.anchoredPosition;
        Vector3 endPos = new Vector3(startPos.x * -1, startPos.y, startPos.z);
        float elapsedTime = 0.0f;

        while (elapsedTime < 0.5f)
        {
            rt.anchoredPosition = Vector3.Lerp(startPos, endPos, (elapsedTime / 0.5f));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(secVisible);

        elapsedTime = 0.0f;

        while (elapsedTime < 0.5f)
        {
            rt.anchoredPosition = Vector3.Lerp(endPos, (startPos * 1.2f), (elapsedTime / 0.5f));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(secDelay);

        MessageController mc = FindObjectOfType<MessageController>();
        if (rt.anchoredPosition.x < 0)
            mc.CheckLeftQueue();
        else
            mc.CheckRightQueue();

        Destroy(this.gameObject);
    }

}
