﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReminderPanelFitter : MonoBehaviour
{
    public string text;

    public void InitializeReminderText(string text)
    {
        Text textReminder = this.transform.GetChild(0).gameObject.GetComponent<Text>();
        textReminder.text = text;
        RectTransform textRc = textReminder.gameObject.GetComponent<RectTransform>();
        textRc.sizeDelta = new Vector3(textRc.sizeDelta.x, textReminder.preferredHeight, 0);
        LayoutElement le = this.GetComponent<LayoutElement>();
        le.preferredHeight = textReminder.preferredHeight + 20;
        RectTransform rc = this.GetComponent<RectTransform>();
        rc.localPosition = new Vector3 (rc.localPosition.x, rc.localPosition.y, 0);
        rc.localScale = new Vector3(1, 1, 1);
        rc.localEulerAngles = new Vector3(0, 0, 0);
        WidthFitter[] parent = GetComponentsInParent<WidthFitter> (true);
        parent[0].InsertPanel(le.preferredHeight);
    }
}
