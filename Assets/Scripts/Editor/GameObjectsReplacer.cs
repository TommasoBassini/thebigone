﻿using UnityEngine;
using UnityEditor;

public class GameObjectsReplacer : ScriptableWizard
{
    public bool copyValues = true;
    public GameObject newType;
    public GameObject[] oldObjects;

    [MenuItem("Tools/Replace GameObjects")]

    static void CreateWizard()
    {
        DisplayWizard("Replace GameObjects", typeof(GameObjectsReplacer), "Replace");
    }

    void OnWizardCreate()
    {
        foreach (GameObject go in oldObjects)
        {
            GameObject newObject;
            newObject = (GameObject)PrefabUtility.InstantiatePrefab(newType);
            newObject.transform.position = go.transform.position;
            newObject.transform.rotation = go.transform.rotation;
            newObject.transform.parent = go.transform.parent;

            DestroyImmediate(go);
        }
    }
}