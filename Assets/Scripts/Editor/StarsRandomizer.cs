﻿using UnityEngine;
using UnityEditor;

public class StarsRandomizer : ScriptableWizard
{
    [Header("Minimum and maximum light range of stars")]
    public float minLightRange = 0.1f;
    public float maxLightRange = 0.5f;

    [Header("Multiplicative factor light range and star scale")]
    public float factor = 1f;

    [MenuItem("Tools/Stars Randomizer")]

    static void CreateWizard()
    {
        DisplayWizard("Stars Randomizer", typeof(StarsRandomizer), "Randomize");
    }

    void OnWizardCreate()
    {
        GameObject[] stars = GameObject.FindGameObjectsWithTag("star");

        foreach (GameObject star in stars)
        {
            float range = Random.Range(minLightRange, maxLightRange);
            star.GetComponentInChildren<Light>().range = range;
            star.transform.localScale = new Vector3(factor * range, factor * range, factor * range);
        }
    }
}