﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using UnityEditor.Events;

public class StoryCreator : ScriptableWizard
{
    private StoryManager storyManager;
    private StoryItem storyItem = new StoryItem();
    private TerminalStatus terminalStatus;
    private TriggerEvents triggerEvents;
    private RepairController repairController;
    private ObjInformation objInformation;
    private OpenableControl openableControl;
    private AbsorbMaterial absorbMaterial;
    private RechargeActionObj rechargeActionObj;

    ////// START //////

    [Header("Numero di storie che sono già state scritte")]

    [ShowOnly]
    public int numberOfStories;

    //////

    [Header("La storia fa parte del percorso principale?")]

    [ShowOnly]
    public bool isMainStory = true;

    //////

    [Header("Oggetto che fa partire la storia")]

    public GameObject storyObject;

    [ShowIf("storyObjectType")]
    public StoryObjectType storyObjectType;

    //////

    [Header("Azione che fa partire la storia")]

    [ShowIf("storyObjectType", StoryObjectType.Terminale)]
    public TerminalAction terminalAction;

    [ShowIf("storyObjectType", StoryObjectType.Trigger)]
    public TriggerAction triggerAction;

    [ShowIf("storyObjectType", StoryObjectType.Riparabile)]
    public RepairAction repairAction;

    [ShowIf("storyObjectType", StoryObjectType.Esaminabile)]
    public InspectAction inspectAction;

    [ShowIf("storyObjectType", StoryObjectType.Apribile)]
    public OpenAction openAction;

    [ShowIf("storyObjectType", StoryObjectType.Bombola)]
    public TankAction tankAction;

    [ShowIf("storyObjectType", StoryObjectType.StazioneDiRicarica)]
    public RechargeAction rechargeAction;

    //////

    [Header("Cosa succede nella storia?")]

    public UnityEvent storyEventStep;

    ////// END //////

    static void CreateWizard()
    {
    }

    void OnWizardUpdate()
    {
        storyManager = FindObjectOfType<StoryManager>();
        numberOfStories = storyManager.storyEvents.Count;
        if (storyObject)
        {
            storyItem.item = storyObject;
            // Sei tu un terminale?
            if (storyObject.GetComponentInChildren<TerminalStatus>())
            {
                terminalStatus = storyObject.GetComponentInChildren<TerminalStatus>();
                storyObjectType = StoryObjectType.Terminale;
            }
            // Sei tu un trigger?
            else if (storyObject.GetComponent<TriggerEvents>())
            {
                triggerEvents = storyObject.GetComponent<TriggerEvents>();
                storyObjectType = StoryObjectType.Trigger;
            }
            // Sei tu un riparabile?
            else if (storyObject.GetComponent<RepairController>())
            {
                repairController = storyObject.GetComponent<RepairController>();
                storyObjectType = StoryObjectType.Riparabile;
            }
            // Sei tu un esaminabile?
            else if (storyObject.GetComponent<ObjInformation>())
            {
                objInformation = storyObject.GetComponent<ObjInformation>();
                storyObjectType = StoryObjectType.Esaminabile;
            }
            // Sei tu un apribile?
            else if (storyObject.GetComponent<OpenableControl>())
            {
                openableControl = storyObject.GetComponent<OpenableControl>();
                storyObjectType = StoryObjectType.Apribile;
            }
            // Sei tu una bombola?
            else if (storyObject.GetComponent<AbsorbMaterial>())
            {
                absorbMaterial = storyObject.GetComponent<AbsorbMaterial>();
                storyObjectType = StoryObjectType.Bombola;
            }
            // Sei tu una stazione di ricarica?
            else if (storyObject.GetComponent<RechargeActionObj>())
            {
                rechargeActionObj = storyObject.GetComponent<RechargeActionObj>();
                storyObjectType = StoryObjectType.StazioneDiRicarica;
            }
            // E allora muori!
            else
            {
                storyObjectType = StoryObjectType.Ignoto;
            }
            helpString = "Scegli il tipo di azione e aggiungi gli eventi";
        }
        else
        {
            storyItem.item = null;
            storyObjectType = StoryObjectType.Ignoto;
            helpString = "Aggiungi l'oggetto che deve far partire la storia per far apparire i tipi di azioni disponibili";
        }
    }

    void OnWizardOtherButton()
    {
        Close();
        StoryTeller window = (StoryTeller)GetWindow(typeof(StoryTeller), false, "StoryTeller");
    }

    void OnWizardCreate()
    {
        if (storyObject && storyEventStep.GetPersistentEventCount() > 0 && (terminalAction > 0 || triggerAction > 0 || repairAction > 0 || openAction > 0 || inspectAction > 0 || tankAction > 0 || rechargeAction > 0))
        {
            StoryEvent inGameStoryEvent = new StoryEvent();
            inGameStoryEvent.storyEvent = storyEventStep;
            storyManager.storyEvents.Add(inGameStoryEvent);

            StoryItem storyItem = new StoryItem();
            storyItem.item = storyObject;
            storyItem.storyNumber = numberOfStories;

            #region Check Action
            UnityAction<int> action = new UnityAction<int>(storyManager.StartStoryEvent);
            switch (storyObjectType)
            {
                case StoryObjectType.Terminale:
                    {
                        storyItem.storyAction = (int)terminalAction;
                        switch (terminalAction)
                        {
                            case TerminalAction.Interagisce:
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.interactionEvent, action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.FattoUpgradePermessi:
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventUpgrade.upgradedPodEvent, action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.GiaUpgradatoPermessi:
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventUpgrade.alreadyUpgradedEvent, action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.NessunPODInserito:
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventUpgrade.noPodInsertedEvent, action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.CodiceGiusto: // In teoria è un array
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventPin.rightPinEvent[0], action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.CodiceSbagliato:
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventPin.wrongPinEvent, action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.AttivaSistemaEnergia: // In teoria è un array
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackButtonVariableEvent.active[0], action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.DisattivaSistemaEnergia: // In teoria è un array
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackButtonVariableEvent.deactive[0], action, numberOfStories);
                                    break;
                                }
                            case TerminalAction.NoEnergia:
                                {
                                    UnityEventTools.AddIntPersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackButtonVariableEvent.noEnergy, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Trigger:
                    {
                        storyItem.storyAction = (int)triggerAction;
                        switch (triggerAction)
                        {
                            case TriggerAction.Entra:
                                {
                                    UnityEventTools.AddIntPersistentListener(triggerEvents.feedbackEventTrigger.onTriggerEnter, action, numberOfStories);
                                    break;
                                }
                            case TriggerAction.Esce:
                                {
                                    UnityEventTools.AddIntPersistentListener(triggerEvents.feedbackEventTrigger.onTriggerExit, action, numberOfStories);
                                    break;
                                }
                            case TriggerAction.EntraDaFronte:
                                {
                                    UnityEventTools.AddIntPersistentListener(triggerEvents.dotProductTriggerEvent.forwardEvent, action, numberOfStories);
                                    break;
                                }
                            case TriggerAction.EntraDaDietro:
                                {
                                    UnityEventTools.AddIntPersistentListener(triggerEvents.dotProductTriggerEvent.backEvent, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Riparabile:
                    {
                        storyItem.storyAction = (int)repairAction;
                        switch (repairAction)
                        {
                            case RepairAction.Ripara:
                                {
                                    UnityEventTools.AddIntPersistentListener(repairController.repairEvent, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Esaminabile:
                    {
                        storyItem.storyAction = (int)inspectAction;
                        switch (inspectAction)
                        {
                            case InspectAction.Esamina:
                                {
                                    UnityEventTools.AddIntPersistentListener(objInformation.inspectEvent, action, numberOfStories);
                                    break;
                                }
                            case InspectAction.SmetteDiEsaminare:
                                {
                                    UnityEventTools.AddIntPersistentListener(objInformation.stopInspectEvent, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Apribile:
                    {
                        storyItem.storyAction = (int)openAction;
                        switch (openAction)
                        {
                            case OpenAction.Apre:
                                {
                                    UnityEventTools.AddIntPersistentListener(openableControl.openEvent, action, numberOfStories);
                                    break;
                                }
                            case OpenAction.Chiude:
                                {
                                    UnityEventTools.AddIntPersistentListener(openableControl.closeEvent, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Bombola:
                    {
                        storyItem.storyAction = (int)tankAction;
                        switch (tankAction)
                        {
                            case TankAction.Assorbe:
                                {
                                    UnityEventTools.AddIntPersistentListener(absorbMaterial.absorbEvent, action, numberOfStories);
                                    break;
                                }
                            case TankAction.Versa:
                                {
                                    UnityEventTools.AddIntPersistentListener(absorbMaterial.expelEvent, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.StazioneDiRicarica:
                    {
                        storyItem.storyAction = (int)rechargeAction;
                        switch (rechargeAction)
                        {
                            case RechargeAction.MettePOD:
                                {
                                    UnityEventTools.AddIntPersistentListener(rechargeActionObj.podInsertEvent, action, numberOfStories);
                                    break;
                                }
                            case RechargeAction.PrendePOD:
                                {
                                    UnityEventTools.AddIntPersistentListener(rechargeActionObj.podTakeEvent, action, numberOfStories);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                default:
                    break;
            }
            #endregion

            storyManager.storyItems.Add(storyItem);
        }
    }
}