﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using UnityEditor.Events;

public class StoryDestroyer : ScriptableWizard
{
    private StoryManager storyManager;
    private StoryItem storyItem = new StoryItem();
    private TerminalStatus terminalStatus;
    private TriggerEvents triggerEvents;
    private RepairController repairController;
    private ObjInformation objInformation;
    private OpenableControl openableControl;
    private AbsorbMaterial absorbMaterial;
    private RechargeActionObj rechargeActionObj;

    ////// START //////

    [Header("Numero di storie che sono già state scritte")]

    [ShowOnly]
    public int numberOfStories;

    //////

    [Header("Oggetto che faceva partire la storia")]

    public GameObject storyObject;

    [ShowIf("storyObjectType")]
    public StoryObjectType storyObjectType;

    //////

    [Header("Azione che faceva partire la storia")]

    [ShowIf("storyObjectType", StoryObjectType.Terminale)]
    public TerminalAction terminalAction;

    [ShowIf("storyObjectType", StoryObjectType.Trigger)]
    public TriggerAction triggerAction;

    [ShowIf("storyObjectType", StoryObjectType.Riparabile)]
    public RepairAction repairAction;

    [ShowIf("storyObjectType", StoryObjectType.Esaminabile)]
    public InspectAction inspectAction;

    [ShowIf("storyObjectType", StoryObjectType.Apribile)]
    public OpenAction openAction;

    [ShowIf("storyObjectType", StoryObjectType.Bombola)]
    public TankAction tankAction;

    [ShowIf("storyObjectType", StoryObjectType.StazioneDiRicarica)]
    public RechargeAction rechargeAction;

    ////// END //////

    static void CreateWizard()
    {
    }

    void OnWizardUpdate()
    {
        storyManager = FindObjectOfType<StoryManager>(); 
        numberOfStories = storyManager.storyEvents.Count;
        if (storyObject)
        {
            // Sei tu un terminale?
            if (storyObject.GetComponentInChildren<TerminalStatus>())
            {
                terminalStatus = storyObject.GetComponentInChildren<TerminalStatus>();
                storyObjectType = StoryObjectType.Terminale;
            }
            // Sei tu un trigger?
            else if (storyObject.GetComponent<TriggerEvents>())
            {
                triggerEvents = storyObject.GetComponent<TriggerEvents>();
                storyObjectType = StoryObjectType.Trigger;
            }
            // Sei tu un riparabile?
            else if (storyObject.GetComponent<RepairController>())
            {
                repairController = storyObject.GetComponent<RepairController>();
                storyObjectType = StoryObjectType.Riparabile;
            }
            // Sei tu un esaminabile?
            else if (storyObject.GetComponent<ObjInformation>())
            {
                objInformation = storyObject.GetComponent<ObjInformation>();
                storyObjectType = StoryObjectType.Esaminabile;
            }
            // Sei tu un apribile?
            else if (storyObject.GetComponent<OpenableControl>())
            {
                openableControl = storyObject.GetComponent<OpenableControl>();
                storyObjectType = StoryObjectType.Apribile;
            }
            // Sei tu una bombola?
            else if (storyObject.GetComponent<AbsorbMaterial>())
            {
                absorbMaterial = storyObject.GetComponent<AbsorbMaterial>();
                storyObjectType = StoryObjectType.Bombola;
            }
            // Sei tu una stazione di ricarica?
            else if (storyObject.GetComponent<RechargeActionObj>())
            {
                rechargeActionObj = storyObject.GetComponent<RechargeActionObj>();
                storyObjectType = StoryObjectType.StazioneDiRicarica;
            }
            // E allora muori!
            else
            {
                storyObjectType = StoryObjectType.Ignoto;
            }
            helpString = "Scegli il tipo di azione";
        }
        else
        {
            storyObjectType = StoryObjectType.Ignoto;
            helpString = "Aggiungi l'oggetto che faceva partire la storia per far apparire i tipi di azioni disponibili";
        }
    }

    void OnWizardOtherButton()
    {
        Close();
        StoryTeller window = (StoryTeller)GetWindow(typeof(StoryTeller), false, "StoryTeller");
    }

    void OnWizardCreate()
    {
        // Rimuovo le azioni all'evento della storia
        // Per ora le tengo...

        // Rimuovo l'evento della storia dall'oggetto (spero che l'evento della storia sia sempre il primo...)
        if (storyObject && (terminalAction > 0 || triggerAction > 0 || repairAction > 0 || openAction > 0 || inspectAction > 0 || tankAction > 0 || rechargeAction > 0))
        {
            storyItem = storyManager.storyItems.Find(item => item.item == storyObject);
            storyManager.storyItems.Remove(storyItem);
            switch (storyObjectType)
            {
                case StoryObjectType.Terminale:
                    {
                        switch (terminalAction)
                        {
                            case TerminalAction.Interagisce:
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.interactionEvent, 0);
                                    break;
                                }
                            case TerminalAction.FattoUpgradePermessi:
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventUpgrade.upgradedPodEvent, 0);
                                    break;
                                }
                            case TerminalAction.GiaUpgradatoPermessi:
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventUpgrade.alreadyUpgradedEvent, 0);
                                    break;
                                }
                            case TerminalAction.NessunPODInserito:
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventUpgrade.noPodInsertedEvent, 0);
                                    break;
                                }
                            case TerminalAction.CodiceGiusto: // In teoria è un array
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventPin.rightPinEvent[0], 0);
                                    break;
                                }
                            case TerminalAction.CodiceSbagliato:
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackEventPin.wrongPinEvent, 0);
                                    break;
                                }
                            case TerminalAction.AttivaSistemaEnergia: // In teoria è un array
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackButtonVariableEvent.active[0], 0);
                                    break;
                                }
                            case TerminalAction.DisattivaSistemaEnergia: // In teoria è un array
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackButtonVariableEvent.deactive[0], 0);
                                    break;
                                }
                            case TerminalAction.NoEnergia:
                                {
                                    UnityEventTools.RemovePersistentListener(terminalStatus.gameObject.GetComponent<TerminalEvents>().feedbackButtonVariableEvent.noEnergy, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Trigger:
                    {
                        switch (triggerAction)
                        {
                            case TriggerAction.Entra:
                                {
                                    UnityEventTools.RemovePersistentListener(triggerEvents.feedbackEventTrigger.onTriggerEnter, 0);
                                    break;
                                }
                            case TriggerAction.Esce:
                                {
                                    UnityEventTools.RemovePersistentListener(triggerEvents.feedbackEventTrigger.onTriggerExit, 0);
                                    break;
                                }
                            case TriggerAction.EntraDaFronte:
                                {
                                    UnityEventTools.RemovePersistentListener(triggerEvents.dotProductTriggerEvent.forwardEvent, 0);
                                    break;
                                }
                            case TriggerAction.EntraDaDietro:
                                {
                                    UnityEventTools.RemovePersistentListener(triggerEvents.dotProductTriggerEvent.backEvent, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Riparabile:
                    {
                        switch (repairAction)
                        {
                            case RepairAction.Ripara:
                                {
                                    UnityEventTools.RemovePersistentListener(repairController.repairEvent, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Esaminabile:
                    {
                        switch (inspectAction)
                        {
                            case InspectAction.Esamina:
                                {
                                    UnityEventTools.RemovePersistentListener(objInformation.inspectEvent, 0);
                                    break;
                                }
                            case InspectAction.SmetteDiEsaminare:
                                {
                                    UnityEventTools.RemovePersistentListener(objInformation.stopInspectEvent, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Apribile:
                    {
                        switch (openAction)
                        {
                            case OpenAction.Apre:
                                {
                                    UnityEventTools.RemovePersistentListener(openableControl.openEvent, 0);
                                    break;
                                }
                            case OpenAction.Chiude:
                                {
                                    UnityEventTools.RemovePersistentListener(openableControl.closeEvent, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.Bombola:
                    {
                        switch (tankAction)
                        {
                            case TankAction.Assorbe:
                                {
                                    UnityEventTools.RemovePersistentListener(absorbMaterial.absorbEvent, 0);
                                    break;
                                }
                            case TankAction.Versa:
                                {
                                    UnityEventTools.RemovePersistentListener(absorbMaterial.expelEvent, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                case StoryObjectType.StazioneDiRicarica:
                    {
                        switch (rechargeAction)
                        {
                            case RechargeAction.MettePOD:
                                {
                                    UnityEventTools.RemovePersistentListener(rechargeActionObj.podInsertEvent, 0);
                                    break;
                                }
                            case RechargeAction.PrendePOD:
                                {
                                    UnityEventTools.RemovePersistentListener(rechargeActionObj.podTakeEvent, 0);
                                    break;
                                }
                            default:
                                break;
                        }
                        break;
                    }
                default:
                    break;
            }
        }
    }
}