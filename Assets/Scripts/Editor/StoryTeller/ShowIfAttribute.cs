﻿using UnityEngine;

public class ShowIfAttribute : PropertyAttribute
{
    public readonly string variable;
    public readonly bool boolValue;
    public readonly int intValue;
    public readonly StoryObjectType storyObjectTypeValue;

    public ShowIfAttribute(string variable, int value = -1)
    {
        this.variable = variable;
        this.intValue = value;
    }

    public ShowIfAttribute(string variable, bool value)
    {
        this.variable = variable;
        this.boolValue = value;
    }

    public ShowIfAttribute(string variable, StoryObjectType value)
    {
        this.variable = variable;
        this.storyObjectTypeValue = value;
    }
}