﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public enum StoryObjectType
{
    Ignoto,
    Terminale,
    Trigger,
    Riparabile,
    Esaminabile,
    Apribile,
    Bombola,
    StazioneDiRicarica
}

public enum TerminalAction
{
    ScegliUnaAzione,
    Interagisce,
    FattoUpgradePermessi,
    GiaUpgradatoPermessi,
    NessunPODInserito,
    CodiceGiusto,
    CodiceSbagliato,
    AttivaSistemaEnergia,
    DisattivaSistemaEnergia,
    NoEnergia
}

public enum TriggerAction
{
    ScegliUnaAzione,
    Entra,
    Esce,
    EntraDaFronte,
    EntraDaDietro
}

public enum RepairAction
{
    ScegliUnaAzione,
    Ripara
}

public enum InspectAction
{
    ScegliUnaAzione,
    Esamina,
    SmetteDiEsaminare
}

public enum OpenAction
{
    ScegliUnaAzione,
    Apre,
    Chiude
}

public enum TankAction
{
    ScegliUnaAzione,
    Assorbe,
    Versa
}

public enum RechargeAction
{
    ScegliUnaAzione,
    MettePOD,
    PrendePOD
}

public class StoryTeller : EditorWindow
{
    private static Texture2D logo;

    [MenuItem("Tools/StoryTeller")]

    static void Init()
    {
        StoryTeller window = (StoryTeller)GetWindow(typeof(StoryTeller), false, "StoryTeller");
    }
    
    void OnEnable()
    {
        logo = Resources.Load("StoryTeller/dandelion", typeof(Texture2D)) as Texture2D;
    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect((Screen.width / 2) - (Screen.height / 5.1f), 0, (Screen.height - 30)/2.55f, Screen.height - 30), logo);
        GUILayout.BeginArea(new Rect((Screen.width / 2) - 150, (Screen.height / 2) - 40, 300, 100));
        if (GUILayout.Button("Racconta una storia (aggiungi)"))
        {
            Close();
            ScriptableWizard.DisplayWizard("StoryCreator", typeof(StoryCreator), "Aggiungi >", "< Torna indietro");
        }
        GUILayout.Space(10);
        if (GUILayout.Button("Trasforma una storia in leggenda (rimuovi)"))
        {
            Close();
            ScriptableWizard.DisplayWizard("StoryDestroyer", typeof(StoryDestroyer), "Rimuovi >", "< Torna indietro");
        }
        GUILayout.Space(10);
        if (GUILayout.Button("Ripensa una storia (modifica)"))
        {
            Close();
            ScriptableWizard.DisplayWizard("StoryEditor", typeof(StoryEditor), "Modifica >", "< Torna indietro");
        }
        GUILayout.EndArea();
    }
}