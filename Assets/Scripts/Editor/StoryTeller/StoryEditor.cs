﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using UnityEditor.Events;

public class StoryEditor : ScriptableWizard
{
    private StoryManager storyManager;
    private StoryItem storyItem;
    private TerminalStatus terminalStatus;
    private TriggerEvents triggerEvents;
    private RepairController repairController;
    private ObjInformation objInformation;
    private OpenableControl openableControl;
    private AbsorbMaterial absorbMaterial;
    private RechargeActionObj rechargeActionObj;

    private int storyNumber = -1;

    ////// START //////

    [Header("Numero di storie che sono già state scritte")]

    [ShowOnly]
    public int numberOfStories;

    //////

    [Header("Oggetto che faceva partire la storia")]

    public GameObject storyObject;

    [ShowIf("storyObjectType")]
    public StoryObjectType storyObjectType;

    //////

    [Header("Azione che faceva partire la storia")]

    [ShowIf("storyObjectType", StoryObjectType.Terminale)]
    public TerminalAction terminalAction;

    [ShowIf("storyObjectType", StoryObjectType.Trigger)]
    public TriggerAction triggerAction;

    [ShowIf("storyObjectType", StoryObjectType.Riparabile)]
    public RepairAction repairAction;

    [ShowIf("storyObjectType", StoryObjectType.Esaminabile)]
    public InspectAction inspectAction;

    [ShowIf("storyObjectType", StoryObjectType.Apribile)]
    public OpenAction openAction;

    [ShowIf("storyObjectType", StoryObjectType.Bombola)]
    public TankAction tankAction;

    [ShowIf("storyObjectType", StoryObjectType.StazioneDiRicarica)]
    public RechargeAction rechargeAction;

    //////

    [Header("Cosa succede ora nella storia?")]

    public UnityEvent storyEventStep = new UnityEvent();

    ////// END //////

    static void CreateWizard()
    {
    }

    void OnWizardUpdate()
    {
        storyManager = FindObjectOfType<StoryManager>(); 
        numberOfStories = storyManager.storyEvents.Count;
        if (storyObject)
        {
            // Sei tu un terminale?
            if (storyObject.GetComponentInChildren<TerminalStatus>())
            {
                terminalStatus = storyObject.GetComponentInChildren<TerminalStatus>();
                storyObjectType = StoryObjectType.Terminale;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)terminalAction);
            }
            // Sei tu un trigger?
            else if (storyObject.GetComponent<TriggerEvents>())
            {
                triggerEvents = storyObject.GetComponent<TriggerEvents>();
                storyObjectType = StoryObjectType.Trigger;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)triggerAction);
            }
            // Sei tu un riparabile?
            else if (storyObject.GetComponent<RepairController>())
            {
                repairController = storyObject.GetComponent<RepairController>();
                storyObjectType = StoryObjectType.Riparabile;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)repairAction);
            }
            // Sei tu un esaminabile?
            else if (storyObject.GetComponent<ObjInformation>())
            {
                objInformation = storyObject.GetComponent<ObjInformation>();
                storyObjectType = StoryObjectType.Esaminabile;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)inspectAction);
            }
            // Sei tu un apribile?
            else if (storyObject.GetComponent<OpenableControl>())
            {
                openableControl = storyObject.GetComponent<OpenableControl>();
                storyObjectType = StoryObjectType.Apribile;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)openAction);
            }
            // Sei tu una bombola?
            else if (storyObject.GetComponent<AbsorbMaterial>())
            {
                absorbMaterial = storyObject.GetComponent<AbsorbMaterial>();
                storyObjectType = StoryObjectType.Bombola;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)tankAction);
            }
            // Sei tu una stazione di ricarica?
            else if (storyObject.GetComponent<RechargeActionObj>())
            {
                rechargeActionObj = storyObject.GetComponent<RechargeActionObj>();
                storyObjectType = StoryObjectType.StazioneDiRicarica;
                storyItem = storyManager.storyItems.Find(item => item.item == storyObject && item.storyAction == (int)rechargeAction);
            }
            // E allora muori!
            else
            {
                storyObjectType = StoryObjectType.Ignoto;
            }
            helpString = "Scegli il tipo di azione e modifica gli eventi";
            if (storyItem != null && storyItem.storyNumber >= 0)
            {
                if (storyItem.storyNumber != storyNumber)
                {
                    storyEventStep = storyManager.storyEvents[storyItem.storyNumber].storyEvent;
                    errorString = "ATTENZIONE! L'EDITING E' IN TEMPO REALE!!!";
                }
                storyNumber = storyItem.storyNumber;
            }
            else
            {
                ResetAll(true);
            }
        }
        else
        {
            ResetAll();
        }
    }

    void OnWizardOtherButton()
    {
        Close();
        StoryTeller window = (StoryTeller)GetWindow(typeof(StoryTeller), false, "StoryTeller");
    }

    void OnWizardCreate()
    {
        if (storyObject && storyNumber >= 0 && storyEventStep.GetPersistentEventCount() > 0 && (terminalAction > 0 || triggerAction > 0 || repairAction > 0 || openAction > 0 || inspectAction > 0 || tankAction > 0 || rechargeAction > 0))
        {
            storyManager.storyEvents[storyNumber].storyEvent = storyEventStep;
        }
    }

    void ResetAll(bool nSO = false)
    {
        storyItem = new StoryItem();
        storyItem.storyNumber = -1;
        storyNumber = -1;
        storyEventStep = null;
        errorString = "";
        if (!nSO)
        {
            storyObjectType = StoryObjectType.Ignoto;
            helpString = "Aggiungi l'oggetto che deve far partire la storia per far apparire i tipi di azioni disponibili";
        }
    }
}