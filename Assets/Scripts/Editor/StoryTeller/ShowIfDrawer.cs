﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShowIfAttribute))]
public class ShowIfDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (ShouldShow(property))
        {
            EditorGUI.PropertyField(position, property, true);
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (ShouldShow(property))
        {
            return base.GetPropertyHeight(property, label);
        }
        else
        {
            return 0;
        }
    }

    private bool ShouldShow(SerializedProperty property)
    {
        ShowIfAttribute hider = attribute as ShowIfAttribute;
        SerializedProperty varToCheck = property.serializedObject.FindProperty(hider.variable);

        if (hider.intValue == -1)
        {
            return varToCheck.enumValueIndex > 0;
        }
        else if (varToCheck.propertyType == SerializedPropertyType.Boolean)
        {
            return varToCheck.boolValue == hider.boolValue;
        }
        else if (varToCheck.propertyType == SerializedPropertyType.Integer)
        {
            return varToCheck.intValue == hider.intValue;
        }
        else if (varToCheck.propertyType == SerializedPropertyType.Enum)
        {
            return varToCheck.enumValueIndex == (int)hider.storyObjectTypeValue;
        }
        else
        {
            return false;
        }
    }
}