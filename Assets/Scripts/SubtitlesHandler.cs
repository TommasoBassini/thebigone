﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public struct SubtitleLines
{
    public string audioName;
    public string subtitlesText;
    public float delay;
    public string speaker;
}

public static class SubtitlesHandler
{
    public static List<SubtitleLines> subtitlesList = new List<SubtitleLines>();

    public static IEnumerator subtitlesEvent(Subtitles subs, GameObject text, AudioSource source)
    {
        for (int i = subs.startLines; i <= subs.endLines; i++)
        {
            yield return new WaitForSeconds(subtitlesList[i].delay);
            text.GetComponent<Text>().text = subtitlesList[i].subtitlesText;
            AudioClip audioClip = Resources.Load<AudioClip>("Dialogues/" + subtitlesList[i].audioName);
            source.PlayOneShot(audioClip);
            yield return new WaitForSeconds(audioClip.length);
        }
        text.GetComponent<Text>().text = "";
        subs.onSubtitleEnd.Invoke();
    }

    public static void ReadCSV()
    {
        var path = "Assets/Resources/subtitles.csv";

        StreamReader sr = new StreamReader(path);

        subtitlesList.Add(new SubtitleLines());

        string s;

        while ((s = sr.ReadLine()) != null)
        {
            string[] cells = s.Split(';');

            SubtitleLines sub;
            sub.audioName = cells[0];
            sub.subtitlesText = cells[1];
            sub.delay = float.Parse(cells[2]);
            sub.speaker = cells[3];
            subtitlesList.Add(sub);
        }
        sr.Close();
    }
}