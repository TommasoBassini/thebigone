﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public class FeedbackEventPin
{
    public UnityEvent[] rightPinEvent;
    public UnityEvent wrongPinEvent;
}

[System.Serializable]
public class FeedbackEventPod
{
    public UnityEvent upgradedPodEvent;
    public UnityEvent alreadyUpgradedEvent;
    public UnityEvent noPodInsertedEvent;
}

[System.Serializable]
public class FeedbackEventPermission
{
    public UnityEvent[] hasPermission;
    public UnityEvent[] noPermission;
}

[System.Serializable]
public class FeedbackButtonVariableEvent
{
    public UnityEvent[] active;
    public UnityEvent[] deactive;
    public UnityEvent noEnergy;
}

[System.Serializable]
public class TimedFeedbackEvent
{
    public UnityEvent[] timedEvent;
}

[System.Serializable]
public class TimedEvent
{
    public float timeToWaitMin;
    public float timeToWaitMax;
    public UnityEvent timedEvent;
}

[System.Serializable]
public class StoryEvent
{
    [HideInInspector]
    public bool isDone;
    public UnityEvent storyEvent;
}

[System.Serializable]
public class StoryItem
{
    public GameObject item;
    public int storyNumber;
    public int storyAction;
}

[System.Serializable]
public class OneShotFeedbackEvent
{
    public UnityEvent[] oneShotEvent;
    public bool[] oneShotBools; 
}

[System.Serializable]
public class GeneralFeedbackEvent
{
    public UnityEvent[] generalEvent;
}

[System.Serializable]
public class FeedbackTriggerEvent
{
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;
}

[System.Serializable]
public class DotProductTriggerEvent
{
    public UnityEvent forwardEvent;
    public UnityEvent backEvent;
}

[System.Serializable]
public struct Images
{
    public Image imageToChange;
    public Color imageColor;
    public Sprite imageSprite;
    public float imageFill;
    public float timerToFill;
    public bool isWorking;
}

[System.Serializable]
public struct ImagesToFillEnergy
{
    public Image imageToChange;
    public float timerToFill;
    public int terminalValue;
    public Text textValue;
    public string valuePrefix;
    public string valueSuffix;
}

[System.Serializable]
public struct Texts
{
    public Text textToChange;
    public Color textColors;
    public string textString;
}

[System.Serializable]
public struct Timed
{
    public float timeToWait;
    public int nEvent;
    public bool isBlocked;
}

[System.Serializable]
public struct Audios
{
    public AudioClip audioClip;
}

[System.Serializable]
public struct Animations
{
    public Animator objAnimator;
    public string triggerName;
}

[System.Serializable]
public struct Lights
{
    public GameObject lightsParent;
    [Header("Change Light Behaviour")]
    public LightBehavior lightBehaviour;
}

[System.Serializable]
public struct Particles
{
    public GameObject particlesParent;
}

[System.Serializable]
public struct Gameobjects
{
    public GameObject gameobject;
    public int nSecondiFlesh;
    public bool isWorking;
}

[System.Serializable]
public struct Subtitles
{
    public int startLines;
    public int endLines;
    public UnityEvent onSubtitleEnd;
}

[System.Serializable]
public struct UpgradePermission
{
    public bool isDone;
    public SecurityType permission;
    public int level;
}

[System.Serializable]
public struct SceneLoader
{
    public int nScena;
    public string nomeScena;
    public Vector3 newScenePlayerPosition;
    public Sprite imageLoad;
}

public enum RaycastTarget
{
    nothing,
    pickable,
    terminal,
    actionButton,
    actionLever,
    actionOpenable,
    actionHelmet,
    actionPod,
    absorb,
    expel,
    putRecharge,
    takeRecharge,
    repair
}

[System.Serializable]
public struct ObjectInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct TerminalInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct ActionButtonInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct ActionLeverInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct ActionOpenableInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct AbsorbInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct ExpelInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct HelmetInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct PodInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct PutRechargeInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct TakeRechargeInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct RepairInputFeedback
{
    public Sprite button;
    public string textToView;
    [Tooltip("L'icona che è sotto il mirino")]
    public Sprite actionSprite;
}

[System.Serializable]
public struct AutoMovePlayer
{
    public GameObject parentTransform;
    public bool lookOtherGameobject;
    public GameObjectToLook[] gameObjectToLook;
}

[System.Serializable]
public struct GameObjectToLook
{
    public GameObject go;
    public float timeToview;
}
