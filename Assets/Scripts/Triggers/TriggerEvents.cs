﻿using UnityEngine;
using System.Collections;

public class TriggerEvents : MonoBehaviour
{
    [Header("Eventi per trigger")]
    public FeedbackTriggerEvent feedbackEventTrigger;
    public bool dontDestroy = false;

    [Header("Eventi per dotProduct")]
    public DotProductTriggerEvent dotProductTriggerEvent;
    public bool isDotProductEvent = false;

    [Header("Eventi a tempo")]
    public Timed[] times;
    public TimedFeedbackEvent timedFeedbackEvent;

    private bool isDoneEnter = false;
    private bool isDoneExit = false;


    public IEnumerator TimedFeedback(int n, float t)
    {
        yield return new WaitForSeconds(t);
        timedFeedbackEvent.timedEvent[n].Invoke();
    }

    public void TimedEvent(int n)
    {
        StartCoroutine(TimedFeedback(times[n].nEvent, times[n].timeToWait));
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!dontDestroy)
            {
                if (!isDoneEnter)
                {
                    feedbackEventTrigger.onTriggerEnter.Invoke();
                    isDoneEnter = true;
                }
            }
            else
            {
                feedbackEventTrigger.onTriggerEnter.Invoke();
            }

            if (isDotProductEvent)
            {
                Vector3 toTarget = (col.gameObject.transform.position - transform.position).normalized;
                float dotProduct = Vector3.Dot(toTarget, col.transform.position.normalized);
                dotProduct *= -1;
                if (dotProduct > 0)
                {
                    dotProductTriggerEvent.forwardEvent.Invoke();
                }
                else
                {
                    dotProductTriggerEvent.backEvent.Invoke();
                }

            }
        }
    }

    void OnTriggerExit()
    {
        if (!isDoneExit)
        {
            feedbackEventTrigger.onTriggerExit.Invoke();
            isDoneExit = true;
        }
    }
}
